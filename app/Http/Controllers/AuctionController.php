<?php

namespace App\Http\Controllers;

use App\Models\Auction;
use App\Models\AuctionConfirm;
use App\Models\Bid;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AuctionController extends Controller
{
    public function showDetail(int $id, Request $request)
    {
        //Đổ dữ liệu
        $data = [];
        $auction_detail = Auction::with('product.seller')->findOrFail($id);
        $data['auction_detail'] = $auction_detail;

        //---------------------Xử lí Bid--------------------->tiếp tục xử lí ở BidController
        // Lấy thông tin id bidder từ authenticated user
        $bidder_id = auth()->id();

        // Lấy thông tin bidder hiện tại
        $bidder = Bid::where('bidder_id', $bidder_id)->where('auction_id', $id)->first();

        // Lấy thông tin phiên đấu giá
        $auction = Auction::with('product')->findOrFail($id);

        // Lấy giá cao nhất hiện tại
        if ($auction->highest_price === null) {
            $auction->highest_price = $auction->product->starting_price;     
        } 
        $auction->save();

        $current_highest_price = $auction->highest_price;
        $data['current_highest_price'] = $current_highest_price;


        // Giá cao nhất của bidder 
        $highest_your_price = $bidder->price_bidding ?? 0;
        $data['highest_your_price'] = $highest_your_price;

        // Kiểm tra nếu giá cao nhất hiện tại bằng giá cao nhất của bidder để hiển thị id="UserHighestPrice"
        $user_highest_price = ($current_highest_price ===  $highest_your_price); // true or false
        $data['user_highest_price'] = $user_highest_price;

        //---------------------End xử lí Bid-----------------------

        //---------------------Xử lí trường hợp login - đăng kí---------------------
        // Xử lí trường hợp đã login, muốn đăng kí
        if (Auth::check()) {
            $user_login = Auth::user();
            $registered = AuctionConfirm::where('bidder_id', $user_login->id)
                ->where('status', 0)
                ->exists();

            $data['registered'] = $registered;
        } else {
            // Xử lí trường hợp đã login, muốn đăng kí
            // Lưu URL hiện tại vào session chỉ khi người dùng chưa đăng nhập
            if (!$request->session()->has('redirect_url')) {
                $currentUrl = $request->fullUrl();
                $request->session()->put('redirect_url', $currentUrl);
            }
        }

        return view('auctions.auction_detail', $data);
    }

    public function register(int $id)
    {
        //Đổ dữ liệu
        $data = [];
        //biến này dùng trong countdown_timer
        $auction_detail = Auction::with('product.seller')->findOrFail($id);
        $data['auction_detail'] = $auction_detail;

        $auction_register = Auction::with('product.seller')->findOrFail($id);
        $data['auction_register'] = $auction_register;
        $data['auction_id'] = $id;

        return view('auctions.auction_register', $data);
    }

    public function payment(Request $request, $id)
    {
        // Lấy thông tin phiên đấu giá dựa trên id để thanh toán
        $data = [];
        $auction_payment = Auction::with('product')->findOrFail($id);
        $data['auction_payment'] = $auction_payment;

        // Lấy thông tin người đấu giá
        $bidder_info = Auth::user();
        $data['bidder_info'] = $bidder_info;

        // Kiểm tra check từ form 
        $validate_data = $request->validate([
            'options' => 'required|array|min:1',
            'options.*' => 'required|accepted',
        ]);

        return view('auctions.auction_payment', $data);
    }

    public function complete(Request $request, $id)
    {
        $data = [];
        /**
         * @var User $user_login
         */
        // Lấy thông tin người đăng nhập
        $user_login = Auth::user();

        // Lấy thông tin phiên đấu giá dựa trên id để hoàn tất thanh toán
        $auction_complete = Auction::findOrFail($id);
        $data['auction_complete'] = $auction_complete;
        // dd($request->all());

        //Check balance
        $total = $auction_complete->registration_fee + $auction_complete->down_payment;
        $balance = Auth::user()->balance;
        if ($balance < $total) {
            return redirect()->back()->with('error', 'Số dư của bạn không đủ');
        }

        // Kiểm tra xác nhận điều khoản và điều kiện
        if (!$request->has('confirm')) {
            return redirect()->back()->withErrors(['confirm' => 'Vui lòng đồng ý với điều khoản và điều kiện.']);
        }

        //save DB
        $data_save = [
            'bidder_id' => $user_login->id,
            'auction_id' => $id,
            'status' => 0
        ];

        DB::beginTransaction();
        try {

            AuctionConfirm::create($data_save);

            //update balance
            $user_login->balance = $balance - $total;
            $user_login->save();
            DB::commit();

            //lưu DB ok
            return redirect()->route('home')->with('success', 'Đăng kí thành công');
            //with(): gọi là session flash. chỉ hiển thị trong 1 phiên làm việc
        } catch (Exception $exception) {
            //lưu DB FAIL
            DB::rollBack();

            // dd($exception->getMessage());
            return redirect()->back()->with('error', 'Đăng kí thất bại ' .  $exception->getMessage());
        }
    }
}
