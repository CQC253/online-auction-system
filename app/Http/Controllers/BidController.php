<?php

namespace App\Http\Controllers;

use App\Models\Auction;
use App\Models\AuctionConfirm;
use App\Models\Bid;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
//gửi mail
use Illuminate\Support\Facades\Mail;
use App\Mail\ReceiptMail;
use App\Models\Receipt;

class BidController extends Controller
{
    public function bid(Request $request, $id)
    {
        // Lấy thông tin id bidder từ authenticated user
        $bidder_id = auth()->id();

        // Lấy thông tin bidder hiện tại
        $bidder = Bid::where('bidder_id', $bidder_id)->where('auction_id', $id)->first();

        // Lấy thông tin phiên đấu giá
        $auction = Auction::with('product')->findOrFail($id);
        
        // Lấy số bước giá từ request
        $step = $request->input('step');

        // Lấy giá bước giá từ cơ sở dữ liệu
        $price_step = $auction->price_step;


        //Xử lí giá cao nhất của bidder và giá cao nhất hiện tại
        if (!$bidder) {
            // Cập nhật giá cao nhất của bidder khi chưa trả giá lần nào
            $bidder = Bid::create([
                'bidder_id' => $bidder_id,
                'auction_id' => $id,
                'price_bidding' => 0,
            ]);
            //Tính giá cao nhất hiện tại
            // Lấy giá cao nhất hiện tại
            if ($auction->highest_price === null) {
                $current_highest_price = $auction->product->starting_price + ($step * $price_step);;
            } else {
                $current_highest_price = $auction->highest_price + ($step * $price_step);
            }
            $data['current_highest_price'] = $current_highest_price;
     
            //Cập nhật giá cao nhất hiện tại
            $auction->highest_price =  $current_highest_price;
            $auction->save();

            // Giá cao nhất của bidder khi trả giá
            $highest_your_price = $current_highest_price;
            // Cập nhật giá cao nhất của bidder trong cơ sở dữ liệu
            $bidder->price_bidding = $highest_your_price;
            $bidder->save();
        } else {
            // Lấy bidder đang trả giá cao nhất là giá cao nhất hiện tại
            $current_highest_bidder = Bid::where('auction_id', $id)
            ->orderBy('price_bidding', 'desc')
            ->first();
            //Tính giá cao nhất hiện tại
            $current_highest_price = $current_highest_bidder->price_bidding + ($step * $price_step);
            // Cập nhật giá cao nhất hiện tại trong cơ sở dữ liệu
            $auction->highest_price = $current_highest_price;
            $auction->save();

            // Giá cao nhất của bidder khi trả giá
            $highest_your_price = $current_highest_price;
            // Cập nhật giá cao nhất của bidder trong cơ sở dữ liệu
            $bidder->price_bidding = $highest_your_price;
            $bidder->save();
        }
        //Giá cao nhất hiện tại
        $current_highest_bidder = $auction->highest_price;

        // Giá cao nhất của bidder 
        $highest_your_price = $bidder->price_bidding;

        return redirect()->back()->with('success', 'Trả giá thành công!');
    }

    public function highest_price($id)
    {
        $highest_price = Bid::where('auction_id', $id)->max('price_bidding');

        return response()->json(['highest_price' => $highest_price]);
    }

    public function user_highest_price($id)
    {
        // Lấy thông tin id bidder từ authenticated user
        $bidder_id = auth()->id();

        // Lấy thông tin bidder hiện tại
        $bidder = Bid::where('bidder_id', $bidder_id)->where('auction_id', $id)->first();

        // Lấy thông tin phiên đấu giá
        $auction = Auction::with('product')->findOrFail($id);
        // Lấy giá cao nhất hiện tại
        if ($auction->highest_price === null) {
            $current_highest_price = $auction->product->starting_price;
        }else{
            $current_highest_price = $auction->highest_price;
        }
           
        // Giá cao nhất của bidder 
        $highest_your_price = $bidder->price_bidding ?? 0;

        // Kiểm tra nếu giá cao nhất hiện tại bằng giá cao nhất của bidder
        $user_highest_price = ($current_highest_price ===  $highest_your_price);

        return response()->json(['user_highest_price' => $user_highest_price]);
    }

    public function update_status($id) //dùng để update trạng thái của bảng auctions thành 0: đấu giá thất bại, 1: hoàn thành đấu giá (bid_countdown_timer.js)
    //Cập nhật thêm bảng Receipt
    {
        // Xử lí status của bảng auctions
        $auction = Auction::with('product')->findOrFail($id);
        $bidder = User::leftJoin('bids', function ($join) use ($auction) {
            $join->on('users.id', '=', 'bids.bidder_id')
                ->where('bids.auction_id', '=', $auction->id);
        })->orderByDesc('bids.price_bidding')
            ->select('users.*', 'bids.price_bidding')
            ->first();

        if ($auction->highest_price === null) {
            $auction->highest_price = $auction->product->starting_price;
            $auction->save();
        }

        if($auction->status === null){
            if ($auction->product->starting_price === $auction->highest_price) {
                $auction->status = 0; // Đấu giá thất bại
                $auction->save();
            } elseif ($auction->product->starting_price < $auction->highest_price) {
                $auction->status = 1; // Hoàn thành đấu giá 
                $auction->save();
                Receipt::create([
                    'bidder_id' => $bidder->id,
                    'auction_id' => $auction->id,
                ]);
            }
            $this->send_email($id); // Gửi email khi đồng hồ đếm kết thúc
        }
        return response()->json(['message' => 'Status updated successfully'], 200);
    }

    public function result_bid($id)
    {
        $data = [];
        $auction_result = Auction::with('product.seller')->findOrFail($id);
        
        $data['auction_result'] = $auction_result;

        $user = User::leftJoin('bids', function ($join) use ($auction_result) {
            $join->on('users.id', '=', 'bids.bidder_id')
                ->where('bids.auction_id', '=', $auction_result->id);
        })->orderByDesc('bids.price_bidding')
            ->select('users.*', 'bids.price_bidding')
            ->first();

        if ($user && $auction_result->highest_price > $auction_result->product->starting_price) {
            $user_name = $user->name;
        } else {
            $user_name = 'Không có ai đấu giá';
        }
        $data['user_name'] = $user_name;

        return view('auctions.auction_result_bid', $data);
    }

    public function send_email($id)
    {
        // Xử lí gửi mail
        $auction_result = Auction::with('product.seller')->findOrFail($id);
        $data['auction_result'] = $auction_result;
        $user = User::leftJoin('bids', function ($join) use ($auction_result) {
            $join->on('users.id', '=', 'bids.bidder_id')
                ->where('bids.auction_id', '=', $auction_result->id);
        })->orderByDesc('bids.price_bidding')
            ->select('users.*', 'bids.price_bidding')
            ->first();

        //-------------Mail Receipt-----------
        if ($user && $auction_result->highest_price > $auction_result->product->starting_price) {
            $mail_data = [
                
            ];
            Mail::to($user->email)->send(new ReceiptMail($mail_data, $auction_result->id));
        }
        //-----------------------------------------------------

        return response()->json(['message' => 'Email sent successfully'], 200);
        
    }
}
