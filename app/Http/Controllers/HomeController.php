<?php

namespace App\Http\Controllers;

use App\Models\Auction;
use App\Models\AuctionConfirm;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function home()
    {   
        $data = [];
    
        $current_date = date('Y-m-d');
        
        $auctions = Auction::with('product')
            ->where('start_time', '>', $current_date) // Lọc các sản phẩm có start_time lớn hơn ngày hiện tại
            ->get();
        
        $data['auctions'] = $auctions;
        
        return view('layouts.homepage', $data);
    }

    public function list(Request $request)
    {   
        $data = [];
        $current_date = date('Y-m-d'); // Lấy ngày hiện tại
        $data['current_date'] = $current_date;

        //----------------Xử lí phần search--------------
        $keyword = $request->input('keyword');
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
    
        $query = Auction::query();
        
        if ($keyword) {
            // Xử lí tìm kiếm theo từ khóa
            $query->whereHas('product', function ($query) use ($keyword) {
                $query->where('name', 'like', '%' . $keyword . '%');
            });
        }
    
        if ($from_date) {
            // Xử lí tìm kiếm theo từ ngày
            $query->where('start_time', '>=', $from_date);
        }
    
        if ($to_date) {
            // Xử lí tìm kiếm theo đến ngày
            $query->where('end_time', '<=', $to_date);
        }
        //---------------Kết thúc phần search-------------

        //---------------Xử lí phần filter---------------
        $status = $request->input('checkbox-status');
        $data['status'] = $status;

        if ($status) {
            // Xử lí filter theo trạng thái
            if ($status == 1) {
                // Lấy các đấu giá sắp diễn ra 
                $query->where('start_time', '>', $current_date);
            } elseif ($status == 2) {
                // Lấy các đấu giá đang diễn ra 
                $query->where('start_time', '<=', $current_date)
                    ->where('end_time', '>=', $current_date);
            } elseif ($status == 3) {
                // Lấy các đấu giá đã kết thúc 
                $query->where('end_time', '<', $current_date);
            }elseif ($status == 0) {
                
            }
        }
        //-------------Kết thúc phần filter-------------

        //---------------Xử lí phần My Auctions---------------      
        $status_my_auction = $request->input('checkbox');
        $data['status_my_auction'] = $status_my_auction;

        //Kiểm tra người dùng đã đăng nhập chưa và đăng kí phiên đấu giá nào chưa
        if (Auth::check()) {
            $user_login = Auth::user();
            $registered = AuctionConfirm::where('bidder_id', $user_login->id)
                ->where('status', 0)
                ->exists();
                $data['registered'] = $registered;
            if ($registered) {
                // Hiển thị các cuộc đấu giá người dùng đã đăng ký, có thể xử lí lọc theo các checkbox
                if ($status_my_auction == 4) {
                    // Hiển thị tất cả các cuộc đấu giá đã đăng ký
                    $query->whereHas('auction_confirms', function ($query) use ($user_login) {
                        $query->where('bidder_id', $user_login->id);
                    });
                } elseif ($status_my_auction == 5) {
                    // Hiển thị các cuộc đấu giá đã đăng ký và sắp diễn ra
                    $query->whereHas('auction_confirms', function ($query) use ($user_login, $current_date) {
                        $query->where('bidder_id', $user_login->id)
                            ->whereHas('auction', function ($query) use ($current_date) {
                                $query->where('start_time', '>', $current_date);
                            });
                    });
                } elseif ($status_my_auction == 6) {
                    // Hiển thị các cuộc đấu giá đã đăng ký và đang diễn ra
                    $query->whereHas('auction_confirms', function ($query) use ($user_login, $current_date) {
                        $query->where('bidder_id', $user_login->id)
                            ->whereHas('auction', function ($query) use ($current_date) {
                                $query->where('start_time', '<=', $current_date)
                                    ->where('end_time', '>=', $current_date);
                            });
                    });
                } elseif ($status_my_auction == 7) {
                    // Hiển thị các cuộc đấu giá đã đăng ký và đã kết thúc
                    $query->whereHas('auction_confirms', function ($query) use ($user_login, $current_date) {
                        $query->where('bidder_id', $user_login->id)
                            ->whereHas('auction', function ($query) use ($current_date) {
                                $query->where('end_time', '<', $current_date);
                            });
                    });
                }
            }else{
                $mess = 'Bạn chưa đăng ký cuộc đấu giá nào';
                $data['mess'] = $mess;
            }
        }
        //---------------Kết thúc phần My Auctions---------------  

        //Truyền dữ liệu vào view
        $lists = $query->with('product')->paginate(2);
        $lists->appends($request->query());
        $data['lists'] = $lists;    

        return view('auctions.auction_list',$data);
       
    }

}
