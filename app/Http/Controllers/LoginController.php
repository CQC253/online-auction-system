<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showForm()
    {
        return view('auth.login');
    }

    public function handleForm(LoginRequest $request)
    {
        $creadentials = $request->only('email','password');

        if (Auth::attempt($creadentials)) {
            //Authentication successful
            // Kiểm tra xem có URL chuyển hướng được lưu trong session hay không
            if ($request->session()->has('redirect_url')) {
                $redirectUrl = $request->session()->get('redirect_url');
                $request->session()->forget('redirect_url'); // Xóa URL chuyển hướng khỏi session
                return redirect()->to($redirectUrl);
            }
            return redirect()->intended('/');
        }else{
            //Authentication failed
            session()->flash('error','Email hoặc password không đúng.');
            return redirect()->back()->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    } 
}
