<?php
namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;
use App\Models\Auction;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    public function showProfile($id)
    {
        $data = [];
        $user = User::findOrFail($id);
        
        // Truyền dữ liệu người dùng vào view
        $data['user'] = $user; 

        return view('auth.profile', $data);
    }

    public function updateProfile(UpdateProfileRequest $request, $id)
    {
        $data = [];
        $user = User::findOrFail($id);
        
        $dataSave = [
            'birth_date' => $request->birth_date , 
            'phone' => $request->phone, 
            'email' => $request->email, 
            'address' => $request->address, 
            'balance' => $request->balance, 
        ];

        try {
            //dùng model User để lưu vào DB
            $user->update($dataSave);

            //write Log để điều tra khi cần
            Log::info('Updated Profile');

            //lưu DB ok
            return redirect()->route('show-profile',['id' => $id])->with('success', 'Cập nhật thông tin cá nhân thành công.');
            //with(): gọi là session flash. chỉ hiển thị trong 1 phiên làm việc
        } catch (Exception $exception) {
            //write Log để debug khi cần
            Log::error($exception->getMessage());

            //lưu DB FAIL
            return redirect()->route('show-profile',['id' => $id])->with('error', 'Cập nhật thông tin cá nhân thành công.');
        }
    }
}
