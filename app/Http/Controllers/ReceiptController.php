<?php
namespace App\Http\Controllers;

use App\Models\Auction;
use App\Models\Receipt;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReceiptController extends Controller
{
    public function showReceipt($id)
    {
        $data = [];
        $auction_detail = Auction::with('product')->findOrFail($id);
        $data['auction_detail'] = $auction_detail;
        $user_test = User::join('receipts', 'users.id', '=', 'receipts.bidder_id')
            ->where('receipts.auction_id', '=', $auction_detail->id)
            ->select('users.*', 'receipts.bidder_id')
            ->get();

        foreach ($user_test as $user) {
            $data['user'] = $user;
        }

        return view('emails.receipt', $data);
    }

    public function paymentReceipt($id)
    {
        $data = [];
        $auction_detail = Auction::with('product')->findOrFail($id);
        $data['auction_detail'] = $auction_detail;
        $user_test = User::join('receipts', 'users.id', '=', 'receipts.bidder_id')
            ->where('receipts.auction_id', '=', $auction_detail->id)
            ->select('users.*', 'receipts.bidder_id')
            ->get();

        $user = $user_test->first();
        if (!$user) {
            return redirect()->back()->with('error', 'Không tìm thấy người dùng');
        }
        $data['user'] = $user;

        $payment = Auction::findOrFail($id);
        $data['payment'] = $payment;


        // Kiểm tra đã thanh toán hay chưa
        $receipt = Receipt::where('auction_id', $auction_detail->id)->first();
        if ($receipt && $receipt->status == 1) {
            return redirect()->back()->with('success', 'Bạn đã thanh toán');
        }

        //Check balance
        if (!isset($user)) {
            return redirect()->back()->with('error', 'Không tìm thấy người dùng');
        }    
        $highest_price = $payment->highest_price;
        $balance = $user->balance;
        if ($balance < $highest_price) {
            return redirect()->back()->with('error', 'Số dư của bạn không đủ');
        }

        //Thanh toán
        DB::beginTransaction();
        try {
            //update balance
            $user->balance = $balance - $highest_price;
            $user->save();

            // Cập nhật trạng thái thanh toán
            if ($receipt) {
                $receipt->status = 1;
                $receipt->save();
            }

            DB::commit();

            //lưu DB ok
            session()->flash('success', 'Thanh toán thành công!!!');
            return redirect()->back();
            //with(): gọi là session flash. chỉ hiển thị trong 1 phiên làm việc
        } catch (Exception $exception) {
            //lưu DB FAIL
            DB::rollBack();

            // dd($exception->getMessage());
            session()->flash('error', 'Thanh toán thất bại!!!' . $exception->getMessage());
            return redirect()->back()->withInput();
        }
    }    
}
