<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function showForm()
    {
        return view('auth.register');
    }

    public function handleForm(RegisterRequest $request)
    {
        // Validate the user input
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        // Create a new user record
        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => bcrypt($validatedData['password']),
        ]);

        // Log in the user

        // You can use Laravel's built-in authentication here to log in the user automatically
        // You'll need to include the necessary authentication traits in your User model

        // For example:
        // auth()->login($user);

        // Or, you can redirect the user to the login page and prompt them to log in manually
        // You can customize this according to your specific requirements
        return redirect()->route('login')->with('success', 'Registration successful! Please log in to continue.');
    }
}
