<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ReceiptMail extends Mailable
{
    
    use Queueable, SerializesModels;

    protected $data;
    protected $id;

    /**
     * Create a new message instance.
     *
     * @param array $data
     */
    public function __construct(array $data, int $id)
    {
        $this->data = $data;
        $this->id = $id;
    }

    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Hóa đơn đấu giá 2QC Auction',
        );
    }


    public function content(): Content
    {
        return new Content(
            view: 'emails.mail_receipt' 
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }

    public function build(): void
    {
        $this->with(['data' => $this->data]); 
        $this->view('emails.mail_receipt', ['id' => $this->id]);
    }



}
