<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Auction extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'product_id',
        'time_open_registration',
        'time_close_registration',
        'registration_fee',
        'price_step',
        'max_step',
        'down_payment',
        'auction_method',
        'start_time',
        'end_time',
        'status',
        'highest_price',
    ];

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function bids(): HasMany
    {
        return $this->hasMany(Bid::class);
    }

    public function auction_confirms(): HasMany
    {
        return $this->hasMany(AuctionConfirm::class,'auction_id', 'id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function receipt()
    {
        return $this->hasOne(Receipt::class);
    }
}
