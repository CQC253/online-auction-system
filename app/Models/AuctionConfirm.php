<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AuctionConfirm extends Model
{
    use HasFactory;
    protected $table = 'auction_confirms';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'bidder_id',
        'auction_id',
        'status',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class,'bidder_id', 'id');
    }

    public function auction(): BelongsTo
    {
        return $this->belongsTo(Auction::class,'auction_id', 'id');
    }
}
