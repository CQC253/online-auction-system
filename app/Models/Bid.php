<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Bid extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'bidder_id',
        'auction_id',
        'price_bidding',
    ];

    public function bidder(): BelongsTo
    {
        return $this->belongsTo(User::class, 'bidder_id', 'id');
    }

    public function auction(): BelongsTo
    {
        return $this->belongsTo(Auction::class);
    }
}
