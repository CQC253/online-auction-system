<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'name',
    ];
    
    public function permission_role(): HasMany
    {
        return $this->hasMany(PermissionRole::class);
    }

    public function user_role(): HasMany
    {
        return $this->hasMany(UserRole::class);
    }

}
