<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('seller_id');
            $table->string('name');
            $table->unsignedBigInteger('starting_price');
            $table->text('description');
            $table->json('thumbnail')->nullable();
            $table->json('images')->nullable();
            $table->integer('status')->default(0); //0: sắp diễn ra, 1: đang diễn ra, 2: đã kết thúc
            $table->timestamps();

            // định nghĩa khoá ngoại (FK foreign key cho table products)
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('seller_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
