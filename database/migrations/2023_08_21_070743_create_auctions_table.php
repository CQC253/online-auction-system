<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->datetime('time_open_registration');
            $table->datetime('time_close_registration');
            $table->unsignedBigInteger('registration_fee');
            $table->unsignedBigInteger('price_step');
            $table->string('max_step')->default('Không giới hạn');
            $table->unsignedBigInteger('down_payment');
            $table->string('auction_method')->default('Đấu giá liên tục'); //Continuous Auction
            $table->datetime('start_time');
            $table->datetime('end_time');
            $table->integer('status')->nullable(); //0: đấu giá thất bại, 1: đấu giá thành công
            $table->unsignedBigInteger('highest_price')->nullable();
            $table->timestamps();

            // định nghĩa khoá ngoại (FK foreign key cho table auctions)
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('auctions');
    }
};
