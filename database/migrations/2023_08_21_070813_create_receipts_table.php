<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bidder_id')->nullable();
            $table->unsignedBigInteger('auction_id')->nullable();
            $table->integer('status')->default(0); //0: chưa thanh toán, 1: đã thanh toán
            $table->text('payment_method')->default('2QCPay');
            $table->timestamps();

            // định nghĩa khoá ngoại (FK foreign key cho table receipts)
            $table->foreign('bidder_id')->references('id')->on('users');
            $table->foreign('auction_id')->references('id')->on('auctions');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('receipts');
    }
};
