<?php

namespace Database\Seeders;

use App\Models\Auction;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuctionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('auctions')->insert([
            [
                'product_id' => 1,
                'time_open_registration' => '2023-09-25 15:00:00',
                'time_close_registration' => '2023-09-28 15:55:00',
                'registration_fee' => 200000,
                'price_step' => 5000000,
                'down_payment' => 60000000,
                'start_time' => '2023-09-29 15:57:00',
                'end_time' => '2023-09-30 15:58:00',
            ],
            [
                'product_id' => 2,
                'time_open_registration' => '2023-09-25 09:00:00',
                'time_close_registration' => '2023-09-28 09:00:00',
                'registration_fee' => 200000,
                'price_step' => 10000000,
                'down_payment' => 80000000,
                'start_time' => '2023-09-28 10:00:00',
                'end_time' => '2023-09-30 00:00:00',
            ],
            [
                'product_id' => 3,
                'time_open_registration' => '2023-08-10 09:00:00',
                'time_close_registration' => '2023-08-11 09:00:00',
                'registration_fee' => 200000,
                'price_step' => 10000000,
                'down_payment' => 50000000,
                'start_time' => '2023-08-20 10:00:00',
                'end_time' => '2023-08-21 00:00:00',
            ],
            [
                'product_id' => 4,
                'time_open_registration' => '2023-09-25 09:00:00',
                'time_close_registration' => '2023-09-28 09:00:00',
                'registration_fee' => 500000,
                'price_step' => 100000000,
                'down_payment' => 100000000,
                'start_time' => '2023-09-28 10:00:00',
                'end_time' => '2023-09-30 00:00:00',
            ],
        ]);
    }
}
