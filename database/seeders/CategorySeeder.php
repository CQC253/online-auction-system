<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            ['name' => 'Bất động sản'],
            ['name' => 'Phương tiện - Xe'],
            ['name' => 'Sưu tầm - Nghệ thuật'],
            ['name' => 'Hàng hiệu'],
            ['name' => 'Tài sản khác'],
        ]);
    }
}
