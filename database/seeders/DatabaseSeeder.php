<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);

        // \App\Models\User::factory(10)->create();

        
        // $staff = User::factory()->create([
        //     'name' => 'staff',
        //     'email' => 'staff@example.com',
        // ]);
        // $staff->roles()->sync([2]);
        
        // $system_admin = User::factory()->create([
        //     'name' => 'system_admin',
        //     'email' => 'system_admin@example.com',
        // ]);
        // $system_admin->roles()->sync([1]);

        $seller = User::factory()->create([
                'id' => 1,
                'name' => 'John Doe',
                'role' => 'seller',
                'address' => '123 Main St',
                'tel' => '555-1234',
                'email' => 'seller@example.com',
                'password' => Hash::make('password'),
                'balance' => 10000000000,
                'birthdate' => '1998/03/25',
        ]);
        $seller->roles()->sync([4]);
        
        $buyer_1 = User::factory()->create([
                'id' => 2,
                'name' => 'Jane Smith',
                'role' => 'bidder',
                'address' => '456 Elm St',
                'tel' => '555-5678',
                'email' => 'buyer1@example.com',
                'password' => Hash::make('password'),
                'balance' => 10000000000,
                'birthdate' => '1998/03/25',
        ]);
        $buyer_1->roles()->sync([3]);
        
        $buyer_2 = User::factory()->create([
                'id' => 3,
                'name' => 'Alice Witch',
                'role' => 'bidder',
                'address' => '123 Elm St',
                'tel' => '123-4567',
                'email' => 'opticalwar253@gmail.com',
                'password' => Hash::make('password'),
                'balance' => 10000000000,
                'birthdate' => '1998/03/25',
        ]);
        $buyer_2->roles()->sync([3]);

        

        $this->call(CategorySeeder::class);
        // $this->call(UserSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(AuctionSeeder::class);        
        $this->call(BidSeeder::class);        
        $this->call(CommentSeeder::class);     
        $this->call(ReceiptSeeder::class);

        
    }
}
