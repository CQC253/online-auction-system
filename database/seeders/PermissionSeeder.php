<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('permissions')->insert([
            ['name' => 'View'],
            ['name' => 'Login'],
            ['name' => 'Logout'],
            ['name' => 'Manage profile'],
            ['name' => 'Change password'],
            ['name' => 'Forget password'],
            ['name' => 'Manage my auction'],
            ['name' => 'Manage my credit account'],
            ['name' => 'Create new auction'],
            ['name' => 'Cancle auction'],
            ['name' => 'Manage my bidding'],
            ['name' => 'Make a bid'],
            ['name' => 'Start auction'],
            ['name' => 'Close auction'],
            ['name' => 'Auction management'],
            ['name' => 'Product management'],
            ['name' => 'Customer management'],
            ['name' => 'Statistic and Reports'],
            ['name' => 'create'],
            ['name' => 'edit'],
            ['name' => 'delete'],
        ]);
    }
}
