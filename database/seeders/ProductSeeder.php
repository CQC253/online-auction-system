<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('products')->insert([
            [
                'category_id' => 5, // ID của category tương ứng
                'seller_id' => 1, // ID của seller tương ứng
                'name' => 'Electronic',
                'starting_price' => '100000000',
                'description' => 'Description of Product 1',
                'thumbnail' => json_encode([ 'https://www.collegechalo.com/news/wp-content/uploads/2021/12/Electronics-Engineering.jpg', ]),            
                'images' => json_encode([
                    'https://ugc.futurelearn.com/uploads/images/4b/e0/header_4be055c5-f980-4119-9a4f-d2fa5c5294b1.jpg',
                    'https://www.york.ac.uk/media/study/courses/undergraduate/electronics/Yellow-blue-circuit-MEng-EE.jpg',
                    'https://cgu-odisha.ac.in/wp-content/uploads/2023/05/electronic-engineering-1.jpg',
                    'https://dipslab.com/wp-content/uploads/2019/04/Electronic-Circuit.jpg',
                ])
            ],
            [
                'category_id' => 4, // ID của category tương ứng
                'seller_id' => 1, // ID của seller tương ứng
                'name' => 'Clothes',
                'starting_price' => '200000000',
                'description' => 'Description of Product 2',
                'thumbnail' => json_encode([ 'https://cf.shopee.com.my/file/99e35b125f552d38cb55e2e932a8c90b', ]), 
                'images' => json_encode([
                    'https://cdn.shopify.com/s/files/1/0275/1060/3843/products/product-image-1652322946_2000x.jpg?v=1630088756',
                    'https://m.media-amazon.com/images/I/61D-VuDZx+L._AC_SX522_.jpg',
                    'https://i5.walmartimages.com/asr/b2e89999-da94-4b56-92a7-848bcc4ee292.efa15a56cb2ec17bc1cc22bbc462db9b.jpeg',
                    'https://cdn-amz.woka.io/images/I/71DjBoanJKL.jpg',
                ])
            ],
            [
                'category_id' => 1, // ID của category tương ứng
                'seller_id' => 1, // ID của seller tương ứng
                'name' => 'Building',
                'starting_price' => '300000000',
                'description' => 'Description of Product 3',
                'thumbnail' => json_encode([ 'https://www.cnnphilippines.com/.imaging/default/dam/cnn/2022/9/8/AI-Future-Cities_CNNPH.jpg/jcr:content.jpg?width=750&height=450&crop:1:1,smart', ]),
                'images' => json_encode([
                    'https://i.pinimg.com/1200x/e3/8f/61/e38f61b7bf959d2df94294d3dada1308.jpg',
                    'https://ychef.files.bbci.co.uk/1280x720/p0db81jf.jpg',
                    'https://www.aurecongroup.com/-/media/images/aurecon/content/insights/botf_featured-insights_buildings-of-the-future/building-of-the-future-desktop-v2.jpg?as=0&w=2048',
                    'https://images.adsttc.com/media/images/649c/35f8/5921/186b/f076/0674/medium_jpg/coeru-shibuya-offices-and-commercial-building-maeda-corporation_11.jpg?1687959062',
                ])
            ],
            [
                'category_id' => 2, // ID của category tương ứng
                'seller_id' => 1, // ID của seller tương ứng
                'name' => 'Cars',
                'starting_price' => '4000000000',
                'description' => 'Description of Product 4',
                'thumbnail' => json_encode([ 'https://ichef.bbci.co.uk/news/976/cpsprodpb/156EF/production/_120419778_mercedes.jpg', ]),
                'images' => json_encode([
                    'https://images.drive.com.au/driveau/image/upload/c_fill,f_auto,g_auto,h_1080,q_auto:eco,w_1920/v1/cms/uploads/jjslyagf8e3gcny2doyy',
                    'https://www.topgear.com/sites/default/files/2022/07/6_0.jpg',
                    'https://imageio.forbes.com/specials-images/imageserve/5d35eacaf1176b0008974b54/0x0.jpg?format=jpg&crop=4560,2565,x790,y784,safe&width=1200',
                    'https://cdn.arstechnica.net/wp-content/uploads/2023/04/IMG_2117-800x533.jpg',
                ])
            ],
        ]);
    }
}
