<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('roles')->insert([
            ['id' => 1,'name' => 'system_admin'],
            ['id' => 2,'name' => 'staff'],
            ['id' => 3,'name' => 'buyer'],
            ['id' => 4,'name' => 'seller'],
        ]);
    }
}
