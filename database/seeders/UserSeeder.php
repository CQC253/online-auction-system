<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'John Doe',
                'role' => 'seller',
                'address' => '123 Main St',
                'tel' => '555-1234',
                'email' => 'john@example.com',
                'password' => Hash::make('password'),
                'balance' => 10000000000,
                'birthdate' => '1998/03/25',
            ],
            [
                'id' => 2,
                'name' => 'Jane Smith',
                'role' => 'bidder',
                'address' => '456 Elm St',
                'tel' => '555-5678',
                'email' => 'jane@example.com',
                'password' => Hash::make('password'),
                'balance' => 10000000000,
                'birthdate' => '1998/03/25',
            ],
            [
                'id' => 3,
                'name' => 'Alice Witch',
                'role' => 'bidder',
                'address' => '123 Elm St',
                'tel' => '123-4567',
                'email' => 'opticalwar253@gmail.com',
                'password' => Hash::make('password'),
                'balance' => 10000000000,
                'birthdate' => '1998/03/25',
            ],
        ]);
    }
}
