function openModalLogin() {
    $('#loginModal').modal('show');
}
function closeModalLogin() {
    $('#loginModal').modal('hide');
};
//function updateClock() {
//    var now = new Date(currentServerDatetimeValue);
//    var day = now.getDay(),
//        sec = now.getSeconds(),
//        min = now.getMinutes(),
//        hou = now.getHours(),
//        mo = now.getMonth(),
//        dy = now.getDate(),
//        yr = now.getFullYear();
//    var days = ["Thứ Hai", "Thứ Ba", "Thứ Tư", "Thứ Năm", "Thứ Sáu", "Thứ Bảy", "Chủ nhật"];
//    var months = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
//    var date = dy + "/" + months[mo] + "/" + yr;
//    var time = hou.toString().padStart(2, '0') + ":" + min.toString().padStart(2, '0') + ":" + sec.toString().padStart(2, '0');
//    var tags1 = ["dayHeader", "dateHeader", "timeHeader"],
//        corr1 = [days[day - 1], date, time];
//    for (var i = 0; i < tags1.length; i++) {
//        document.getElementById(tags1[i]).firstChild.nodeValue = corr1[i];
//    }
//}

//function initClock() {
//    updateClock();
//    window.setInterval("updateClock()", 1);
//}
//initClock();
$(document).ready(function () {
    //Harry update 0806 - 1 lúc chỉ 1 user
    loadServerDateTimeValue();
});

jQuery('#BackLoginForm').on("click", function () {
    $("#myModal").css("display", "none");
    document.getElementById("modal-log-in").setAttribute('style', 'display:block !important');
    document.getElementById("tabContent").setAttribute('style', 'display:block !important');
    document.getElementById("navTabs").setAttribute('style', 'display:block !important');
});
jQuery('#lostPassword-modal').on("click", function () {
    document.getElementById("tabContent").setAttribute('style', 'display:none !important');
    document.getElementById("navTabs").setAttribute('style', 'display:none !important');
});
var currentLoggingInUser;
var OTPchecking = 0;
function Login(e) {
    e.preventDefault();
    if (OTPchecking == 1) {
        OTPVerify(currentLoggingInUser);
    }

    //debugger;
    var Username = $("#user_login").val();
    var Password = $("#user_pass").val();
    if (Username.length == 0 && Password.length == 0 || Username.length == 0 && Password.length < 6) {
        closeModalLogin();
        Swal.fire({
            title: 'Đăng nhập không thành công',
            text: 'Hãy nhập đầy đủ tên đăng nhập và mật khẩu!',
            icon: 'info',
            showCancelButton: false,
            confirmButtonText: 'OK'
        }).then((result) => {
            $("#loginModal").show();
            if (result.value) {

            }
        })
    }
    else if (Username.length == 0 && Password.length < 6) {
        closeModalLogin();
        Swal.fire({
            title: 'Đăng nhập không thành công',
            text: 'Mật khẩu ít nhất là 8 kí tự!',
            icon: 'info',
            showCancelButton: false,
            confirmButtonText: 'OK'
        }).then((result) => {
            $("#loginModal").show();
            if (result.value) {

            }
        })
    }
    else {
        var obj = {
            "id": 1,
            "accountTypeId": 1000001,
            "accountStatusId": 1000001,
            "roleId": 1000001,
            "active": 1,
            "name": "",
            "email": "",
            "username": $("#user_login").val(),
            "password": $("#user_pass").val(),
            "photo": "",
            "description": "",
            "info": ""
        };

        currentLoggingInUser = obj;
        //debugger;
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: '/Account/api/Login',
            data: JSON.stringify(obj),
            success: function (responseData) {
                //debugger;
                //responseData = JSON.parse(responseData)
                if (responseData.status == 1 && responseData.message == "BLOCKED") {
                    closeModalLogin();
                    Swal.fire({
                        title: 'Tài khoản của bạn đã bị khoá!',
                        text: 'Tài khoản của bạn đã bị khoá do chưa nộp phí duy trì tài khoản thường niên. Vui lòng liên hệ 0243.211.5234 để được hỗ trợ.',
                        icon: 'warning',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        window.location.reload();
                    })
                }
                else if (responseData.status == 200 && responseData.message === "SUCCESS" && responseData.data[0].isActivated == 0) {
                    closeModalLogin();
                    Swal.fire({
                        title: 'Đăng nhập thành công',
                        text: 'Hãy kích hoạt Email: ' + responseData.data[0].email + ' để đấu giá tài sản!',
                        footer: "Lưu ý: Website hoạt động tốt nhất với trình duyệt Google Chrome.",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        var currentUrl = window.location.href;
                        if (window.location.pathname == "/Register" || window.location.pathname == "/dang-ky" || window.location.pathname == "/confirmEmail") {
                            currentUrl = 'https://lacvietauction.vn/';
                        }
                        window.location.href = currentUrl;
                        if (result.value) {
                            //alert('login success');
                        }
                    })
                }
                else if (responseData.status == 200 && responseData.message === "SUCCESS") {
                    localStorage.setItem("currentLoggedInUser", JSON.stringify(responseData.data[0]));
                    closeModalLogin();
                    Swal.fire({
                        title: 'Đăng nhập thành công!',
                        text: 'Xin chào, ' + responseData.data[0].name + '!',
                        footer: "Lưu ý: Website hoạt động tốt nhất với trình duyệt Google Chrome.",
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        var currentUrl = window.location.href;
                        if (window.location.pathname == "/Register" || window.location.pathname == "/dang-ky" || window.location.pathname == "/confirmEmail") {
                            currentUrl = 'https://lacvietauction.vn/';
                        }
                        if (window.location.pathname.substring(window.location.pathname.indexOf("/"), window.location.pathname.lastIndexOf("/")) == "//AuctionPropertyDetail" || window.location.pathname.substring(window.location.pathname.indexOf("/"), window.location.pathname.lastIndexOf("/")) == "//tai-san-dau-gia" || window.location.pathname.substring(window.location.pathname.indexOf("/"), window.location.pathname.lastIndexOf("/")) == "/AuctionPropertyDetail" ||
                            window.location.pathname.substring(window.location.pathname.indexOf("/"), window.location.pathname.lastIndexOf("/")) == "/tai-san-dau-gia"
                        ) {
                            window.location.reload();
                        }
                        if (window.location.pathname == "/403.html") {
                            window.location.href = currentUrl;
                        }
                        else {
                            window.location.href = currentUrl;
                        }
                        if (result.value) {
                            //alert('login success');
                        }
                    })
                }

                else if (responseData.status == 200 && responseData.message === "OTP_REQUIRED") {
                    OTPchecking = 1;

                    obj.id = responseData.data[0].id;
                    //alert('otp required!');
                    OTPVerify(obj);
                }
            },
            error: function (e) {
                //console.log(e.message);
                closeModalLogin();
                Swal.fire({
                    title: 'Đăng nhập không thành công',
                    text: 'Tài khoản đăng nhập hoặc mật khẩu không chính xác!',
                    icon: 'error',
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                }).then((result) => {
                    openModalLogin();
                });
            }
        });
    }

}
function Logout() {
    Swal.fire({
        title: 'Bạn có muốn đăng xuất?',
        text: 'Lạc Việt Auction',
        icon: 'question',
        showCancelButton: true,
        cancelButtonColor: '#443',
        confirmButtonText: 'Xác nhận',
        cancelButtonText: 'Hủy'
    }).then((result) => {
        if (result.value) {
            localStorage.clear();
            window.location.href = "/Logout";
        }
    })
}
function OTPLog(accountId, OTPCode, Description) {
    var IP = "";
    var windows = "";
    let apiKey = '1be9a6884abd4c3ea143b59ca317c6b2';
    $.getJSON('https://ipgeolocation.abstractapi.com/v1/?api_key=' + apiKey, function (data) {
        IP = data.ip_address;
    });
    var isMobilePC = isMobile();
    var browser = checkBrowser();
    var browserName = browser.split("-");
    var userAgentData = navigator.userAgent;
    var browserData = userAgentData.split(" ");
    var getBrowserVersion = "";
    for (var i = 0; i < browserData.length; i++) {
        if (browserData[i].includes(browserName[0])) {
            getBrowserVersion = browserData[i];
        }
    }
    getBrowserVersion = getBrowserVersion.split("/");
    var version = getBrowserVersion[1].split(".");
    var info = "";
    var isFlash = "no";
    if (navigator.mimeTypes[
        'application/x-shockwave-flash'] != undefined) {
        isFlash = "yes";
    }
    var getVersion = window.navigator.appVersion.split("NT")[1].split(";")[0].trim();
    getVersion = getVersion.split(".");
    windows = getVersion[0];
    if (isMobilePC == "MOBILE") {
        info = "OS: Windows " + windows + " Browser: " + getBrowserVersion[0] + " " + version[0] + " (" + getBrowserVersion[1] + ") Mobile: true Flash: " + isFlash + " check Cookies: true Screen Size: " + window.screen.width + " x " + window.screen.height + " Full User Agent: " + navigator.userAgent;
    } else {
        info = "OS: Windows " + windows + " Browser: " + getBrowserVersion[0] + " " + version[0] + " (" + getBrowserVersion[1] + ") Mobile: false Flash: " + isFlash + " check Cookies: true Screen Size: " + window.screen.width + " x " + window.screen.height + " Full User Agent: " + navigator.userAgent;
    }
    var updateObj = {
        "Active": 1,
        "AccountId": accountId,
        "GuId": 1,
        "Name": OTPCode,
        "EntityCode": "ENTEROTP",
        "Info": info,
        "ObjectOld": "",
        "ObjectNew": "",
        "Url": window.location.href,
        "UrlSource": window.location.href,
        "IpAddress": IP,
        "Device": isMobilePC,
        "Browser": browser,
        "OS": "",
        "UserAgent": navigator.userAgent,
        "Description": Description
    };
    $.ajax({
        url: "/ActivityLog/api/add",
        method: "POST",
        contentType: "application/json",
        data: JSON.stringify(updateObj),
        success: function (responseData) {
            if (responseData.status == 200 && responseData.message == "SUCCESS") {
                console.log("OTP LOG");
            }
        },
        error: function (e) {
            console.log(e.message);
        }

    })
}
function checkBrowser() {
    navigator.sayswho = (function () {
        var ua = navigator.userAgent;
        var tem;
        var M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE ' + (tem[1] || '');
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();

    var output = navigator.sayswho;
    return output.replace(" ", "-");
}
function isMobile() {
    var isMobile = "PC"; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isMobile = "MOBILE";
    }
    return isMobile;
}
function OTPVerify(obj) {
    Swal.fire({
        title: 'Mã xác thực OTP',
        text: 'Mã xác thực OTP đã được gửi về địa chỉ email đăng ký của bạn.',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        //showCancelButton: true,
        confirmButtonText: 'Xác nhận',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then((result) => {
        console.log(result);
        if (result.value.length == 6 && result.isConfirmed) {
            //Call ajax check OTP
            obj.info = result.value;

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: '/Account/api/OTPVerify',
                data: JSON.stringify(obj),
                success: function (responseData) {
                    if (responseData.status == 200 && responseData.message === "SUCCESS") {
                        OTPLog()
                        localStorage.setItem("currentLoggedInUser", JSON.stringify(responseData.data[0]));
                        OTPchecking = 0;
                        Swal.fire({
                            title: 'Đăng nhập thành công!',
                            html: 'Xin chào, ' + responseData.data[0].name + '!',
                            footer: "Lưu ý: Website hoạt động tốt nhất với trình duyệt Google Chrome.",
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            var currentUrl = window.location.href;
                            if (window.location.pathname == "/Register" || window.location.pathname == "/dang-ky" || window.location.pathname == "/confirmEmail") {
                                currentUrl = 'https://lacvietauction.vn/';
                            }
                            window.location.href = currentUrl;
                            if (result.value) {
                                //alert('login success');
                            }
                        })
                    }
                },
                error: function (e) {
                    //console.log(e.message);
                    document.getElementById("modal-log-in").setAttribute('style', 'display:none !important');
                    Swal.fire({
                        title: 'Đăng nhập không thành công',
                        text: 'Tài khoản đăng nhập hoặc mật khẩu không chính xác!',
                        icon: 'error',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        $("#modal-log-in").show();
                    });
                }
            });

        }
        else {
            Swal.fire({
                title: 'Mã OTP của bạn không hợp lệ!',
                text: 'Vui lòng kiểm tra lại mã OTP đã được gửi đến địa chỉ email đăng ký của bạn!',
                icon: 'error',
                showCancelButton: false,
                confirmButtonText: 'OK',
                showCancelButton: false,
                allowOutsideClick: false
            }).then((result) => {
                //recursive gọi lại check OTP
                OTPVerify(obj);
            });
        }
    })
}
$('#user_login, #user_pass').keypress(function (e) {
    if (e.key === 'Enter') {
        Login(e);
    }
});
function daysInMonth(m, y) {
    switch (m) {
        case 2:
            return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
        case 4: case 6: case 9: case 11:
            return 30;
        default:
            return 31
    }
}
function isValid(d, m, y) {
    return m > 0 && m < 13 && d > 0 && d <= daysInMonth(m, y);
}
function checkDate(d, m, y) {
    var date = new Date(y, m - 1, d);
    if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {
        return true
    }
    return false
}

function CheckForgotPassword() {
    debugger;
    var checkType = $("#emailForgotPassword").val();
    var filterEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var filterPhonenumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;

    if (filterEmail.test($("#emailForgotPassword").val()) == true) {
        ForgotPassword();
    }

    else if (filterPhonenumber.test($("#emailForgotPassword").val()) == true) {
        ForgotPasswordNumber();
    } else {
        $("#myModal").css("display", "none");
        Swal.fire({
            title: 'Số điện thoại hoặc email chưa đúng định dạng!',
            icon: 'info',
            showCancelButton: false,
            confirmButtonText: 'OK'
        }).then((result) => {
            $("#myModal").show();
        });
    }
}
function ForgotPassword() {
    //debugger;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if ($("#emailForgotPassword").val().length < 1) {
        $("#myModal").css("display", "none");
        Swal.fire({
            title: 'Email chưa được nhập!',
            icon: 'info',
            showCancelButton: false,
            confirmButtonText: 'OK'
        }).then((result) => {
            $("#myModal").show();
        });
    }
    else if (!filter.test($("#emailForgotPassword").val())) {
        $("#myModal").css("display", "none");
        Swal.fire({
            title: 'Email nhập không đúng định dạng!',
            icon: 'info',
            showCancelButton: false,
            confirmButtonText: 'OK'
        }).then((result) => {
            $("#myModal").show();
        });
    }
    else {
        //debugger;
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/Account/api/SendEmailForgotPassword/" + $("#emailForgotPassword").val(),
            success: function (responseData) {
                //debugger;
                if (responseData.status == 200 && responseData.message === "SUCCESS") {
                    $("#myModal").css("display", "none");
                    Swal.fire({
                        title: 'Thành công!',
                        text: 'Email đã được gửi, vui lòng kiểm tra hộp thư để cập nhật thông tin.',
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        window.location.reload();
                    })
                }
                if (responseData.status == 1 && responseData.message === "EMAIL_INCORRECT") {
                    $("#myModal").css("display", "none");
                    Swal.fire({
                        title: 'Email không tồn tại, vui lòng thử lại!',
                        icon: 'info',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        $("#myModal").show();
                    });
                }
            },
            error: function (e) {
                $("#myModal").css("display", "none");
                Swal.fire({
                    title: 'Có lỗi xảy ra vui lòng thử lại!',
                    icon: 'info',
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                }).then((result) => {
                    $("#myModal").show();
                });
            }
        });
    }

}
function ForgotPasswordNumber() {
    var filter = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;

    if ($("#emailForgotPassword").val().length < 10) {
        $("#myModal").css("display", "none");
        Swal.fire({
            title: 'Số điện thoại chưa đúng định dạng!',
            icon: 'info',
            showCancelButton: false,
            confirmButtonText: 'OK'
        }).then((result) => {
            $("#myModal").show();
        });
    }
    else if (!filter.test($("#emailForgotPassword").val())) {
        $("#myModal").css("display", "none");
        Swal.fire({
            title: 'Số điện thoại nhập không đúng định dạng!',
            icon: 'info',
            showCancelButton: false,
            confirmButtonText: 'OK'
        }).then((result) => {
            $("#myModal").show();
        });
    }
    else {
        //debugger;
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/Account/api/SendEmailForgotPasswordPhone/" + $("#emailForgotPassword").val(),
            success: function (responseData) {
                //debugger;
                if (responseData.status == 200 && responseData.message === "SUCCESS") {
                    $("#myModal").css("display", "none");
                    Swal.fire({
                        title: 'Thành công!',
                        text: 'Mã OTP đã được gửi, vui lòng kiểm tra mã OTP trong tin nhắn để cập nhật thông tin.',
                        icon: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        window.location.href = "/doi-mat-khau/" + "reset_password_otp";
                    })
                }
                if (responseData.status == 1 && responseData.message === "PHONE_INCORRECT") {
                    $("#myModal").css("display", "none");
                    Swal.fire({
                        title: 'Số điện thoại không tồn tại, vui lòng thử lại!',
                        icon: 'info',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        $("#myModal").show();
                    });
                }
                if (responseData.status == 1 && responseData.message === "NOTVERIFY") {
                    $("#myModal").css("display", "none");
                    Swal.fire({
                        title: 'Tài khoản chưa được kích hoạt trên hệ thống',
                        icon: 'info',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        $("#myModal").show();
                    });
                }
                if (responseData.status == 204 && responseData.message === "OTP_EXIST") {
                    $("#myModal").css("display", "none");
                    Swal.fire({
                        title: 'Mã OTP đã tồn tại vui lòng kiểm tra lại',
                        icon: 'info',
                        showCancelButton: false,
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        $("#myModal").show();
                    });
                }
            },
            error: function (e) {
                $("#myModal").css("display", "none");
                Swal.fire({
                    title: 'Có lỗi xảy ra vui lòng thử lại!',
                    icon: 'info',
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                }).then((result) => {
                    $("#myModal").show();
                });
            }
        });
    }

}

$(document).ready(function () {
    //Tên đăng nhập tiếng Việt không dấu
    $("#reg_username").keyup(function () {
        $(this).val($(this).val().toLowerCase().replace(/[^a-zA-Z0-9]+/g, ""));
    });
});
function TabSelectAccount(tabname) {
    var currentUrl = window.location.href;
    if (!currentUrl.includes('/thong-tin-ca-nhan')) {
        window.location.href = "/thong-tin-ca-nhan" + tabname;
    }
    else {
        if (tabname == '#tai-lieu-cua-toi') {
            $("#v-pills-purchase-tab").click();
        }
        else if (tabname == '#lich-bieu') {
            $("#v-pills-dashboard-tab").click();
        }
        else if (tabname == '#thong-bao') {
            $("#v-pills-notification-tab").click();
        }
        else if (tabname == '#lich-su-dau-gia') {
            $("#v-pills-order-tab").click();
        }
        else if (tabname == '#ho-so') {
            $("#v-pills-profile-tab").click();
        }
        else if (tabname == '#dau-gia-cua-toi') {
            $("#v-pills-myauction-tab").click();
        }
    }
}

document.getElementById('key-searching').onkeypress = function (e) {
    if (!e) e = window.event;
    var keyCode = e.code || e.key;
    if (keyCode == 'Enter') {
        window.location.href = '/tim-kiem-tai-san/' + $("#key-searching").val();
    }
}

function FilterMobile() {
    var textSearch = $("#key-searching-mobile").val();
    if (textSearch.length == 0) {
        Swal.fire({
            title: 'Vui lòng nhập từ khóa tìm kiếm',
            icon: 'warning',
        })
    }
    else {
        window.location.href = '/tim-kiem-tai-san/' + textSearch;
    }
}
function changeLanguage(lang, returnUrl) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: '/ChangeLanguage?culture=' + lang + '&returnUrl=' + returnUrl,
        success: function (responseData) {
            localStorage.setItem("language", lang);
            window.location.href = returnUrl;
        },
        error: function (e) {

        }
    });
}

var hrefloc = window.location.href;
        if (hrefloc.includes("danh-muc-tai-san")) {
            $("#banner-new").css("padding-top", "82px");
            $("#banner-new").css("padding-bottom", "82px");
        }
        else if (hrefloc.includes("dang-ky-dau-gia") || (hrefloc.includes("ket-qua-dau-gia") && !hrefloc.includes("danh-muc-tin-tuc") && !hrefloc.includes("chi-tiet-tin-tuc"))) {
            $("#banner-new").css("padding-top", "79px");
            $("#banner-new").css("padding-bottom", "85px");
        }
        //else if (hrefloc.includes("tai-san-dau-gia") || hrefloc.includes("cuoc-dau-gia")) {
        //    if ($(window).width() < 768) {
        //        $(".current-page").css("font-size", "16px");

        //    }
        //}

        $(window).on("load", function () {
            var app = '';
            if(app == "APP"){
                $("#divHeader").css("display","none");
                $("#divFooter").css("display", "none");
            }
        });
        function hrefShare() {
            var href = "https://www.facebook.com/sharer/sharer.php?u=https://lacvietauction.vn//tai-san-dau-gia/1002395-accu-vien-thong-da-qua-su-dung-cu,-hong,-khong-con-su-dung-thu-hoi-tai-cac-kho-cua-vien-thong-hoa-binh&picture=https://data.lvo.vn/media/upload/1001406/IMAGE/Năm 2023/VT Hòa Bình/5.jpg&description=Accu viễn thông đã qua sử dụng cũ, hỏng, không còn sử dụng thu hồi tại các kho của Viễn thông Hoà Bình";
            window.open(href, "mywindow", "location=0,menubar=0,status=0,scrollbars=0,width=650,height=691");
        }

        const notification = document.querySelector('.notification');
        $(document).ready(function () {
    
            if ((parseInt("0") == 1000006 || parseInt("0") == 1000005 ) && "" == "0" && location.href.indexOf('Checkout') < 0) {
                showMessage();
    
            }
            var source = "";
            if (source == "APP") {
                $("#notificationForm").css("display", "none");
            }
        })
        // function called when the button to dismiss the message is clicked
        function dismissMessage() {
            // remove the .received class from the .notification widget
            notification.classList.remove('received');
            $("#notificationForm").attr("style", "display:none !important");
        }
        // function showing the message
        function showMessage() {
    
            if (localStorage.getItem("SHOW_ANNUAL_FEE_ALERT") !== null) {
                return;
            }
    
            Swal.fire({
                title: 'Thông báo',
                text: "Bạn chưa hoàn thành tiền phí duy trì tài khoản thường niên.",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                cancelButtonText: 'Thanh toán sau',
                confirmButtonText: 'Đến trang thanh toán',
                footer: '<a style="font-size:13px" href="https://drive.google.com/file/d/1HWwAXibxbiN9V_2VmoEAazT052IM2NK-/view">Quy chế hoạt động của Cổng đấu giá Lạc Việt</a>'
            }).then((result) => {
                //stop showing this message next time
                localStorage.setItem("SHOW_ANNUAL_FEE_ALERT", "NO");
                if (result.value) {
                    window.location.href = "/thong-tin-ca-nhan#ho-so";
                }
            })
            return;
    
            $("#notificationForm").attr("style", "display:block !important;z-index: 99999");
            // add a class of .received to the .notification container
            notification.classList.add('received');
    
            // attach an event listener on the button to dismiss the message
            // include the once flag to have the button register the click only one time
            const button = document.querySelector('.notification__message button');
            button.addEventListener('click', dismissMessage, { once: true });
        }

        function loadServerDateTimeValue() {
            $.ajax({
                //url: "https://lacvietauction.vn//GetServerDateTimeValue",
                url: "https://lacvietauction.vn//GetServerCurrentTimestamp",
                type: "GET",
                success: function (responseData) {
        
                    if (responseData.includes("SESSION_ENDED_FOR_USER_")) {
                        forceLogout();
                        return;
                    }
                    else {
                        currentServerDatetimeValue = new Date(responseData);
                        currentServerTimestampValue = currentServerDatetimeValue.getTime();
                    }
                    var currentGetDate = new Date();
                    //debugger;
        
                    //currentServerTimestampValue = parseInt(responseData);
                    //currentServerDatetimeValue = new Date(currentServerTimestampValue);
                },
                error: function (e) {
                    console.log(e.message);
                    console.log("Failed when load server time")
                }
            })
            }
            var btn = $('#back-to-top');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    btn.addClass('show');
                    $(".zalo-chat-widget").removeClass("zalo-back-bottom");
        
                } else {
                    btn.removeClass('show');
                    $(".zalo-chat-widget").addClass("zalo-back-bottom");
                }
            });
            btn.on('click', function (e) {
                e.preventDefault();
                $('html').animate({ scrollTop: 0 }, '100');
            });

            $(document).ready(function() {
                var app = '';
                if (app == "APP") {
                    $("header").css("display", "none");
                    $("footer").css("display", "none");
                    $("#banner-new").css("display", "none");
                }
                $("body").css("display","block");
            });

            function formatNumber() {
                $(".column-price , .column-openingPrice , .column-value , .novaticPrice , .column-amount, .column-available, .column-balance1 , .column-biddingValue, .column-stepPrice, #realTimeHighestPrice, .realTimeHighestPrice, #openingPrice-value, #stepPrice-value, #highestPrice").each(function (n) {
                    $(this).text(function (n, t) {
                        return t.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                    })
                }), $(".input-price,.input-depositPrice,.input-openingPrice,.input-buyPrice,.input-stepPrice , .input-value, .input-novaticPrice , .input-balance, .input-available, .input-auctionPropertyRegisterFee, .input-registerFee").each(function (n) {
                    $(this).attr("type", "text"), $(this).val(function (n, t) {
                        return t.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                    })
                }), $(".column-createdTime , .column-publishedTime , .column-openTime , .column-closedTime, .column-OpenTime, .column-ClosedTime, .column-biddingCreatedTime,#closedTime-value").each(function (n) {
                    var t = $(this).text();
                    if (t.includes("T")) {
                        var e = new Date(t);
                        e.toLocaleDateString(), e.toLocaleTimeString();
                        $(this).text(formatDisplayTimeFromDBTime(t))
                    }
                }), $(".input-createdTime , .input-publishedTime").each(function (n) {
                    var t = $(this).find("input"),
                        e = t.val();
                    if (e.includes("T")) {
                        var i = new Date(e),
                            r = i.toLocaleDateString() + " " + i.toLocaleTimeString();
                        t.val(r)
                    }
                })
            }

            function logElementEvent(eventName, element) {
                //console.log(Date.now(), eventName, element.getAttribute("data-src"));
            }
        
            var callback_enter = function (element) {
                logElementEvent("🔑 ENTERED", element);
            };
            var callback_exit = function (element) {
                logElementEvent("🚪 EXITED", element);
            };
            var callback_loading = function (element) {
                logElementEvent("⌚ LOADING", element);
                if (!element.dataset.src != '') {
                    element.src = "/files/backend/upload/istockphoto-931643150-612x612.jpg";
                }
                else {
                    element.src = "/files/backend/upload/loading.gif";
                }
            };
            var callback_loaded = function (element) {
                logElementEvent("👍 LOADED", element);
                if (element.dataset.src == '') {
                    element.src = "/files/backend/upload/istockphoto-931643150-612x612.jpg";
                }
                else {
                    element.onerror = function () {
                        this.src = '/files/backend/upload/istockphoto-931643150-612x612.jpg'; // place your error.png image instead
                    };
                    element.src = element.dataset.src;
                }
            };
            var callback_error = function (element) {
                logElementEvent("💀 ERROR", element);
                element.src = "/files/backend/upload/istockphoto-931643150-612x612.jpg";
            };
            var callback_finish = function () {
                logElementEvent("✔️ FINISHED", document.documentElement);
            };
            var callback_cancel = function (element) {
                logElementEvent("🔥 CANCEL", element);
            };
            var ll = new LazyLoad({
                class_applied: "lz-applied",
                class_loading: "lz-loading",
                class_error: "lz-error",
                class_loaded: "lz-loaded",
                class_entered: "lz-entered",
                class_exited: "lz-exited",
                // Assign the callbacks defined above
                callback_enter: callback_enter,
                callback_exit: callback_exit,
                callback_cancel: callback_cancel,
                callback_loading: callback_loading,
                callback_error: callback_error,
                callback_loaded: callback_loaded,
                callback_finish: callback_finish
            });

            //Lấy VietQRCode
            $(document).ready(function () {
                //By default, plugin uses `data-fancybox-group` attribute to create galleries.
            $(".fancybox").jqPhotoSwipe({
                galleryOpen: function (gallery) {
                    //with `gallery` object you can access all methods and properties described here http://photoswipe.com/documentation/api.html
                    //console.log(gallery);
                    //console.log(gallery.currItem);
                    //console.log(gallery.getCurrentIndex());
                    //gallery.zoomTo(1, {x:gallery.viewportSize.x/2,y:gallery.viewportSize.y/2}, 500);
                    gallery.toggleDesktopZoom();
                }
            });
            //This option forces plugin to create a single gallery and ignores `data-fancybox-group` attribute.
            $(".forcedgallery > a").jqPhotoSwipe({
                forceSingleGallery: true
            });
        
                    $("#photo-elements").addClass("mode-register");
                    $("#photos-gallery").addClass("mode-register-gallery");
                    $("#selected-photo").addClass("mode-register-selectimg");
                if ('AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_MANUAL' == 'AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_VIB' || 'AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_MANUAL' == 'AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_VPBANK') {
                    var BankNumberInfo = $("#BankNumberInfo").text();
                if (BankNumberInfo != 'Số tài khoản ngân hàng sẽ khác nhau với mỗi tài sản và với mỗi khách hàng. Quý khách hàng cần đăng ký tham gia đấu giá để xem được thông tin này.') {
                var acqId = 0;
                // Xử lý vs VPBank và VIB
                    if ('AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_MANUAL' == 'AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_VIB') {
                        acqId = 970441;
                        var link = "https://img.vietqr.io/image/" + acqId + "-" + BankNumberInfo + "-compact.jpg"
                    }
                    else if ('AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_MANUAL' == 'AUCTIONPROPERTY_META_BANK_CONFIG_TYPE_VPBANK') {
                        acqId = 970432;
                        var link = "https://img.vietqr.io/image/" + acqId + "-" + BankNumberInfo + "-compact.jpg"
                    }
                    $("#QrImage").attr("src", link);
              }
                }
            });
                function bidNowHighestPrice() {
                    BidMoreThanOneStep();
                }
                function checkBiddingStatus(id) {
                    $.ajax({
                        url: "https://lacvietauction.vn/Bidding/api/ListBiddingByUser/" + 0 + "/" + id,
                        type: "GET",
                        contentType: "application/json",
                        success: function (responseData) {
        
                            var data = responseData.data;
                            dataSourceBidding = data;
                            if (dataSourceBidding[0].value == parseInt($("#realTimeHighestPrice").text().replaceAll(".", ""))) {
                                $("#btnUserHighestPrice").css("display", "block");
                            } else {
                                $("#btnUserHighestPrice").css("display", "none");
                            }
                        },
                        error: function (e) {
                            console.log(e.message);
                            //initTableBidding();
                        }
                            });
        
        
                        }
                function getTimestampFromDBTime(n) {
                    let t = n.length;
                    22 == t && (n += "0"),
                        21 == t && (n += "00"),
                        19 == t && (n += ".000");
                    let e = n.substring(0, 4)
                        , i = n.substring(5, 7)
                        , r = n.substring(8, 10)
                        , u = n.substring(11, 13)
                        , o = n.substring(14, 16)
                        , c = n.substring(17, 19)
                        , a = n.substring(20, n.length)
                        , s = new Date;
                    return s.setYear(e),
                        s.setMonth(i - 1),
                        s.setDate(r),
                        s.setHours(u),
                        s.setMinutes(o),
                        s.setSeconds(c),
                        s.setMilliseconds(a),
                        s.getTime()
                }
                function formatDisplayTimeFromDBTime(n) {
                    let t = n.length;
                    22 == t && (n += "0"),
                        21 == t && (n += "00"),
                        19 == t && (n += ".000");
                    let e = n.substring(0, 4)
                        , i = n.substring(5, 7)
                        , r = n.substring(8, 10)
                        , u = n.substring(11, 13)
                        , o = n.substring(14, 16)
                        , c = n.substring(17, 19);
                    n.substring(20, t > 23 ? 23 : n.length);
                    return r + "/" + i + "/" + e + " " + u + ":" + o + ":" + c
                }
                function formatDisplayTimeWithMilisecondsFromDBTime(n) {
                    let t = n.length;
                    22 == t && (n += "0"),
                        21 == t && (n += "00"),
                        19 == t && (n += ".000");
                    let e = n.substring(0, 4)
                        , i = n.substring(5, 7);
                    return n.substring(11, 13) + ":" + n.substring(14, 16) + ":" + n.substring(17, 19) + "." + n.substring(20, t > 23 ? 23 : n.length)
                }
                function formatDisplayTimeWithMicrosecondsFromDBTime(n) {
                    let t = n.length;
                    22 == t && (n += "0"),
                        21 == t && (n += "00"),
                        19 == t && (n += ".000");
                    let e = n.substring(0, 4)
                        , i = n.substring(5, 7);
                    return n.substring(8, 10) + "/" + i + "/" + e + " " + n.substring(11, 13) + ":" + n.substring(14, 16) + ":" + n.substring(17, 19) + "." + n.substring(20, n.length)
                }
                var primaryTime;
                var isSendEndAlert = 0;
                function setTimeCountDown(toTime, method) {
                    // Set the date we're counting down to
                    var countDownDate = new Date(toTime).getTime();
        
                    // Update the count down every 1 second
                    primaryTime = setInterval(function () {
        
                        // Get today's date and time
                        var now = new Date($("#server-time-clock").data("time"));
        
                        // Find the distance between now and the count down date
                        var distance = countDownDate - now;
        
                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
                        if (days < 10) {
                            days = "0" + days;
                        }
                        if (hours < 10) {
                            hours = "0" + hours;
                        }
                        if (minutes < 10) {
                            minutes = "0" + minutes;
                        }
                        if (seconds < 10) {
                            seconds = "0" + seconds;
                        }
        
                         //Display the result in the element with id="demo"
                        if (days > 0) {
                            document.getElementById("days").innerHTML = days;
                            document.getElementById("hours").innerHTML = hours;
                            document.getElementById("minutes").innerHTML = minutes;
                            document.getElementById("seconds").innerHTML = seconds;
        
                        }
                        else {
                            $("#day-div-count").css("display", "none");
                            $("#day-knockout-div-count").css("display", "none");
        
                            document.getElementById("hours").innerHTML = hours;
                            document.getElementById("minutes").innerHTML = minutes;
                            document.getElementById("seconds").innerHTML = seconds;
                        }
        
        
                        // If the count down is finished, write some text
                        if (distance < 0) {
                            clearInterval(primaryTime);
                            if (method == "To Start") {
                                document.getElementById("timestamp").innerHTML = "Đến thời gian đấu giá...";
                                Swal.fire({
                                    title: 'Đến thời gian đấu giá',
                                    text: "Đã đến thời gian đấu giá tài sản, quý khách có thể theo dõi và đặt giá",
                                    icon: 'info',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Đồng ý',
                                }).then((result) => {
                                    window.location.reload();
                                })
                                setTimeout(function () {
                                    window.location.reload();
                                }, 3000);
                            }
        
                            else if (method == "To End") {
        
                                if ("1000001" == "1000001") {
                                    document.getElementById("timestamp").innerHTML = "Kết thúc đấu giá...";
                                    Swal.fire({
                                        title: 'Đã kết thúc thời gian đấu giá tài sản',
                                        text: "Quý khách có thể truy cập trang kết quả đấu giá tài sản",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Đi đến kết quả đấu giá',
                                        cancelButtonText: 'Bỏ qua',
                                        showCancelButton: true,
                                    }).then((result) => {
                                        if (result.value) {
                                            window.location.href = '/ket-qua-dau-gia/' + currentAuctionPropertyId;
                                        }
                                    });
                                    $(".bid-form").css("display", "none");
                                    $("#countdown-txt").text("Cuộc đấu giá đã kết thúc");
                                    $("#timestamp").css("display", "block");
                                    document.getElementById("timestamp").innerHTML = "<p class='check-result-txt' style='margin-bottom: 0 !important'>Xem kết quả cuộc đấu giá <a onclick='return CheckStatusEnded(" + 0 +")' class='result-link'>tại đây</a></p>";
                                    //PHIÊN ĐÃ KẾT THÚC
                                    $(".bidding-rq-stt").css("display", "none");
                                    $(".register-form").css("display", "block");
                                    $("#auction-info-tab").css("display", "none");
        
                                }
                                else {
                                    document.getElementById("timestamp").innerHTML = "Đến thời gian đấu giá loại trực tiếp...";
                                    Swal.fire({
                                        title: 'Đến thời gian đấu giá loại trực tiếp',
                                        text: "Đã đến thời gian đấu giá loại trực tiếp, quý khách có thể theo dõi và đặt giá",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Đồng ý',
                                    }).then((result) => {
                                        window.location.reload();
                                    })
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 3000);
                                }
        
                            }
                            else if (method == "Knockout To End Auction") {
                                document.getElementById("timestamp").innerHTML = "Kết thúc đấu giá...";
                                $(".div-knockout-time").css("display", "none");
                                if (isSendEndAlert == 0) {
                                    Swal.fire({
                                        title: 'Đã kết thúc thời gian đấu giá tài sản',
                                        text: "Quý khách có thể truy cập trang kết quả đấu giá tài sản",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Đi đến kết quả đấu giá',
                                        cancelButtonText: 'Bỏ qua',
                                        showCancelButton: true,
                                    }).then((result) => {
                                        if (result.value) {
                                            window.location.href = '/ket-qua-dau-gia/' + currentAuctionPropertyId;
                                        }
                                    });
                                }
                                     isSendEndAlert = 1;
        
                                    $(".bid-form").css("display", "none");
                                    $("#countdown-txt").text("Cuộc đấu giá đã kết thúc");
                                    $("#timestamp").css("display", "block");
                                    document.getElementById("timestamp").innerHTML = "<p class='check-result-txt' style='margin-bottom: 0 !important'>Xem kết quả cuộc đấu giá <a onclick='return CheckStatusEnded(" + 0 +")' class='result-link'>tại đây</a></p>";
                                    //PHIÊN ĐÃ KẾT THÚC
                                    $(".bidding-rq-stt").css("display", "none");
                                    $(".register-form").css("display", "block");
                                    $("#auction-info-tab").css("display", "none");
                            }
                        }
                    }, 1000);
                }
                function SetTotalKnoutTime(toTime, method) {
                    var TimeEnded = new Date("2023/09/08 10:00:00");
                    TimeEnded.setSeconds(toTime);
                    toTime = TimeEnded;
                    var countDownDate = new Date(toTime).getTime();
        
                    // Update the count down every 1 second
                    var x = setInterval(function () {
        
                        // Get today's date and time
                        var now = new Date($("#server-time-clock").data("time"));
        
                        // Find the distance between now and the count down date
                        var distance = countDownDate - now;
        
                        // Time calculations for days, hours, minutes and seconds
                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
                        if (days < 10) {
                            days = "0" + days;
                        }
                        if (hours < 10) {
                            hours = "0" + hours;
                        }
                        if (minutes < 10) {
                            minutes = "0" + minutes;
                        }
                        if (seconds < 10) {
                            seconds = "0" + seconds;
                        }
        
                         //Display the result in the element with id="demo"
                        if (days > 0) {
                            document.getElementById("days-knockout").innerHTML = days;
                            document.getElementById("hours-knockout").innerHTML = hours;
                            document.getElementById("minutes-knockout").innerHTML = minutes;
                            document.getElementById("seconds-knockout").innerHTML = seconds;
        
                        }
                        else {
                            document.getElementById("hours-knockout").innerHTML = hours;
                            document.getElementById("minutes-knockout").innerHTML = minutes;
                            document.getElementById("seconds-knockout").innerHTML = seconds;
                        }
        
        
                        // If the count down is finished, write some text
                        if (distance < 0) {
                            $(".div-knockout-time").css("display", "none");
                            $("#knockout-guide").css("display", "none");
        
                            clearInterval(x);
        
                            if (method == "Knockout To End") {
                                if (isSendEndAlert == 0) {
                                    Swal.fire({
                                        title: 'Đã kết thúc thời gian đấu giá tài sản',
                                        text: "Quý khách có thể truy cập trang kết quả đấu giá tài sản",
                                        icon: 'info',
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: 'Đi đến kết quả đấu giá',
                                        cancelButtonText: 'Bỏ qua',
                                        showCancelButton: true,
                                    }).then((result) => {
                                        if (result.value) {
                                            window.location.href = '/ket-qua-dau-gia/' + currentAuctionPropertyId;
                                        }
                                    });
                                }
                                isSendEndAlert = 1;
                    $(".bid-form").css("display" , "none");
                    $("#countdown-txt").text("Cuộc đấu giá đã kết thúc");
                    $("#timestamp").css("display", "block");
                    document.getElementById("timestamp").innerHTML = "<p class='check-result-txt' style='margin-bottom: 0 !important'>Xem kết quả cuộc đấu giá <a onclick='return CheckStatusEnded("+ 0 +")' class='result-link'>tại đây</a></p>";
                   //PHIÊN ĐÃ KẾT THÚC
                    $(".bidding-rq-stt").css("display" , "none");
                    $(".register-form").css("display", "block");
                    $("#auction-info-tab").css("display", "none");
                            }
                        }
                    }, 1000);
                }
        
                function CheckLoginBiddingRequest(userId) {
                    if (userId != 0) {
                        window.location.href = '/dang-ky-dau-gia/' + 1002395;
                    }
                    else {
                        openModalLogin();
                    }
                }
        
              function CheckStatusEnded(userId) {
                    if (userId != 0) {
                        window.location.href = '/ket-qua-dau-gia/' + currentAuctionPropertyId;
                    }
                    else {
                        openModalLogin();
                    }
                }
                function convertMoneyNumberToText(n) {
                    if (0 == (n = n.replace(/[^0-9]/g, "")))
                        return mangso[0];
                    var t = ""
                        , e = "";
                    do {
                        ty = n % 1e9,
                            t = (n = Math.floor(n / 1e9)) > 0 ? dochangtrieu(ty, !0) + e + t : dochangtrieu(ty, !1) + e + t;
                        e = " tỷ"
                    } while (n > 0);
                    return (t = (t = t.trim()).charAt(0).toUpperCase() + t.slice(1)) + " đồng"
                }
                var mangso = ["không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín"];
                function dochangtrieu(n, t) {
                    var e = "";
                    return trieu = Math.floor(n / 1e6),
                        n %= 1e6,
                        trieu > 0 && (e += dochangnghin(trieu, t) + " triệu",
                            t = !0),
                        n > 0 && (e += dochangnghin(n, t)),
                        e
                }
                function dochangchuc(n, t) {
                    var e = "";
                    return chuc = Math.floor(n / 10),
                        donvi = n % 10,
                        chuc > 1 ? (e = " " + mangso[chuc] + " mươi",
                            1 == donvi && (e += " mốt")) : 1 == chuc ? (e = " mười",
                                1 == donvi && (e += " một")) : t && donvi > 0 && (e = " lẻ"),
                        5 == donvi && chuc > 0 ? e += " lăm" : (donvi > 1 || 1 == donvi && 0 == chuc) && (e += " " + mangso[donvi]),
                        e
                }
                function dochangtram(n, t) {
                    var e = "";
                    return tram = Math.floor(n / 100),
                        n %= 100,
                        t || tram > 0 ? (e = " " + mangso[tram] + " trăm",
                            e += dochangchuc(n, !0)) : e = dochangchuc(n, !1),
                        e
                }
                function dochangnghin(n, t) {
                    var e = "";
                    return nghin = Math.floor(n / 1e3),
                        n %= 1e3,
                        nghin > 0 && (e += dochangtram(nghin, t) + " nghìn",
                            t = !0),
                        n > 0 && (e += dochangtram(n, t)),
                        e
                }
                function bidHighestPrice(newPrice) {
                    $("#price-bid").attr("min", newPrice);
                    $("#price-bid").attr("value", newPrice);
                    $("#price-bid").val(newPrice.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
        /*            $("#bidValueInWords").text(convertMoneyNumberToText($("#bidValue").val()));*/
                    formatNumber();
                }
                function checkCurrentHighestBiddingPrice(accountId) {
                if (biddingStatus != 1) return;
                $.ajax({
                    url: "/Bidding/api/FindMaxBidding/" + currentAuctionPropertyId,
                    type: "GET",
                    contentType: "application/json",
                    success: function (responseData) {
                        var biddingDataSource = responseData.data[0].value;
                        //find highest value
                        if (biddingDataSource > 0) {
        
                            //Reset đồng hồ trả giá
                            if ("NOT" == "KNOCKOUT") {
                                // Hiển thị tổng thời gian
        
                                $("#titleCountDown-total").css("display", "block");
                                $("#knockout-guide").css("display", "block");
                                $(".div-knockout-time").css("display", "block");
                                $("#FormCountDown-TotalKnockoutTime").css("display", "block");
        
        
                                var currentOldKnockout = new Date(responseData.data[0].description);
                                var currentCloseTime = new Date(closeTime);
                                if (currentOldKnockout > currentCloseTime) {
                                    var currentNewKnockout = currentOldKnockout.setSeconds(currentOldKnockout.getSeconds());
                                    var closedAllTime = currentCloseTime.setSeconds(currentCloseTime.getSeconds() + parseInt(""));
                                    if (closedAllTime < currentNewKnockout) {
                                        Swal.fire({
                                            title: "Kết thúc đấu giá!",
                                            icon: "info",
                                            text: "Cuộc đấu giá đã kết thúc, sang trang kết quả để xem kết quả cuộc đấu giá!",
                                            showCancelButton: false,
                                            confirmButtonText: "OK"
                                        }).then((result) => {
                                            window.location.href = "/tai-san-dau-gia/" + currentAuctionPropertyId + "-" + currentAuctionPropertyUrl;
                                        });
                                            setTimeout(function () { window.location.href = "/tai-san-dau-gia/" + currentAuctionPropertyId + "-" + currentAuctionPropertyUrl; }
                                            , 3000)
                                    }
                                    else {
                                        var currentNewKnockout = currentOldKnockout.setSeconds(currentOldKnockout.getSeconds() + parseInt(""));
                                        clearInterval(primaryTime);
                                        setTimeCountDown(currentNewKnockout, "Knockout To End Auction");
                                        //countDown(currentNewKnockout, closedAllTime);
                                    }
                                }
        
                            }
                            // Nếu thằng này là người trả giá cao nhất thì show thông báo bạn là ng cao nhất
                            if (0 > 0 && accountId > 0) {
                                if (0 == accountId) {
                                    $("#btnUserHighestPrice").css("display", "block");
                                }
                                else {
                                    $("#btnUserHighestPrice").css("display", "none");
                                }
                            };
                            //Nếu giá được cập nhật  thì thông báo bằng cách nhấp nháy:
                            if (currentHighestBiddingPrice != biddingDataSource) {
                                $("#realTimeHighestPrice").addClass("priceBlink");
                                setTimeout(function () { $("#realTimeHighestPrice").removeClass("priceBlink"); }, 1500);
                            }
                            currentHighestBiddingPrice = biddingDataSource;
                            console.log("HIGHEST VALUE:" + currentHighestBiddingPrice);
                            $("#realTimeHighestPrice").text(currentHighestBiddingPrice);
                            if (currentHighestBiddingPrice != 0) {
                                let currentStepPrice = parseFloat(10000000);
                                let newMinPrice = currentHighestBiddingPrice + currentStepPrice;
                                //Harry 061021 comment hủy auto sửa giá mới nhất
                                //Cập nhật giá đặt nếu giá đang định trả nhỏ hơn giá tối thiếu mới
                                //var currentBidValue = parseFloat($("#price-bid").val().replace(/\./g, ''));
                                //if (currentBidValue < newMinPrice) {
                                //    $("#bidValue").attr("value", newMinPrice);
                                //    $("#bidValue").val(newMinPrice);
                                //}
                                //openingPrice = parseInt(openingPrice.replaceAll(".", ""));
                                if (currentHighestBiddingPrice > openingPrice) {
                                    if (firstTimeVisitUpdate == 1) {
                                        bidHighestPrice(newMinPrice);
                                        firstTimeVisitUpdate = 0;
                                    }
                                }
                                else if (currentHighestBiddingPrice == openingPrice) {
                                    bidHighestPrice(openingPrice + stepPrice);
                                    firstTimeVisitUpdate = 0;
                                }
                                else if (myHighestPrice == 0) {
                                    bidHighestPrice(openingPrice);
                                }
                            }
                            else {
                                let openPrice = parseFloat(345116000);
                                $("#bidValue").attr("value", openPrice);
                                $("#bidValue").attr("min", openPrice);
                                $("#bidValue").val(openPrice);
                                $("#bidValueInWords").text(convertMoneyNumberToText($("#price-bid").val()));
                                $("#realTimeHighestPrice").text(openPrice);
                            }
        
                            formatNumber();
        
                            //Cập nhật bảng rút lại giá đã trả - chỉ áp dụng cho người đang trả giá cao nhất: ẩn nút rút lại giá đã trả
                            if ($("#highYourPrice").text() != $("#realTimeHighestPrice").text()) {
        
                                $("#tableData tbody tr td:last-child").html("");
                                //Harry 061021
                                $("#tableData tbody tr td:nth-last-child(4)").html('<div style="color: #b41712;">Giá trả thấp hơn</div>');
                            }
                            //bỏ logic này chuyển về chỉ show của người đang đăng nhập
                            //else {
                            //    //show nút hủy giá
                            //    let biddingRequestId = $("#biddingRequestId").text();
                            //    $("#tableData tbody tr").each(function () {
                            //        let thisRowId = $(this).attr("id");
                            //        let thisBiddingRequestId = $("#" + thisRowId + " td:nth-child(3)").text();
                            //        let thisBiddingValue = $("#" + thisRowId + " td:nth-child(6)").text();
                            //        let thisBiddingId = parseInt( $("#" + thisRowId + " td:nth-child(2)").text());
                            //        if (thisBiddingRequestId == biddingRequestId && thisBiddingValue == $("#realTimeHighestPrice").text()) {
                            //            //alert();
                            //            $("#" + thisRowId + " td:nth-child(7)").html(`<a onclick="InactiveBiddingItem(`+thisBiddingId+`)"><i class="fa fa-trash fa-2x" style="color:#e91e63"></i></a>`);
                            //        }
                            //    });
        
                            //}
        
                        }
                        else {
                            let openPrice = parseFloat(345116000);
                            $("#bidValue").attr("value", openPrice);
                            $("#bidValue").attr("min", openPrice);
                            $("#bidValue").val(openPrice);
                            $("#bidValueInWords").text(convertMoneyNumberToText($("#price-bid").val()));
                            $("#realTimeHighestPrice").text(openPrice);
                            formatNumber();
                        }
                    },
                    error: function (e) {
                        //console.log(e.message);
                        //initTable();
        
                        //Harry 061021
                        //temp case server 500
                        if (e.status === 500) {
                            $("#realTimeHighestPrice").text(0);
                        }
                        else {
                            let openPrice = parseFloat(345116000);
                            $("#bidValue").attr("value", openPrice);
                            $("#bidValue").attr("min", openPrice);
                            $("#bidValue").val(openPrice);
                            $("#bidValueInWords").text(convertMoneyNumberToText($("#price-bid").val()));
                            //$("#realTimeHighestPrice").text(openPrice);
                        }
                        formatNumber();
                    }
                });
        
                //Harry 061021 - lúc này đã check xong rồi
                //firstTimeVisitUpdate = 0;
            }
                function generateRandomString(n) {
            for (var t = "", e = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", i = e.length, r = 0; r < n; r++)
                t += e.charAt(Math.floor(Math.random() * i));
            return t
        }
                function InactiveBiddingItem(id) {
                var cancelBiddingConfirmString = generateRandomString(6);
                Swal.fire({
                    title: 'Xác nhận rút lại giá đã trả.',
                    html: "Nếu rút lại giá đã trả bạn sẽ mất tiền đặt trước và bị truất quyền đấu giá! Nhập mã xác nhận để rút lại giá đã trả: <span style='color:#b41712;font-weight:bold' class='preventCopy'>"+cancelBiddingConfirmString+"</span>",
                    icon: 'warning',
                    input: 'text',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: 'Xác nhận',
                    cancelButtonText: 'Huỷ'
                }).then((result) => {
                    if (result.dismiss != "cancel") {
                        if (result.value === cancelBiddingConfirmString) {
                            //CALL AJAX TO DELETE
                            $.ajax({
                                url: systemURL + "bidding/api/InactiveBiddingByUser/" + id,
                                type: "GET",
                                contentType: "application/json",
                                success: function (responseData) {
                                    updateDisplayBiddingCancel("row" + id);
                                    if (responseData.status == 200 && responseData.message === "SUCCESS") {
                                        Swal.fire(
                                            'Thành công!',
                                            'Đã rút lại giá đã trả',
                                            'success'
                                        );
                                    }
                                },
                                error: function (e) {
                                    //console.log(e.message);
                                    Swal.fire(
                                        'Lỗi!',
                                        'Đã xảy ra lỗi, vui lòng thử lại',
                                        'error'
                                    );
                                }
                            });
                        }
                        else {
                            Swal.fire(
                                'Mã xác nhận không chính xác !',
                                'Xin vui lòng kiểm tra lại mã xác nhận.',
                                'info'
                            );
                        }
                    }
                })
            }

            firstTimeVisitUpdate = 1;
            var biddingStatus = 0;
            var myHighestPrice = 0;
            var stepPrice = parseInt($("#step-price").text().replaceAll(".", ""));
            var openingPrice = parseFloat(345116000);
            var currentAuctionPropertyId = 1002395;
            const openingPriceFirst = new Intl.NumberFormat('vi-VN', {
              style: 'currency',
              currency: 'VND',
            });
            //openingPrice = openingPriceFirst.format(openingPrice).replaceAll("₫", "");
            var openTime = "2023/09/08 09:30:00"; // ĐÂY LÀ GIỜ GMT+7
            var closeTime = "2023/09/08 10:00:00";
            document.querySelector("#price-bid").value = openingPrice;
            //PHIÊN CHƯA DIỄN RA
            if ("NOT" == "NOT") {
                setTimeCountDown(openTime, "To Start");
                $("#countdown-txt").text("Thời gian đếm ngược bắt đầu trả giá:")
                $(".register-form").css("display", "block");
                $("#auction-info-tab").css("display", "none");
            }
            else if ("NOT" == "NOW") {
            //PHIÊN ĐANG DIỄN RA
            setTimeCountDown(closeTime, "To End");
                if ("0" == "1") {
                  $(".big-image").css("height", "400px");
                  $(".bid-form").css("display", "block");
                  $("#auction-status-bidding").attr('style', 'display: flex !important');
                  $("#auction-status-waiting").attr('style', 'display: none !important');
                  $("#pills-description").removeClass("active show wow fadeInUp");
                  $("#pills-bid").addClass("active show wow fadeInUp");
                  biddingStatus = 1;
                  if ("0" == "0") {
                       checkCurrentHighestBiddingPrice();
                       formatNumber();
                  }
                  else {
                       checkCurrentHighestBiddingPrice();
                       $("#highYourPrice").text(0);
                       formatNumber();
                  }
    
    
            }
                else if ("0" == "1001210") {
                  $("#topBiddingPrice").css("display", "none");
                  $("#div-bid-form").css("display", "none");
                  $(".form-title-bidding").css("display", "none");
    
                  $(".big-image").css("height", "400px");
                  $(".bid-form").css("display", "block");
                  $("#auction-status-bidding").attr('style', 'display: flex !important');
                  $("#auction-status-waiting").attr('style', 'display: none !important');
                  $("#pills-description").removeClass("active show wow fadeInUp");
                  $("#pills-bid").addClass("active show wow fadeInUp");
                  biddingStatus = 1;
    
    
                  if ("0" == "0") {
                       checkCurrentHighestBiddingPrice();
                       formatNumber();
                  }
                  else {
                       checkCurrentHighestBiddingPrice();
                       $("#highYourPrice").text(0);
                       formatNumber();
                  }
                  }
            else {
                  $(".register-form").css("display", "block");
            }
            }
            else if ("NOT" == "KNOCKOUT") {
    
                //setTimeCountDown(closeTime, "To End");
                $(".div-knockout-time").css("display", "block");
                var currentTimeKnockout = new Date("").getTime();
                SetTotalKnoutTime(parseInt(""), "Knockout To End")
                setTimeCountDown(currentTimeKnockout, "Knockout To End Auction");
                $("#countdown-txt").text("Thời gian trả giá còn lại của lần trả giá:");
                if ("0" == "1") {
                    $("#auction-status-bidding").attr('style', 'display: flex !important');
                    $("#auction-status-waiting").attr('style', 'display: none !important');
                    $("#pills-description").removeClass("active show wow fadeInUp");
                    $("#pills-bid").addClass("active show wow fadeInUp");
                    $(".bid-form").css("display", "block");
    
    
                    biddingStatus = 1;
                    if ("0" == "0") {
                        checkCurrentHighestBiddingPrice();
                        formatNumber();
                    }
                    else {
                        checkCurrentHighestBiddingPrice();
                        $("#highYourPrice").text(0);
                        formatNumber();
                    }
                    if ("" == "YES" && "0" == "0") {
                        $(".bid-form").css("display", "none");
                        $(".register-form").css("display", "block");
                    }
                    else {
                        $(".big-image").css("height", "400px");
                    }
                }
                else {
                    $(".register-form").css("display", "block");
                }
            }
            else if ("NOT" == "CANCEL"){
                //PHIÊN ĐÃ BỊ HỦY
                $("#countdown-txt").css("display", "none")
                $("#timestamp").css("display", "block");
                $(".bidding-rq-stt").css("display" , "none");
                document.getElementById("timestamp").innerHTML = "<p class='check-result-txt' style='margin-bottom: 0 !important'>Cuộc đấu giá đã bị hủy</p>";
                $(".register-form").css("display", "block");
                $("#auction-info-tab").css("display", "none");
            }
            else {
                //PHIÊN ĐÃ KẾT THÚC
                $("#countdown-txt").text("Cuộc đấu giá đã kết thúc");
                $("#timestamp").css("display", "block");
                $(".bidding-rq-stt").css("display" , "none");
                document.getElementById("timestamp").innerHTML = "<p class='check-result-txt' style='margin-bottom: 0 !important'>Xem kết quả cuộc đấu giá <a onclick='return CheckStatusEnded("+ 0 +")' class='result-link'>tại đây</a></p>";
                $(".register-form").css("display", "block");
                $("#auction-info-tab").css("display", "none");
            }

            function PlusPrice() {
                var priceNow = parseInt($("#price-bid").val().replaceAll(".", ""));
                var highestPriceNow = parseInt($("#realTimeHighestPrice").text().replaceAll(".", ""));
                var newPriceSet = priceNow + stepPrice;
                if (highestPriceNow != 0){
            //if ("UNLIMITALL" == "LIMIT" && newPriceSet > highestPriceNow + stepPrice * parseInt()) {
            //    Swal.fire({
            //        icon: 'info',
            //        text: 'Đặt giá quá cao',
            //    })
            //}
            //else {
                priceNow = newPriceSet;
                $("#price-bid").val(newPriceSet.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
            //}
        }
        else {
            if ("UNLIMITALL" == "LIMIT" && newPriceSet > openingPrice + stepPrice * parseInt()) {
                Swal.fire({
                    icon: 'info',
                    text: 'Đặt giá quá cao',
                })
            }
                    else {
                        priceNow = newPriceSet;
                        $("#price-bid").val(newPriceSet.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
                    }
    
                }
            }
            function MinusPrice() {
                var priceNow = parseInt($("#price-bid").val().replaceAll(".", ""));
                var highestPriceNow = parseInt($("#realTimeHighestPrice").text().replaceAll(".", ""));
                var newPriceSet = priceNow - stepPrice;
                if (highestPriceNow == 0) {
                    if (newPriceSet >= openingPrice) {
                        priceNow = newPriceSet;
                        $("#price-bid").val(newPriceSet.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
                    }
                    else {
                        Swal.fire({
                            icon: 'info',
                            text: 'Bạn phải đặt giá lớn hơn hoặc bằng giá khởi điểm',
                        })
                    }
                }
                else if (newPriceSet > highestPriceNow) {
                    priceNow = newPriceSet;
                    $("#price-bid").val(newPriceSet.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        text: 'Bạn phải đặt giá lớn hơn giá cao nhất hiện tại ít nhất 1 (một) bước giá',
                    })
                }
            }
            function BidMoreThanOneStep() {
                var highestPriceNow = parseInt($("#realTimeHighestPrice").text().replaceAll(".", ""));
                if (highestPriceNow != 0) {
                    var newPriceSet = highestPriceNow + stepPrice;
                    priceNow = newPriceSet;
                    $("#price-bid").val(newPriceSet.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
                }
                else {
                    var newPriceSet = openingPrice;
                    priceNow = newPriceSet;
                    $("#price-bid").val(newPriceSet.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
                }
    
        //function BidNow() {
        //if (confirm("Đặt lệnh đấu giá với số tiền là: " + priceNow.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.') + ", bằng chữ: ...")) {
        //highestPriceNow = priceNow;
        //$("#highest-price-now").text(priceNow.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'))
        //$("#highest-price-now").addClass("priceBlink");
        //setTimeout(function () { $("#highest-price-now").removeClass("priceBlink"); }, 1500);
        //priceNow = priceNow + stepPrice;
        //$("#price-bid").val(priceNow.toString().replaceAll(/\B(?=(\d{3})+(?!\d))/g, '.'));
    
        //}
        }

        var sendingBiddingFlag = 0;
        var currentHighestBiddingPrice = 0;
        var currentAuctionPropertyId = 1002395;
        checkCurrentHighestBiddingPrice();
        function nBiddingNow() {
            //var valuePrice = $("[data-auction-id='4450']").val();
            //valuePrice = parseFloat(valuePrice.replace(/\./g, ''));
            var valuePrice = parseFloat($("#price-bid").val().replace(/\./g, ''));
            let CurrentMaxValuePrice = $("#realTimeHighestPrice").text();
            CurrentMaxValuePrice = parseFloat(CurrentMaxValuePrice.replace(/\./g, ""));
            CurrentMaxValuePrice = currentHighestBiddingPrice;
            let currentStepPrice = parseFloat(10000000);
            if (valuePrice < 345116000) {
                Swal.fire({
                    title: 'Đặt giá quá thấp',
                    icon: 'info',
                    text: 'Đặt giá phải cao hơn giá khởi điểm!',
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                }).then((result) => {
                    //$("#bidValue").val(345116000);
                    checkCurrentHighestBiddingPrice();
                    formatNumber();
                    var objBidding = '{"AuctionPropertyId":1002395,"Value":"' + valuePrice + '"}';
                    Log(window, "CLIENT_BIDDING_KEY_TOO_LOW", objBidding);

                    return;
                });
            }
            else if ("UNLIMITALL" == "LIMIT" && CurrentMaxValuePrice + currentStepPrice * parseInt() < valuePrice && CurrentMaxValuePrice > 0) {
                Swal.fire({
                    title: 'Đặt giá quá cao',
                    icon: 'info',
                    text: 'Đặt giá phải không quá  bước giá so với giá cao nhất hiện tại!',
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                }).then((result) => {
                    checkCurrentHighestBiddingPrice();
                    bidNowHighestPrice();
                    formatNumber();
                    var objBidding = '{"AuctionPropertyId":1002395,"Value":"' + valuePrice + '"}';
                    Log(window, "CLIENT_BIDDING_KEY_TOO_HIGH", objBidding);

                    return;
                });
            }
            // harry 061021
            else if (CurrentMaxValuePrice >= valuePrice && CurrentMaxValuePrice > 0) {
                Swal.fire({
                    title: 'Đặt giá thất bại',
                    icon: 'warning',
                    text: 'Đã có người trả mức giá này. Giá trả của bạn phải cao hơn giá cao nhất hiện tại ít nhất một bước giá!',
                    showCancelButton: false,
                    confirmButtonText: 'OK'
                }).then((result) => {
                    //harry 061021
                    bidNowHighestPrice();
                    var objBidding = '{"AuctionPropertyId":1002395,"Value":"' + valuePrice + '"}';
                    Log(window, "CLIENT_BIDDING_KEY_FAIL_VALUE_EXISTED", objBidding);


                    //window.location.reload();
                })
            }
            else {
                //debugger;
                let currentValuePrice = parseFloat(valuePrice);
                let openPrice = parseFloat(345116000);
                let soThuong = Math.round(currentValuePrice / currentStepPrice);
                let soDu = (currentValuePrice - openPrice) % currentStepPrice;
                if (soDu == 0) {
                    Swal.fire({
                        title: 'Trả giá tài sản với: ' + $("#price-bid").val() + ' VNĐ',
                        icon: 'question',
                        text: 'Giá của bạn: ' + convertMoneyNumberToText($("#price-bid").val()) + '.',
                        showCancelButton: true,
                        confirmButtonText: 'Xác nhận',
                        allowEnterKey: 'false',
                        cancelButtonText: 'Hủy'
                    }).then((result) => {
                        if (result.value && sendingBiddingFlag == 0) {
                            sendingBiddingFlag = 1;
                            //Henry: url
                            //debugger;
                            var urlTemp = location.href;
                            var currentUrlApi = "";
                            var source = "";
                            if (source == "APP") {
                                currentUrlApi = "/Bidding/api/BiddingNow?auctionPropertyId=" + currentAuctionPropertyId + "&Value=" + valuePrice + "&source=app";
                            }
                            else {
                                currentUrlApi = "/Bidding/api/BiddingNow?auctionPropertyId=" + currentAuctionPropertyId + "&Value=" + valuePrice;
                            }
                            var newCurrentMaxValuePrice = $("#realTimeHighestPrice").text();
                            //debugger;
                            //if (parseFloat(newCurrentMaxValuePrice.replaceAll(".", "")) < valuePrice) {
                            $.ajax({
                                type: "GET",
                                contentType: "application/json",
                                url: currentUrlApi,
                                success: function (responseData) {
                                    sendingBiddingFlag = 0;
                                    //debugger;
                                    if (responseData.status == 201 && responseData.message == "CREATED") {
                                        var newBiddingObj = responseData.data[0];
                                        updateTableNewBidding(newBiddingObj);
                                        $("#highYourPrice").text(valuePrice);
                                        //harry 061021
                                        bidHighestPrice(valuePrice + stepPrice);
                                        myHighestPrice = valuePrice;
                                        formatNumber();
                                    }
                                    if (responseData.status == 1 && responseData.message == "VALUE_EXISTED") {
                                        Swal.fire({
                                            title: 'Đặt giá thất bại',
                                            icon: 'warning',
                                            text: 'Đã có người trả mức giá này. Giá trả của bạn phải cao hơn giá cao nhất hiện tại ít nhất một bước giá!',
                                            showCancelButton: false,
                                            confirmButtonText: 'OK'
                                        }).then((result) => {
                                            //harry 061021
                                            bidNowHighestPrice();
                                            Log(window, "CLIENT_BIDDING_KEY_FAIL_VALUE_EXISTED", responseData.data[0]);


                                            //window.location.reload();
                                        })
                                    }
                                    if (responseData.status == 1 && responseData.message == "STATUS_CANCEL") {
                                        Swal.fire({
                                            title: 'Đặt giá thất bại',
                                            icon: 'warning',
                                            text: 'Tài sản đấu giá đã bị huỷ! Vui lòng liên hệ đấu giá viên để biết thêm thông tin chi tiết.',
                                            showCancelButton: false,
                                            confirmButtonText: 'OK'
                                        }).then((result) => {
                                            Log(window, "CLIENT_BIDDING_KEY_STATUS_CANCEL", responseData.data[0]);
                                            window.location.reload();
                                        })
                                    }
                                    if (responseData.status == 1 && responseData.message == "BIDDINGFAILD") {
                                        var time = responseData.data[0].createdTime;
                                        var date = new Date(time);
                                        var newValue = date.toLocaleDateString() + " " + date.toLocaleTimeString() + "." + date.getMilliseconds();
                                        Swal.fire({
                                            title: 'Đặt giá thất bại',
                                            icon: 'warning',
                                            html: 'Tài sản đấu giá đã đã hết thời gian trả giá. Bạn đã đặt giá vào lúc <b>' + newValue + '</b>. Giá trả của bạn sẽ không được ghi nhận.',
                                            showCancelButton: false,
                                            confirmButtonText: 'OK'
                                        }).then((result) => {
                                            window.location.reload();
                                        })
                                    }
                                    if (responseData.status == 1 && responseData.message == "VALUE_MORE_THAN_MAX_BIDDING") {
                                        Swal.fire({
                                            title: 'Đặt giá quá cao',
                                            icon: 'info',
                                            text: 'Đặt giá phải không quá  bước giá so với giá cao nhất hiện tại!',
                                            showCancelButton: false,
                                            confirmButtonText: 'OK'
                                        }).then((result) => {
                                            //harry 061021
                                            bidNowHighestPrice();
                                            Log(window, "CLIENT_BIDDING_KEY_TOO_HIGH", responseData.data[0]);
                                            //window.location.reload();
                                        })
                                    }
                                },
                                error: function (e) {
                                    console.log("err: " + e.message);
                                    Swal.fire({
                                        title: 'Cảnh báo lỗi!',
                                        icon: 'warning',
                                        text: 'Đã có lỗi xảy ra, vui lòng thử lại!',
                                        showCancelButton: false,
                                        confirmButtonText: 'OK'
                                    }).then((result) => {
                                        Log(window, "CLIENT_BIDDING_KEY_FAIL");
                                        window.location.reload();
                                    })
                                }
                            })
                            //}
                            //else {
                            //    Swal.fire({
                            //        title: 'Đặt giá thất bại',
                            //        icon: 'warning',
                            //        text: 'Tài sản đã có người đặt giá cao hơn. Vui lòng thử lại',
                            //        showCancelButton: false,
                            //        confirmButtonText: 'OK'
                            //    })
                            //}
                        }

                        checkCurrentHighestBiddingPrice();
                        formatNumber();
                    });
                }
                else {

                    let newValuePrice = (currentStepPrice * (Math.round((currentValuePrice - openPrice) / currentStepPrice) + 1)) + openPrice;
                    //Harry comment - bỏ logic tự động update giá real time
                    //$("#bidValue").val(newValuePrice + currentStepPrice);
                    $("#bidValueInWords").text(convertMoneyNumberToText($("#price-bid").val()));
                    formatNumber();
                    //Henry: url
                    //debugger;
                    var urlTemp = location.href;
                    var currentUrlApi = "";
                    if (urlTemp.indexOf("app") > 0) {
                        currentUrlApi = "/Bidding/api/BiddingNow?auctionPropertyId=" + currentAuctionPropertyId + "&Value=" + newValuePrice + "&source=app";
                    }
                    else {
                        currentUrlApi = "/Bidding/api/BiddingNow?auctionPropertyId=" + currentAuctionPropertyId + "&Value=" + newValuePrice;
                    }
                    Swal.fire({
                        title: 'Trả giá tài sản với: ' + $("#price-bid").val() + ' VNĐ',
                        icon: 'question',
                        text: 'Giá tài sản đã được làm tròn với bước giá: ' + $("#step-price").text() + ' VNĐ',
                        showCancelButton: true,
                        allowEnterKey: 'false',
                        confirmButtonText: 'OK'
                    }).then((result) => {
                        if (result.value && sendingBiddingFlag == 0) {
                            sendingBiddingFlag = 1;
                            $.ajax({
                                type: "GET",
                                contentType: "application/json",
                                url: currentUrlApi,
                                success: function (responseData) {
                                    sendingBiddingFlag = 0;
                                    //debugger;
                                    var newBiddingObj = responseData.data[0];
                                    updateTableNewBidding(newBiddingObj);
                                    //console.log(responseData);
                                    //console.log(responseData.message);
                                    //console.log(responseData.status);
                                    if (responseData.status == 201 && responseData.message == "CREATED") {
                                        $("#highYourPrice").text(newValuePrice);
                                        formatNumber();
                                        //Swal.fire({
                                        //    title: 'Đặt giá thành công',
                                        //    icon: 'success',
                                        //    text: 'Chờ kết thúc cuộc đấu giá để xem kết quả người thắng cuộc!',
                                        //    showCancelButton: false,
                                        //    confirmButtonText: 'OK'
                                        //}).then((result) => {
                                        //    //window.location.reload();
                                        //})
                                    }
                                },
                                error: function (e) {
                                    console.log("err: " + e.message);
                                    Swal.fire({
                                        title: 'Cảnh báo lỗi!',
                                        icon: 'warning',
                                        text: 'Lỗi hệ thống, vui lòng thử lại!',
                                        showCancelButton: false,
                                        confirmButtonText: 'OK'
                                    }).then((result) => {
                                        Log(window, "CLIENT_BIDDING_KEY_FAIL");
                                        //window.location.reload();
                                    })
                                }
                            })
                        }
                    });
                }

            }

        }
        function Log(window, key, object) {
            {
                var unknown = '-';

                // screen
                var screenSize = '';
                if (screen.width) {
                    width = (screen.width) ? screen.width : '';
                    height = (screen.height) ? screen.height : '';
                    screenSize += '' + width + " x " + height;
                }

                // browser
                var nVer = navigator.appVersion;
                var nAgt = navigator.userAgent;
                var browser = navigator.appName;
                var version = '' + parseFloat(navigator.appVersion);
                var majorVersion = parseInt(navigator.appVersion, 10);
                var nameOffset, verOffset, ix;

                // Opera
                if ((verOffset = nAgt.indexOf('Opera')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 6);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Opera Next
                if ((verOffset = nAgt.indexOf('OPR')) != -1) {
                    browser = 'Opera';
                    version = nAgt.substring(verOffset + 4);
                }
                // Edge
                else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
                    browser = 'Microsoft Edge';
                    version = nAgt.substring(verOffset + 5);
                }
                // MSIE
                else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(verOffset + 5);
                }
                // Chrome
                else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
                    browser = 'Chrome';
                    version = nAgt.substring(verOffset + 7);
                }
                // Safari
                else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                    browser = 'Safari';
                    version = nAgt.substring(verOffset + 7);
                    if ((verOffset = nAgt.indexOf('Version')) != -1) {
                        version = nAgt.substring(verOffset + 8);
                    }
                }
                // Firefox
                else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                    browser = 'Firefox';
                    version = nAgt.substring(verOffset + 8);
                }
                // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                }
                // Other browsers
                else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                    browser = nAgt.substring(nameOffset, verOffset);
                    version = nAgt.substring(verOffset + 1);
                    if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                    }
                }
                // trim the version string
                if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
                if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

                majorVersion = parseInt('' + version, 10);
                if (isNaN(majorVersion)) {
                    version = '' + parseFloat(navigator.appVersion);
                    majorVersion = parseInt(navigator.appVersion, 10);
                }

                // mobile version
                var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer);

                // cookie
                var cookieEnabled = (navigator.cookieEnabled) ? true : false;

                if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {
                    document.cookie = 'testcookie';
                    cookieEnabled = (document.cookie.indexOf('testcookie') != -1) ? true : false;
                }

                // system
                var os = unknown;
                var clientStrings = [
                    { s: 'Windows 10', r: /(Windows 10.0|Windows NT 10.0)/ },
                    { s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/ },
                    { s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/ },
                    { s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/ },
                    { s: 'Windows Vista', r: /Windows NT 6.0/ },
                    { s: 'Windows Server 2003', r: /Windows NT 5.2/ },
                    { s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/ },
                    { s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/ },
                    { s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/ },
                    { s: 'Windows 98', r: /(Windows 98|Win98)/ },
                    { s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/ },
                    { s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/ },
                    { s: 'Windows CE', r: /Windows CE/ },
                    { s: 'Windows 3.11', r: /Win16/ },
                    { s: 'Android', r: /Android/ },
                    { s: 'Open BSD', r: /OpenBSD/ },
                    { s: 'Sun OS', r: /SunOS/ },
                    { s: 'Chrome OS', r: /CrOS/ },
                    { s: 'Linux', r: /(Linux|X11(?!.*CrOS))/ },
                    { s: 'iOS', r: /(iPhone|iPad|iPod)/ },
                    { s: 'Mac OS X', r: /Mac OS X/ },
                    { s: 'Mac OS', r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/ },
                    { s: 'QNX', r: /QNX/ },
                    { s: 'UNIX', r: /UNIX/ },
                    { s: 'BeOS', r: /BeOS/ },
                    { s: 'OS/2', r: /OS\/2/ },
                    { s: 'Search Bot', r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/ }
                ];
                for (var id in clientStrings) {
                    var cs = clientStrings[id];
                    if (cs.r.test(nAgt)) {
                        os = cs.s;
                        break;
                    }
                }

                var osVersion = unknown;

                if (/Windows/.test(os)) {
                    osVersion = /Windows (.*)/.exec(os)[1];
                    os = 'Windows';
                }

                switch (os) {
                    case 'Mac OS X':
                        osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'Android':
                        osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                        break;

                    case 'iOS':
                        osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                        osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                        break;
                }

                // flash (you'll need to include swfobject)
                /* script src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js" */
                var flashVersion = 'no check';
                if (typeof swfobject != 'undefined') {
                    var fv = swfobject.getFlashPlayerVersion();
                    if (fv.major > 0) {
                        flashVersion = fv.major + '.' + fv.minor + ' r' + fv.release;
                    }
                    else {
                        flashVersion = unknown;
                    }
                }
            }

            window.jscd = {
                screen: screenSize,
                browser: browser,
                browserVersion: version,
                browserMajorVersion: majorVersion,
                mobile: mobile,
                os: os,
                osVersion: osVersion,
                cookies: cookieEnabled,
                flashVersion: flashVersion
            };


            var clientInfo = 'OS: ' + jscd.os + ' ' + jscd.osVersion + '\n' +
                'Browser: ' + jscd.browser + ' ' + jscd.browserMajorVersion +
                ' (' + jscd.browserVersion + ')\n' +
                'Mobile: ' + jscd.mobile + '\n' +
                'Flash: ' + jscd.flashVersion + '\n' +
                'Cookies: ' + jscd.cookies + '\n' +
                'Screen Size: ' + jscd.screen + '\n\n' +
                'Full User Agent: ' + navigator.userAgent;
            var url = window.location.href;
            var urlSource = document.referrer;
            var updatingObj = {
                "active": "1",
                "accountId": 0,
                "guId": "Log",
                "name": "Log",
                "entityCode": key,
                "info": clientInfo,
                "objectOld": object,
                "objectNew": object,
                "url": url,
                "urlSource": urlSource,
                "ipAddress": "Log",
                "device": jscd.mobile ? "MOBILE" : "PC",
                "browser": jscd.browser + "-" + jscd.browserMajorVersion,
                "os": jscd.os + "-" + jscd.osVersion,
                "userAgent": navigator.userAgent,
                "description": "Log",
                "createdTime": "2018-12-10 00:00:00"
            };

            updatingObj.id = 1;
            updatingObj.active = 1;
            //updatingObj.createdTime = new Date();
            //console.log(updatingObj);
            $.ajax({
                url: systemURL + "activityLog/api/add",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(updatingObj),
                success: function (responseData) {
                    // debugger;
                    if (responseData.status == 201 && responseData.message === "CREATED") {
                        console.log("data log success");
                    }
                },
                error: function (e) {
                    console.log(e.message);
                    console.log("data log failed");
                }
            });
        }

        var showItems = ["id", "value", "createdTime", "biddingKey", "description",];
        var dataSource = [];
        var updatingItemID = 0;
        var tableUpdating = 0;
        var table;
        var isBiddingCancelStatus = 0; // Trạng thái giá đã hủy. Nếu đã hủy sẽ mất quyền tiếp tục đấu giá.
        $(document).ready(function () {
            checkBiddingStatus(currentAuctionPropertyId);
           if ("NOT" == "NOW" || "NOT" == "KNOCKOUT" || "NOT" == "ONLINE" || "NOT" == "OFFLINE" || "NOT" == "OFFLINE_CONTROL") {
                loadData();
            }
            setTimeout(function () { formatNumber() }, 350);
        })

        function formatNumber3() {
            $('.child .dtr-details .dtr-data').each(function (event) {
                $(this).text(function (index, value) {
                    return value
                        .replace(/\D/g, '')
                        .replace(/\B(?=(\d{3})+(?!\d))/g, '.');
                })
            })
        }

       function loadData() {
               var urlload = "https://lacvietauction.vn/Bidding/api/ListBiddingByUser/"+0 + "/" + '1002395'
        if ("0" == "1001210") {
             urlload = "https://lacvietauction.vn/Bidding/api/ListBiddingApAuctioneer?auctionPropertyId="+'1002395'
        }
            $.ajax({
                url: urlload,
                type: "GET",
                contentType: "application/json",
                success: function (responseData) {
                    //debugger;
                    var data = responseData.data;
                    dataSource = data;
                    data.forEach(function (item, index) {
                        var rowContent = "";
                        if ((item["value"] == $("#realTimeHighestPrice").text().replace(/\D/g, '')) && (item.biddingStatusId != 1000003)) {
                            rowContent += "<td style='text-align: center;'><img style='cursor: pointer; width: 15.06px; height: 19.94px;' src = '../auctionart/upload/image/FlagBanner.png' alt= '' ></td>";
                        }
                        else rowContent += "<td style='text-align: center;'>" + (index + 1) + "</td>";
                        for (var key in item) {
                            if (item.hasOwnProperty(key)) {
                                // console.log(key + " -> " + item[key]);
                                if (showItems.includes(key)) {
                                    if (key == "description") {
                                        if (item["value"] == $("#realTimeHighestPrice").text().replace(/\D/g, '')) {
                                            rowContent += "<td class='row" + item.id + "-column column-" + key + "' property='" + key + "'><div style='color:green;'>Giá trả cao nhất</div></td>";
                                        }
                                        else {
                                            rowContent += "<td class='row" + item.id + "-column column-" + key + "' property='" + key + "'><div style='color:#b41712'>Giá trả thấp hơn</div></td>";
                                        }
                                    }
                                    else if (key == "biddingKey") {
                                        if ("False" == "True"){
                                                   rowContent += "<td class='row" + item.id + "-column column-" + key + "' title='" + item[key] + "' data='" + item[key] + "' property='" + key + "'><i title='" + item[key] + "' class='fa fa-ticket fa-2x' style='color:green; font-size: 22px; margin-right: 5px' aria-hidden='true'></i>" + item[key].substring(0, 10) + " <span class='bscscan-redirect' onclick='CheckDetailTransactionBSC(" + item.guId + ")'><img style='width: 20px; margin-left: 10px' src='../files/frontend/images/upload/img-search.png'>Tra cứu trên Blockchain</span></td>";

                                        }
                                        else {
                                              rowContent += "<td class='row" + item.id + "-column column-" + key + "' title='" + item[key] + "' data='" + item[key] + "' property='" + key + "'><i title='" + item[key] + "' class='fa fa-ticket fa-2x' style='color:green; font-size: 22px; margin-right: 5px' aria-hidden='true'></i>" + item[key].substring(0, 10) + "</td>";
                                        }
                                    }
                                    else if (key == "value") {
                                        rowContent += "<td class='novaticPrice row"+item.id+"-column column-"+key+"' property='"+key+"'>" + item[key] + "</td>";
                                    }
                                    else if (key == "createdTime") {
                                        //var sendDate = item[key];
                                        //var newDate = new Date(sendDate);
                                        //newDate = newDate.toLocaleString();
                                        rowContent += "<td class='row" + item.id + "-column column-" + key + "'  title='"+formatDisplayTimeWithMicrosecondsFromDBTime(item[key])+"'  property='" + key + "'>" + formatDisplayTimeWithMilisecondsFromDBTime(item[key]) + "</td>";
                                    }
                                    else {
                                        rowContent += "<td class='row"+item.id+"-column column-"+key+"' property='"+key+"'>" + item[key] + "</td>";
                                    }
                                }
                            }
                        }

                        if ((item["value"] == $("#realTimeHighestPrice").text().replace(/\D/g, '')) && (item.biddingStatusId != 1000003)) {
                            rowContent += "<td class='inactive' style='text-align: center;'><a onclick='InactiveBiddingItem(" + item.id + ")'><img class='withdraw-price-icon' style='cursor: pointer; width: 18px; height: 19.5px;'src='../auctionart/upload/image/Trash2.png' alt=''></a></td>";
                        }
                        else if ((item.biddingStatusId == 1000003)) {
                            isBiddingCancelStatus = 1;
                            //Đã hủy giá trả và bị truất quyền đấu giá
                            updateDisplayBiddingCancel();
                            rowContent += "<td class='row"+item.id+"-column column-"+key+"' property='"+key+"'>Đã hủy</td>";
                        }
                        else{
                            //rowContent += "<td class='row"+item.id+"-column column-"+key+"' property='"+key+"'>Không thể huỷ trả giá</td>";
                            rowContent += "<td class='row"+item.id+"-column column-"+key+"' property='"+key+"'></td>";
                        }
                        var newRow = "<tr id='row"+item.id+"' >" + rowContent + "</tr>";
                        $(newRow).appendTo($("#tableData tbody"));
                        $(newRow).appendTo($("#tableData2 tbody"));


                        // $("#tableData #dummyRow").after("<tr id='row"+item.id+"' >" + rowContent + "</tr>");
                    });

                    //Init datatable
                    if (tableUpdating === 0) {
                        initTable();
                    }

                },
                error: function (e) {
                    //console.log(e.message);
                    initTable();
                }
            });
        }


        function updateTableNewBidding(item) {
            //var rowContent = "";
            //rowContent += "<td style='text-align: center;'>" + (index + 1) + "</td>";

            //var addedItems = obj;
            var addedValues = [];
            //xóa nút rút giá
            $("#tableData tr td img.withdraw-price-icon").remove();
            $("#tableData2 tr td img.withdraw-price-icon").remove();

            //addedItems.forEach(function (item, index) {
            // console.log(item, index);
            var rowContent = "";
            addedValues.push("<td style='text-align: center;'></td>");
            for (var key in item) {
                if (item.hasOwnProperty(key)) {
                    // console.log(key + " -> " + item[key]);
                    if (showItems.includes(key)) {
                        if (key == "createdTime") {
                            addedValues.push("<td class='row" + item.id + "-column column-" + key + "' property='" + key + "'>" + formatDisplayTimeFromDBTime(item[key]) + "</td>");
                        }
                        if (key == "biddingKey") {
                            if ("False" == "True") {
                                            addedValues.push("<td class='row" + item.id + "-column column-" + key + "' property='" + key + "'>   <i title='" + item[key] + "' class='fa fa-ticket fa-2x' style='color:green; font-size: 22px; margin-right: 5px' aria-hidden='true'></i>" + item[key].substring(0, 10) + " <span class='bscscan-redirect' onclick='CheckDetailTransactionBSC(" + item.guId + ")'><img style='width: 20px; margin-left: 10px' src='../files/frontend/images/upload/img-search.png'>Tra cứu trên Blockchain</span></td>");

                            }
                            else {
                            addedValues.push("<td class='row" + item.id + "-column column-" + key + "' property='" + key + "'>   <i title='" + item[key] + "' class='fa fa-ticket fa-2x' style='color:green; font-size: 22px; margin-right: 5px' aria-hidden='true'></i>" + item[key].substring(0, 10) + "</td>");
                            }
                        }
                        if (key != "createdTime" && key != "biddingKey" && key != "photo") {
                            addedValues.push("<td class='row" + item.id + "-column column-" + key + "' property='" + key + "'>" + item[key] + "</td>");
                        }
                    }
                }
            }
            //addedValues.push("<td style='text-align: center;'><a onclick='editItem(" + item.id + ")'><i class='fa fa-edit fa-2x' style='color:#03a9f4'></i></a></td>");
            addedValues.push("<td class='inactive' style='text-align: center;'><a onclick='InactiveBiddingItem(" + item.id + ")'><img class='withdraw-price-icon' style='cursor: pointer; width: 18px; height: 19.5px;'src='../auctionart/upload/image/Trash2.png' alt=''></td>");

            table.row.add(addedValues).draw();
            table2.row.add(addedValues).draw();
            //Cập nhật khóa ngoài
            //1. format tiền
            $("#tableData tbody tr:first-child td:nth-child(3)").addClass("column-value");
            $("#tableData2 tbody tr:first-child td:nth-child(3)").addClass("column-value");
            formatNumber();

            //2. format thời gian
            $("#tableData tbody tr:first-child td:nth-child(5)").text(formatDisplayTimeWithMilisecondsFromDBTime(item.createdTime));
            $("#tableData tbody tr:first-child td:nth-child(5)").attr("title", formatDisplayTimeWithMilisecondsFromDBTime(item.createdTime));
            $("#tableData2 tbody tr:first-child td:nth-child(5)").text(formatDisplayTimeWithMilisecondsFromDBTime(item.createdTime));
            $("#tableData2 tbody tr:first-child td:nth-child(5)").attr("title", formatDisplayTimeWithMilisecondsFromDBTime(item.createdTime));

            //3. biddingStatus
            $("#tableData tbody tr:first-child td:nth-child(4)").html(`<div style='color:green;'>Giá trả cao nhất</div>`);
            $("#tableData tbody tr:nth-child(2) td:nth-child(4)").html(`<div style='color:#b41712;'>Giá trả thấp hơn</div>`);
            $("#tableData2 tbody tr:first-child td:nth-child(4)").html(`<div style='color:green;'>Giá trả cao nhất</div>`);
            $("#tableData2 tbody tr:nth-child(2) td:nth-child(4)").html(`<div style='color:#b41712;'>Giá trả thấp hơn</div>`);

            //4. ticket
            var ticketIcon = `<i title="` + item.biddingKey + `" class="fa fa-ticket fa-2x" style="color:green; font-size: 22px; margin-right: 5px" aria-hidden="true"></i>`;
              if ("False" == "True"){
                           $("#tableData tbody tr:first-child td:nth-child(6)").html('<div>' + ticketIcon + item.biddingKey.substring(0, 10)+ ' <span class="bscscan-redirect" onclick="CheckDetailTransactionBSC(' + item.guId + ')"><img style="width: 20px; margin-left: 10px" src="../files/frontend/images/upload/img-search.png">Tra cứu trên Blockchain</span></td>');
              }
              else {
                $("#tableData tbody tr:first-child td:nth-child(6)").html(ticketIcon + item.biddingKey.substring(0, 10));
                $("#tableData2 tbody tr:first-child td:nth-child(6)").html(ticketIcon + item.biddingKey.substring(0, 10));
              }


            //5. cancel button
            $("#tableData tbody tr:nth-child(2) td:nth-child(7)").html(``);
            $("#tableData2 tbody tr:nth-child(2) td:nth-child(7)").html(``);
            //});
        }

           function updateDisplayBiddingCancel(rowId) {
            //debugger;
            //Cập nhật text cho cột Rút lại giá
            if (typeof (rowId) != "undefined") {
                //$("#" + rowId + "  td:last-child").text("Đã hủy");
                $("#tableData tr td:last-child").text("Đã rút");
                $("#tableData2 tr td:last-child").text("Đã rút");

                //$("#tableData tr th:last-child").text("Đã hủy");
            }
            //Ẩn khối đặt lệnh do đã bị truất quyền
               $("#div-bid-form").html("<h5 class='timeopen-auctionproperty' style='color: #b41712; font-weight: bold; font-size: 16px; font-family: inter, san-serif; margin-top: 5px'>Bạn đã bị truất quyền đấu giá</h5>");
               $("#topBiddingPrice").css("display", "none");
               $("#btnUserHighestPrice").css("display", "none");



        }

                //Hùng add hàm initTable local để xử lý responsive
        function initTable() {
            table = $('#tableData').DataTable({
                aLengthMenu: [
                    [-1, 10, 25, 50, 100, 200],
                    ['Tất cả', 10, 25, 50, 100, 200]
                ],
                'order': [
                    [1, 'desc']
                ],
                responsive: true,
                "oLanguage": {
                    "sUrl": "/LVT/assets/js/tableBidding.json"
                },
                "initComplete": function (settings, json) {
                    table.on('order.dt search.dt', function () {
                        table.column(0, {
                            search: 'applied',
                            order: 'applied'
                        }).nodes().each(function (cell, i) {
                            if (i == 0) {
                                cell.innerHTML = '<img style="width: 15.06px; height: 19.94px;" src="/auctionart/upload/image/FlagBanner.png">';
                            }
                            else {
                                cell.innerHTML = i + 1;
                            }
                        });
                    }).draw();


                    $('#tableData tfoot th').each(function () {
                        var title = $(this).text();
                        $(this).html("<input type='text' class='tableFooterFilter' placeholder=' ' />");
                    });

                    table.columns().every(function () {
                        var that = this;
                        $('input.tableFooterFilter', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                }
            });

            setTimeout(function () {
                table.page.len(10).draw();
            }, 1000);

            table2 = $('#tableData2').DataTable({
                aLengthMenu: [
                    [-1, 10, 25, 50, 100, 200],
                    ['Tất cả', 10, 25, 50, 100, 200]
                ],
                'order': [
                    [1, 'desc']
                ],
                responsive: true,
                "oLanguage": {
                    "sUrl": "/LVT/assets/js/tableBidding.json"
                },
                "initComplete": function (settings, json) {
                    table2.on('order.dt search.dt', function () {
                        table2.column(0, {
                            search: 'applied',
                            order: 'applied'
                        }).nodes().each(function (cell, i) {
                            if (i == 0) {
                                cell.innerHTML = '<img style="width: 15.06px; height: 19.94px;" src="/auctionart/upload/image/FlagBanner.png">';
                            }
                            else {
                                cell.innerHTML = i + 1;
                            }
                        });
                    }).draw();


                    $('#tableData2 tfoot th').each(function () {
                        var title = $(this).text();
                        $(this).html("<input type='text' class='tableFooterFilter' placeholder=' ' />");
                    });

                    table2.columns().every(function () {
                        var that = this;
                        $('input.tableFooterFilter', this.footer()).on('keyup change', function () {
                            if (that.search() !== this.value) {
                                that.search(this.value).draw();
                            }
                        });
                    });
                }
            });

            setTimeout(function () {
                table2.page.len(10).draw();
            }, 1000);
        }

        "use strict";
        var checkSecondWinner = [];
        var IsSendBiddingRequest = "0";
        var AuctionPropertyTypeId = "1000002";
        var IsPropertyOwner = "0";
        if (IsSendBiddingRequest == 1 || AuctionPropertyTypeId == "1000001" || parseInt(IsPropertyOwner) == 1) {
            if (!(typeof (currentAccountId) == "undefined") && !(typeof (currentAuctionPropertyId) == "undefined")) {
                var connection = new signalR.HubConnectionBuilder().withUrl("/biddingHub?location=USER_BIDDING_ROOM&state=BIDDING&auctionPropertyId=" + currentAuctionPropertyId + "&accountId=" + currentAccountId).build();
            }

            else {
                var connection = new signalR.HubConnectionBuilder().withUrl("/biddingHub").build();
            }
            connection.on("ReceiveMessage", function (user, message) {
                var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var encodedMsg = user + " says " + msg;
                var li = document.createElement("li");
                li.textContent = encodedMsg;
                document.getElementById("messagesList").appendChild(li);
            });
            connection.on("ReceiveBiddingPrice", function (user, message) {
                var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var encodedMsg = "Cập nhật giá đặt của user: " + user + " says " + msg;
                var li = document.createElement("li");
                li.textContent = encodedMsg;
                document.getElementById("messagesList").appendChild(li);
                //getData object:
                try {
                    var dataObj = JSON.parse(message);
                    if (parseInt(currentAuctionPropertyId) === dataObj.AuctionPropertyId) {
                        checkCurrentHighestBiddingPrice(dataObj.AccountId);
                        //Harry - thêm dòng vào bảng - show tất cả giá của mọi người
                        //Kiểm tra đã có chưa
                        //var currentHighestId = parseInt($("#tableData tbody tr:first-child td:nth-child(2)").text());
                        //var newRowObj = { id: dataObj.Id, counterpartyId: dataObj.CounterpartyId,  createdTime: dataObj.CreatedTime, biddingKey: dataObj.BiddingKey, value: dataObj.Value };
                        //if (currentAccountId !== parseInt(dataObj.AccountId)) {
                        //    updateTableNewBidding(newRowObj);
                        //}
                        //bỏ logic này chuyển về chỉ show của người đang đăng nhập
                    }
                } catch (e) {

                }
            });
            connection.on("StartAuction", function (user, message, openTimeStamp) {
                if ("1002395" == message) {
                     $(".auctionproperty-status").text('Đang diễn ra');
                temp = 1;
                if ("NO" == "YES") {
                    $("#propertyOpenPrice").show();
                    $("#realTimeHighestPriceContainer").show();
                    $("#realTimeHighestPrice").text(345116000);
                    biddingStatus = 1;
                }
                else {
                    if ("0" == "1" || "1000002" == "1000001" || parseInt('0') == 1) {
                        $("#propertyOpenPrice").show();
                        $("#realTimeHighestPriceContainer").show();
                        $("#realTimeHighestPrice").text(345116000);
                        biddingStatus = 1;
                    }
                    else {
                        $("#propertyOpenPrice").remove();
                    }
                }
                $("#valueCountDown").css("display", "block");
                $(".bidding-div").css("display", "block");
                $("#valueCountDown-waiting").css("display", "none");
                var time = Math.floor(openTimeStamp) +  - (7 * 60 * 60);
                $("#valueCountDown").attr("data-time", time);
                countDown2(time * 1000);
                OpenPopupStartAuction(message);
                }
            });
            connection.on("StopAuction", function (user, message, closeTimeStamp) {
                if ("1002395" == message) {
                     var time = Math.floor(closeTimeStamp) + 2 - (7 * 60 * 60);
                    $("#valueCountDown").attr("data-time", time);
                    $("#valueCountDown").css("display", "none");
                    $(".bidding-div").css("display", "none");
                    $("#valueCountDown-waiting").css("display", "none");
                    $(".price-open-div").css("display", "none");
                    $(".time-auctionproperty-div").css("display", "none");
                    $(".auctionproperty-status").text('Đã kết thúc');
                    $(".result-button").css("display", "block");
                    countDown2(time*1000);
                    StopPopupStartAuction(message);
                    $("#FormCountDown").css("display", "none");
                }

            });
            connection.on("SendInactiveBiddingByUser", function (user, message) {
                var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
                var encodedMsg = user + " says " + msg;
                var li = document.createElement("li");
                li.textContent = encodedMsg;
                document.getElementById("messagesList").appendChild(li);
                checkCurrentHighestBiddingPrice();
                try {
                    var dataObj = JSON.parse(message);
                    var obj = dataSource.find(x => x.id == dataObj.AuctionPropertyId);
                    //Check User đăng nhập
                    // Nếu là User thứ 2 thì chỉnh sửa nó là giá cao nhất và thêm nút rút lại giá
                    if (1000002 == dataObj.BiddingStatusId) {
                        if (0 == dataObj.AccountId) {
                            var stepPrice = "10000000";
                            $("#btnUserHighestPrice").show();
                            $("#realTimeHighestPrice").text(dataObj.Value);
                            var newValue = dataObj.Value + parseInt(stepPrice);
                            $("#bidValue").val(newValue).attr("min", newValue);
                            $("#tableData tbody tr#row" + dataObj.Id + " .column-description ").html('<div style="color:green;">Giá trả cao nhất</div>');
                            $("#tableData tbody tr#row" + dataObj.Id + " .column-biddingMeta ").html('<a onclick="InactiveBiddingItem(' + dataObj.Id + ')"><img class="withdraw-price-icon" style="cursor: pointer; width: 18px; height: 19.5px;" src="../auctionart/upload/image/Trash2.png" alt=""></a>');
                            $("#tableData2 tbody tr#row" + dataObj.Id + " .column-biddingMeta ").html('<a onclick="InactiveBiddingItem(' + dataObj.Id + ')"><img class="withdraw-price-icon" style="cursor: pointer; width: 18px; height: 19.5px;" src="../auctionart/upload/image/Trash2.png" alt=""></a>');
                            rowContent += "<td class='inactive' style='text-align: center;'></td>";

                            formatNumber();

                        }
                    }
                }
                catch (e) {

                }
            });
            connection.start().catch(function (err) {
                return console.error(err.toString());
            });
            async function start() {
                try {
                    await connection.start();
                    console.log("Reconnected success");
                } catch (err) {
                    console.log(err);
                    setTimeout(() => start(), 5000);
                }
            };

            connection.onclose(async () => {
                await start();
            });
            document.getElementById("sendButton").addEventListener("click", function (event) {
                var user = document.getElementById("userInput").value;
                var message = document.getElementById("messageInput").value;
                connection.invoke("SendMessage", user, message).catch(function (err) {
                    return console.error(err.toString());
                });
                event.preventDefault();
            });
            document.getElementById("sendButtonBidding").addEventListener("click", function (event) {
                var user = document.getElementById("userInput").value;
                var message = document.getElementById("messageInput").value;
                connection.invoke("SendBiddingPrice", user, message).catch(function (err) {
                    return console.error(err.toString());
                });
                event.preventDefault();
            });
        }
        function sendRealTimeBiddingPrice(message, data) {
            connection.invoke("SendBiddingPrice", message, data).catch(function (err) {
                return console.error(err.toString());
            });
        }
        function CheckDetailTransactionBSC(actionId){
                    $.ajax({
                      url: systemURL + "balance/api/GetTransactionByActionId/" + actionId,
                        type: "GET",
                        contentType: "application/json",
                        success: function (responseData) {
                            if (responseData.status == 200 && responseData.message === "SUCCESS") {
                                var data = responseData.data[0]
                                if (data.transactionHash != null){
                                         redirectBscScan(data.transactionHash);
                                }
                                else {
                                Swal.fire(
                                    'Giao dịch đang được cập nhật',
                                    'Vui lòng thử lại sau ít phút',
                                    'warning'
                                );
                                }
                            }
                        },
                        error: function (e) {
                            //console.log(e.message);
                            Swal.fire(
                                'Giao dịch đang được cập nhật',
                                'Vui lòng thử lại sau ít phút',
                                'warning'
                            );
                        }
                    });
        }
            function redirectBscScan(transactionHash) {
                var url = "https://bscscan.com/tx/" + transactionHash + "#eventlog";
                window.open(url);
        }
           function RedirectSmartContract(smartcontract){
               //console.log(smartcontract);
               var url = "https://bscscan.com/address/" + smartcontract;
                    window.open(url);
            }
             function RedirectSessionAddress(addressSession){
               //console.log(smartcontract);
                 var url = "https://bscscan.com/address/" + addressSession + "#events";
                    window.open(url);
            }

