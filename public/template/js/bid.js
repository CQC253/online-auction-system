
//Xử lí giảm 1 step
function MinusPrice() {
    var stepInput = document.getElementById("step");
    var currentStep = parseInt(stepInput.value);

    if (!isNaN(currentStep) && currentStep > 1) {
        stepInput.value = currentStep - 1;
    } else {
        stepInput.value = 1;
    }
}

//Xử lí tăng 1 step
function PlusPrice() {
    var stepInput = document.getElementById("step");
    var currentStep = parseInt(stepInput.value);

    if (!isNaN(currentStep)) {
        stepInput.value = currentStep + 1;
    } else {
        stepInput.value = 1;
    }
}

//Xử lí khi bấm trả giá
function BiddingNow() {
    // Kiểm tra người dùng có phải là người trả giá cao nhất hay không
    var UserHighestPrice = document.getElementById("UserHighestPrice");


    if (confirm('Bạn có muốn trả giá không?')) {
        // Nếu người dùng chọn OK trong hộp thoại xác nhận, gửi yêu cầu đến máy chủ
        if (UserHighestPrice.style.display === "none") {
            // Cho phép submit form
            document.getElementById("bid-form").submit();
        }else {
            // Hiển thị thông báo
            alert("Bạn đang là người trả giá cao nhất. Không thể trả giá.");
        }
    } else {
        // Nếu người dùng chọn Cancel, không thực hiện hành động gì
        return false;
    }
}

//-----------------------
function numberFormat(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
$(document).ready(function(){
    //Xử lí AJAX giá cao nhất hiện tại 
    //tạo biến timer_highest_price để lấy độ trễ 1s
    var tmpPrice = 0;
    var timer_highest_price;
    function getHighestPrice(){
        clearTimeout(timer_highest_price); //reset timer_highest_price

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') //meta của master layout
            }
        });
        
        $.ajax({
            type:'GET', //method
            url:'/auctions/'+auctionId+'/highest_price', 
            success:function(data){
                console.log(data); //tương tự dd();
    
                var highest_price = data.highest_price;
                var formatted_price = numberFormat(highest_price) ;
                if (highest_price) {
                    $('#highestPrice').html(formatted_price);
                    // Display a success toast, with a title
                    if (highest_price > tmpPrice) {
                        toastr.success('Giá đã được cập nhật')
                    }
                    tmpPrice =  highest_price;
                }
                 
            },
            error:function (err) {
                console.log(err);
            }
        });

        timer_highest_price = setTimeout(getHighestPrice, 1000);
    }
    getHighestPrice();  
    
}) 

//----------------------------------
$(document).ready(function(){
    // Xử lí AJAX Bạn đang trả giá cao nhất
    // Tạo biến timer_user_highest_price để lấy độ trễ 1s
    var timer_user_highest_price;
    function getUserHighestPrice() {
        clearTimeout(timer_user_highest_price); // Reset timer_user_highest_price
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') // trong <meta> của master layout
            }
        });
        
        $.ajax({
            type: 'GET', // Method
            url: '/auctions/' + auctionId + '/user_highest_price', 
            success: function(data) {
                console.log(data); // Tương tự dd() trong php, thấy được ở console trong serve ;
    
                var userHighestPrice = data.user_highest_price; //sau khi request AJAX thành công và nhận được dữ liệu data từ server(thấy đc ở console), thì data này đc giả định cho biến user_highest_price sau đó gán cho biến UserHighestPrice
                var userHighestPriceElement = $('#UserHighestPrice');
                
                if (userHighestPrice) {
                    userHighestPriceElement.show();
                    userHighestPriceElement.html('Bạn đang trả giá cao nhất');
                } else {
                    userHighestPriceElement.hide();
                    userHighestPriceElement.html('');
                }
            },
            error: function(err) {
                console.log(err);
            }
        });

        timer_user_highest_price = setTimeout(getUserHighestPrice, 1000);
    }
    getUserHighestPrice();  
    
})






