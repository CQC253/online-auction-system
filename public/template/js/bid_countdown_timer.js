(function (factory) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else {
        factory(jQuery);
    }
  })(function ($) {
  
    "use strict";

    var BidCountdown = function (element, options) {
            this.$element = $(element);
            this.defaults = $.extend({}, BidCountdown.defaults, this.$element.data(), $.isPlainObject(options) ? options : {});
            this.init();
        };
  
    BidCountdown.prototype = {
        constructor: BidCountdown,
  
        init: function () {
            var content = this.$element.html(),
                date = new Date(this.defaults.date || content);
  
            if (date.getTime()) {
                this.content = content;
                this.date = date;
                this.find();
  
                if (this.defaults.autoStart) {
                    this.start();
                }
            }
        },
  
        find: function () {
            var $element = this.$element;
  
            this.$days = $element.find("[data-days]");
            this.$hours = $element.find("[data-hours]");
            this.$minutes = $element.find("[data-minutes]");
            this.$seconds = $element.find("[data-seconds]");
  
            if ((this.$days.length + this.$hours.length + this.$minutes.length + this.$seconds.length) > 0) {
                this.found = true;
            }
        },
  
        reset: function () {
            if (this.found) {
                this.output("days");
                if(this.hours<10){
                    this.output("hours10");
                }
                else {
                    this.output("hours");
                }
                if(this.minutes<10){
                    this.output("minutes10");
                }
                else {
                    this.output("minutes");
                }
                if (this.seconds < 10) {
                    this.output("seconds10");
                }
                else{
                    this.output("seconds");
                }
  
            } else {
                this.output();
            }
        },
  
        ready: function () {
            var date = this.date,
                decisecond = 100,
                second = 1000,
                minute = 60000,
                hour = 3600000,
                day = 86400000,
                remainder = {},
                diff;
  
            if (!date) {
                return false;
            }
  
            diff = date.getTime() - (new Date()).getTime();
  
            if (diff <= 0) {
                this.end();
                return false;
            }
  
            remainder.days = diff;
            remainder.hours = remainder.days % day;
            remainder.minutes = remainder.hours % hour;
            remainder.seconds = remainder.minutes % minute;
            remainder.milliseconds = remainder.seconds % second;
  
            this.days = Math.floor(remainder.days / day);
            this.hours = Math.floor(remainder.hours / hour);
            this.minutes = Math.floor(remainder.minutes / minute);
            this.seconds = Math.floor(remainder.seconds / second);
            this.deciseconds = Math.floor(remainder.milliseconds / decisecond);
  
            return true;
        },
  
        start: function () {
            if (!this.active && this.ready()) {
                this.active = true;
                this.reset();
                this.autoUpdate = this.defaults.fast ?
                    setInterval($.proxy(this.fastUpdate, this), 100) :
                    setInterval($.proxy(this.update, this), 1000);
            }
        },
  
        stop: function () {
            if (this.active) {
                this.active = false;
                clearInterval(this.autoUpdate);
            }
        },
  
        end: function () {
          if (!this.date) {
              return;
          }
      
          this.stop();
      
          this.days = 0;
          this.hours = 0;
          this.minutes = 0;
          this.seconds = 0;
          this.deciseconds = 0;
          this.reset();
          this.defaults.end();
            // Hiển thị phần đấu giá hoàn thành
            showCompleteAuctionSection() 

            // Cập nhật trạng thái
            updateStatus();
      },
  
        destroy: function () {
            if (!this.date) {
                return;
            }
  
            this.stop();
  
            this.$days = null;
            this.$hours = null;
            this.$minutes = null;
            this.$seconds = null;
  
            this.$element.empty().html(this.content);
            this.$element.removeData("bid_countdown");
        },
  
        fastUpdate: function () {
            if (--this.deciseconds >= 0) {
                this.output("deciseconds");
            } else {
                this.deciseconds = 9;
                this.update();
            }
        },
  
        update: function () {
            if (--this.seconds >= 0) {
                if (this.seconds < 10){
                    this.output("seconds10");
                }
                else {
                    this.output("seconds");
                }
            } else {
                this.seconds = 59;
                this.output("seconds");
                if (--this.minutes >= 0) {
                    if (this.minutes < 10){
                        this.output("minutes10");
                    }
                    else {
                        this.output("minutes");
                    }
                } else {
                    this.minutes = 59;
                    this.output("minutes");
                    if (--this.hours >= 0) {
                        if (this.hours < 10){
                            this.output("hours10");
                        }
                        else {
                            this.output("hours");
                        }
                    } else {
                        this.hours = 23;
                        this.output("hours");
                        if (--this.days >= 0) {
                            this.output("days");
                        } else {
                            this.end();
                        }
                    }
                }
            }
        },
  
        output: function (type) {
            if (!this.found) {
                this.$element.empty().html(this.template());
                return;
            }
  
            switch (type) {
                case "deciseconds":
                    this.$seconds.text(this.getSecondsText());
                    break;
                case "seconds10":
                    this.$seconds.text("0" + this.seconds);
                    break;
  
                case "seconds":
                    this.$seconds.text(this.seconds);
                    break;
                case "minutes10":
                    this.$minutes.text("0" + this.minutes);
                    break;
                case "minutes":
                    this.$minutes.text(this.minutes);
                    break;
                case "hours10":
                    this.$hours.text("0" + this.hours);
                    break;
                case "hours":
                    this.$hours.text(this.hours);
                    break;
  
                case "days":
                    this.$days.text(this.days);
                    break;
  
            }
        },
  
        template: function () {
            return this.defaults.text
                    .replace("%s", this.days)
                    .replace("%s", this.hours)
                    .replace("%s", this.minutes)
                    .replace("%s", this.getSecondsText());
        },
  
        getSecondsText: function () {
            return this.active && this.defaults.fast ? (this.seconds + "." + this.deciseconds) : this.seconds;
        }
    };
  
    BidCountdown.defaults = {
        autoStart: true,
        date: null,
        fast: false,
        end: $.noop,
        text: "%s days, %s hours, %s minutes, %s seconds"
    };
  
    BidCountdown.setDefaults = function (options) {
        $.extend(BidCountdown.defaults, options);
    };
  
    $.fn.bid_countdown = function (options) {
        return this.each(function () {
            var $this = $(this),
                data = $this.data("bid_countdown");
  
            if (!data) {
                $this.data("bid_countdown", (data = new BidCountdown(this, options)));
            }
  
            if (typeof options === "string" && $.isFunction(data[options])) {
                data[options]();
            }
        });
    };
  
    $.fn.bid_countdown.constructor = BidCountdown;
    $.fn.bid_countdown.setDefaults = BidCountdown.setDefaults;
  
    $(function () {
        $("[bid_countdown]").bid_countdown();
    });
  
  });

//-----------------Bid------------------

// Hàm hiển thị phần hoàn thành đấu giá(bid) và ẩn đồng hồ đếm ngược, trả giá và nút đăng ký
function showCompleteAuctionSection() {
    var bid_countdownTimer = document.getElementById("countdown-timer");
    var bid_bidSection = document.getElementById("bid");
    var bid_buttonRegister = document.getElementById("button-register");
    var bid_biddingProcess = document.getElementById("bidding-process");
    var bid_completeAuctionSection = document.getElementById("complete-auction");

    bid_countdownTimer.style.display = "none";
    bid_bidSection.style.display = "none";
    bid_buttonRegister.style.display = "none";
    bid_biddingProcess.style.display = "none";
    bid_completeAuctionSection.style.display = "block";
}

//Hàm để cập nhật trạng thái
function updateStatus() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/auctions/" + auctionId + "/update_status", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                var response = JSON.parse(xhr.responseText);
                console.log(response)
                
                
                ;
            } else {
                console.error("Error:", xhr.status);
            }
        }
    };
    xhr.send();
}

//Hàm để gửi mail
function sendMail() {
    var xhr = new XMLHttpRequest();
    xhr.open("post", "/auctions/" + auctionId + "/send-email", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                var response = JSON.parse(xhr.responseText);
                console.log(response);
                // Xử lý kết quả sau khi gửi email thành công
            } else {
                console.error("Error:", xhr.status);
                // Xử lý lỗi khi gửi email
            }
        }
    };
    xhr.send();
}
//-----------End Bid---------------