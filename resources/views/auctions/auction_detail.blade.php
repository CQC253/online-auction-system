@extends('layouts.master')

{{--set page tittle--}}
@section('title','Auction Detail')

@push('js')
    <script>
        var auctionId = {{ request()->route()->id }};
    </script>
    
    <!-- bid js -->
    <script src="/template/js/bid.js"></script>
    <!-- countdown_timer -->    
    <script src="/template/js/countdown_timer.js"></script>
    <script src="/template/js/bid_countdown_timer.js"></script>
    <script src="/template/js/register_countdown_timer.js"></script>
@endpush

@section('content')
<div class="container" style="padding-top: 100px">
    <div id="banner-new" style="padding-top: 32px; padding-bottom: 26px; border-bottom: 1px solid #E0E0E0; ">
        <h2 class="current-page">{{ $auction_detail->product->name }}</h2>
        <div class="link-redirect">
            <a class="page-index" href="{{ route('home') }}">Trang chủ</a> / <span class="page-des">Tài sản đấu giá</span>
        </div>
    </div>
</div>

<div class="auction-details-section pt-120 section-ap-detail" style="padding-top: 60px">
    <div class="container" style="display: flex; ">   
        {{-- Slide show --}}
        <div class="row g-4 mb-50"></div>
            <div class="col-xl-7 col-lg-8 d-flex flex-row align-items-start justify-content-lg-start justify-content-center flex-md-nowrap flex-wrap gap-4 mode-register" id="photo-elements" style="flex: 1">
                <div class="w3-content w3-display-container" style="width: 100%">
                    <img class="mySlides" src="{{ json_decode($auction_detail->product->thumbnail)[0] }}" style="width:100%">
                    @foreach (json_decode($auction_detail->product->images) as $image)
                    <img class="mySlides" src="{{ $image }}" style="width:100%">
                    @endforeach
                  
                    <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                    <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
                </div>     
            </div>
            {{-- End slide show --}}
    
            <div class="col-xl-5 col-lg-4" style="flex: 1">
                <div class="product-details-right  wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".2s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.2s; animation-name: fadeInDown;"> 
                    {{-- ----------------------------Auction---------------------------- --}} 
                    {{-- -------------------Countdown Timer-- -------------------}} 
                    <div id="countdown-timer" style="display: block;">
                        <div class="timestamp-div">
                            <p style="font-size: 17px; color: #696969; font-family: Montserrat; text-align: center;">Thời gian đếm ngược bắt đầu trả giá:</p>
                            <div id="timestamp" style="display: flex; justify-content: space-around">
                                @include('timers.countdown_timer')
                            </div>
                        </div>
                    </div>
                    {{-- -------------------End Countdown Timer-- -------------------}} 
                    
                    {{------------------------------Bid------------------------------}}
                    <div id="bid" style="display: none;">
                        @if (Auth::check())
                            <div class="timestamp-div">
                                <p style="font-size: 17px; color: #696969; font-family: Montserrat; text-align: center;">Thời gian trả giá còn lại của tài sản</p>
                                <div id="timestamp" style="display: flex; justify-content: space-around">
                                    @include('timers.bid_countdown_timer')
                                </div>
                                <p id="knockout-guide" style="margin: 5px; line-height: 1.5;">
                                    <span style="font-size: 17px; font-weight: 500; color: #b41712 !important; font-family: Montserrat; margin: 0 5px; ">Nếu thời gian trên đây kết thúc và không có người trả giá cao hơn, cuộc đấu giá sẽ kết thúc.</span>
                                </p>
                            </div>
                            @if ($registered)
                                <div class="describe-content info-ap"> 
                                    <div class="bid-form" style="margin-top: 20px">
                                        <div class="form-title" style="margin-bottom: 0;">
                                            <h5 class="form-title-bidding">Đặt giá (VNĐ)</h5>
                                    
                                            <p style="position: unset; margin-bottom: 8px;">Giá cao nhất hiện tại: <span style="color: green; font-weight: bold; font-size: 22px; font-family: saira, sans-serif" id="highestPrice" class="novaticPrice">{{-- hiện giá cao nhất hiện tại ở đây --}}{{ number_format($current_highest_price, 0, ',', '.') . ' VNĐ' }}</span> VNĐ</p>
                                            <p style="position: unset; margin-bottom: 8px;">Giá khởi điểm: <span class="novaticPrice">{{ number_format($auction_detail->product->starting_price, 0, ',', '.') . ' VNĐ' }} </span></p>
                                            <p style="position: unset; margin-bottom: 8px !important;">Bước giá: <span id="step-price" class="novaticPrice">{{ number_format($auction_detail->price_step, 0, ',', '.') . ' VNĐ' }}</span></p>
                                            <p id="topBiddingPrice" style="position: unset; margin-bottom: 8px !important;">Giá cao nhất của bạn: <span style="color: black; font-weight: bold; font-size: 22px; font-family: saira, sans-serif" id="highestYourPrice" class="novaticPrice">{{-- hiện giá cao nhất của bạn ở đây --}}{{ number_format($highest_your_price, 0, ',', '.') . ' VNĐ' }}</span></p>                                            
                                            <p id="UserHighestPrice" style="position: unset; margin-bottom: 8px; font-size: 18px; color: #b41712; font-weight: 600; display: {{ $user_highest_price ? 'block' : 'none' }};"></p>                                            
                                        </div>
                                        <form action="{{ route('auction-bid',['id' => request()->route()->id] ) }}" method="post" id="bid-form">
                                            @csrf
                                            <div class="row" id="div-bid-form">
                                                <p style="position: unset; margin-bottom: 8px !important;">Số bước giá:</p>
                                                <div class="col-lg-9 bid-left-form" style="border-right: 1px solid #E0E0E0">
                                                    <div style="display: flex;">
                                                        <input type="button" title="Giảm một bước giá" value="-" class="minus item-buff" onclick="MinusPrice()">

                                                        <input class="input-novaticPrice novaticPrice" id="step" name="step" type="text" value="1" style=" border-top: 1px solid #E0E0E0; border-bottom: 1px solid #E0E0E0; text-align: center; font-weight: bold; font-size: 22px; font-family: saira, sans-serif; padding: 5px 15px; width: 100%; border-left: 0; border-right: 0; color: black; background: white ">

                                                        <input type="button" title="Tăng một bước giá" value="+" class="plus item-buff" onclick="PlusPrice()">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3" style=" display: flex; justify-content: center; align-items: center;">
                                                    <button id="bidButton" class="eg-btn btn--primary btn--sm" type="button" style="text-transform: unset;border: unset; width: 100%; padding: 10px 12px; font-size: 16px; " onclick="BiddingNow(); return false;">Trả giá</button>
                                                </div>
                                            </div>
                                        </form>  
                                    </div>
                                </div>
                            @else
                            <div class="ap-status-result-success" style="margin-top: 0px">
                                <div style="padding: 8px">
                                    <p class="ap-status-result-txt" style="font-family: Montserrat; font-size: 18px; text-align: center">Bạn đã bỏ qua thời gian đăng kí để đấu giá tài sản này</p>
                                </div>
                            </div>
                            @endif 
                        @else
                            {{-- Người dùng chưa đăng nhập --}}
                            {{-- <div class="ap-status-result-success" style="margin-top: 0px">
                                <div style="padding: 8px">
                                    <p class="ap-status-result-txt" style="font-family: Montserrat; font-size: 18px; text-align: center">Bạn đã bỏ qua thời gian đăng kí để đấu giá tài sản này</p>
                                </div>
                            </div> --}}
                        @endif 
                    </div>
                    {{------------------------------End Bid------------------------------}}
    
                    {{--- -------------------Auction Detail-- --------------------}}
                    <div id="auction-detail" style="display: block">
                        <div class="register-form" style="margin-top: 0px" >
                            <div class="row" style="">
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Mã tài sản:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ $auction_detail->product_id }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian mở đăng ký:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_detail->time_open_registration)); }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian kết thúc đăng ký:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_detail->time_close_registration)); }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Giá khởi điểm:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_detail->product->starting_price, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Phí đăng ký đấu giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_detail->registration_fee, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Bước giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_detail->price_step, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Số bước giá tối đa/ lần trả:</p>
                                </div>
                                <div class="col-6">
                                        <p class="right-info-text no-margin">{{ $auction_detail->max_step }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Tiền đặt trước:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_detail->down_payment, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Phương thức đấu giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ $auction_detail->auction_method }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Tên người bán:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ $auction_detail->product->seller->name }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian bắt đầu trả giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_detail->start_time)); }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian kết thúc trả giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_detail->end_time)); }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--- -------------------End Auction Detail-- --------------------}}
    
                    {{--- -------------------Button Register-- --------------------}}
                    <div id="button-register" style="display: block;"> {{-- xử lí --}}
                        {{-- Đồng hồ đếm kết thúc đăng kí --}}
                        <div style="display: none;">
                            @include('timers.register_countdown_timer')
                        </div>
    
                        @if (Auth::check())
                            {{-- Người dùng đã đăng nhập --}}
                            <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px;">
                                @if ($registered)
                                    <p style="text-transform: unset; border: unset; padding: 5px 20px; margin: 5px 10%; font-size: 16px; background:#b41712; width: 80%; text-align: center; color: #fff; border-radius: 5px; font-weight: 550" >Đã đăng ký đấu giá</p>
                                @else
                                    <a href="{{ route('auction-register',['id' => request()->route()->id] ) }}" style="text-transform: unset;border: unset; padding: 10px 20px; margin: 5px 10%; font-size: 16px; background: #b41712; width:80%; text-align:center; color: #fff; border-radius: 5px; font-weight: 550 ">Đăng ký tham gia đấu giá</a>
                                @endif
                            </div>
                        @else
                            {{-- Người dùng chưa đăng nhập --}}
                            <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px;     ">
                                <a href="{{ route('login', ['redirect' => route('auction-detail', ['id' => $auction_detail['id']]) ]) }}" style="text-transform: unset;border: unset; padding: 10px 20px; margin: 5px 10%; font-size: 16px; background: #b41712; width:80%; text-align:center; color: #fff; border-radius: 5px; font-weight: 550 ">Đăng ký tham gia đấu giá</a>
                            </div>
                        @endif
                    </div>    
                    {{-- -------------------End Button Register-- -------------------}}
    
                    {{-- -------------------Bidding Process-- -------------------}}
                    <div id="bidding-process" style="display: none;">
                        @if (Auth::check())
                            @if ($registered)
                            <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px; ">   
                                <p style="text-transform: unset; border: unset; padding: 5px 20px; margin: 5px 10%; font-size: 16px; background:#b41712; width: 80%; text-align: center; color: #fff; border-radius: 5px; font-weight: 550">Đấu giá đang diễn ra</p>
                            </div>
                            @else
                            <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px; ">   
                                <p style="text-transform: unset; border: unset; padding: 5px 20px; margin: 5px 10%; font-size: 16px; background:#b41712; width: 80%; text-align: center; color: #fff; border-radius: 5px; font-weight: 550">Chưa đăng kí đấu giá</p>
                            </div>
                            @endif
                        @else 
                            {{-- Người dùng chưa đăng nhập --}}
                            <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px;     ">
                                <a href="{{ route('login', ['redirect' => route('auction-detail', ['id' => $auction_detail['id']]) ]) }}" style="text-transform: unset;border: unset; padding: 10px 20px; margin: 5px 10%; font-size: 16px; background: #b41712; width:80%; text-align:center; color: #fff; border-radius: 5px; font-weight: 550 ">Đăng nhập để tiếp tục</a>
                            </div>
                        @endif 
                    </div>
                    {{-- -------------------End Bidding Process-- -------------------}}

                    {{-- -------------------Complete Auction-- -------------------}}
                    <div id="complete-auction" style="display: none;">
                        @if (Auth::check())
                            <div class="timestamp-div" style="margin-top: 20px; border: 1px solid #E0E0E0; background: white; border-radius: 5px;">
                                <div id="timestamp" style="display: block; justify-content: space-around;"><p class="check-result-txt" style="margin-bottom: 0 !important">Xem kết quả cuộc đấu giá <a href="{{ route('auction-result-bid',['id' => request()->route()->id]) }}" class="result-link">tại đây</a></p></div>
                            </div>
                            <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px; ">   
                                <p style="text-transform: unset; border: unset; padding: 5px 20px; margin: 5px 10%; font-size: 16px; background:#b41712; width: 80%; text-align: center; color: #fff; border-radius: 5px; font-weight: 550">Đấu giá đã kết thúc</p>
                            </div>
                        @else
                            <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px;     ">
                                <a href="{{ route('login', ['redirect' => route('auction-detail', ['id' => $auction_detail['id']]) ]) }}" style="text-transform: unset;border: unset; padding: 10px 20px; margin: 5px 10%; font-size: 16px; background: #b41712; width:80%; text-align:center; color: #fff; border-radius: 5px; font-weight: 550 ">Đăng nhập để xem kết quả</a>
                            </div>
                        @endif 
                    </div>
                    
                    
                    {{-- -------------------End Complete Auction-- -------------------}}
    
                    {{-- ---------------------------- End Auction ------------------------------}}
                </div>
            </div>
        </div>

        <div class="row d-flex justify-content-center g-4" style="padding-top: 80px">
            <h4 style="ctext-transform: unset;border: unset; padding: 15px 30px; margin: 10px 50px; font-size: 18px; background: #b41712; text-align:center; color: #fff; border-radius: 5px; font-weight: 550; width: 20% ">Thông tin đấu giá</h4>
        
            <div class="col-lg-12">
                <div class="register-form" style="display: block; border: none; margin: 0 50px;">
                    <div class="describe-content info-ap">
                        <h4 style="margin-bottom: 20px">Thông tin tổ chức đấu giá</h4>
                        <p class="des-ap">Tổ chức đấu giá tài sản: <span class="content-ap">Công ty đấu giá 2CQ Aucion</span></p>
                        <p class="des-ap">Đấu giá viên: <span class="content-ap">Châu Quang Chiến</span></p>
                        <p class="des-ap">Địa chỉ: <span class="content-ap">92 Quang Trung, P. Thạch Thang, Q. Hải Châu, TP. Đà Nẵng</span></p>
                    </div>
                    <div class="describe-content info-ap">
                        <h4 style="margin-bottom: 20px">Tài liệu liên quan</h4>
                        <p class="des-ap" style="margin-bottom: 10px"> a. Mô tả tài sản xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p> 
                        <p class="des-ap" style="margin-bottom: 10px"> b. Hồ sơ mời đấu giá xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p> 
                        <p class="des-ap" style="margin-bottom: 10px"> c. Quy chế đấu giá tài sản xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p> 
                        <p class="des-ap" style="margin-bottom: 10px"> d. Thông báo Đấu giá tài sản xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p>        
                    </div>    
                </div>
            </div>
        </div>
    </div> 
</div>
@endsection