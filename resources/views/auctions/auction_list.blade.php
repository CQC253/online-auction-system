@extends('layouts.master')

{{--set page tittle--}}
@section('title','Auction List ')

@push('css')
    <link rel="stylesheet" href="/template/css/list.css">
@endpush

@push('js')
    <!-- datepicker js -->
    <script src="/template/js/datepicker.js"></script>
    <script>
    document.querySelectorAll('.checkbox-status').forEach(function (radio){
        radio.addEventListener('change', function () {
            // Gửi yêu cầu tìm kiếm lại
            document.querySelector('form').submit();
        });
    });
    </script>
@endpush

@section('content')
<div class="container" style="margin-top: 100px">
    <div id="banner-new" style="padding-top: 32px; padding-bottom: 26px; border-bottom: 1px solid #E0E0E0; background-image: url('/LVT/assets/images/bannertaisandaugia.png');background-position: right;  background-repeat: no-repeat; background-size: contain">
        <h2 class="current-page">Danh sách cuộc đấu giá</h2>
        <div class="link-redirect">
            <a class="page-index" href="/">Trang chủ</a> / <span class="page-des">Cuộc đấu giá</span>
        </div>
    </div>
</div>

<div class="container">
    <div class="row high-padding">
        <div class="col-lg-3 col-md-4 sidebar-content">
            {{-- Search --}}
            <div class="blog-widget-item fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInUp;">
                <div class="search-area">
                    <div class="sidebar-widget-title">
                        <div class="sidebar-widget-title-text">
                            <h4>Tìm kiếm</h4>
                            <span class="hight-light"></span>
                        </div>
                    </div>
                    <div class="blog-widget-body">
                        <form action="" method="GET">
                            <div class="form-inner">
                                <div class="filter-search">
                                    <input type="search" id="filterKeyword" class="search-field" placeholder="Nhập từ khóa..." value="{{ old('keyword', request()->get('keyword')) }}" name="keyword">
                                </div>
                                <div class="filter-date" style="">
                                    <div class="filter-date-body" style="display: block; gap: 5px; margin-bottom: 15px;">
                                        <div class="from-date-container">
                                            <label style="font-weight: 600;" for="fromDate">Từ ngày</label>
                                            <div class="input-group date" id="fromDate" style="padding:0">
                                                <input class="form-control" type="text" style="border: none; border-bottom: 1px solid #e5ecef; box-shadow: unset; border-radius: 0;" name="from_date" id="fromDateInput" data-format="YYYY-MM-DD" readonly>
                                            </div>
                                        </div>
                                        <div class="to-date-container">
                                            <label style="font-weight: 600;" for="toDate">Đến ngày</label>
                                            <div class="input-group date" id="toDate" style="padding:0">
                                                <input class="form-control" type="text" style="border: none; border-bottom: 1px solid #e5ecef; box-shadow: unset; border-radius: 0;" name="to_date" id="toDateInput" data-format="YYYY-MM-DD" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="" style="">
                                        <button type="submit" class="button Buttonbtn">tìm kiếm</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- End Search --}}

            {{-- Filter  --}}
            <aside id="Auction-Property-Status" class="widget woocommerce widget_product_categories">
                <div class="blog-widget-item fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInUp;">
                    <div class="search-area">
                        <div class="sidebar-widget-title">
                            <h4>Trạng thái tài sản</h4>
                            <span class="hight-light"></span>
                        </div>
                        <div class="blog-widget-body">
                            <form method="GET" onchange="this.submit()">
                                <div class="form-inner">
                                    <label class="containercheckbox">
                                        Tất cả <span id="count-all"></span>
                                        <input type="radio" class="checkbox-status" name="checkbox-status" value="0" {{ $status == 0 ? 'checked' : '' }}>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="containercheckbox">
                                        Sắp diễn ra
                                        <input type="radio" class="checkbox-status" name="checkbox-status" value="1" {{ $status == 1 ? 'checked' : '' }}>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="containercheckbox">
                                        Đang diễn ra
                                        <input type="radio" class="checkbox-status" name="checkbox-status" value="2" {{ $status == 2 ? 'checked' : '' }}>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="containercheckbox">
                                        Đã kết thúc
                                        <input type="radio" class="checkbox-status" name="checkbox-status" value="3" {{ $status == 3 ? 'checked' : '' }}>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </form>                           
                        </div>
                    </div>
                </div>
            </aside>
            {{-- End Filter --}}

            {{-- My Auctions --}}
            @if (Auth::check())
                <aside id="Auction-Status" class="widget woocommerce widget_product_categories">
                    <div class="blog-widget-item fadeInUp" data-wow-duration="1.5s" data-wow-delay=".2s" style="visibility: visible; animation-duration: 1.5s; animation-name: fadeInUp;">
                        <div class="search-area">
                            <div class="sidebar-widget-title">
                                <h4>Đấu giá của tôi</h4>
                                <span class="hight-light"></span>
                            </div>
                            <div class="blog-widget-body">
                                <form method="GET" onchange="this.submit()">
                                    <div class="form-inner">
                                        <label class="containercheckbox">
                                            Tất cả <span id="count-all"></span>
                                            <input type="radio" class="checkbox" name="checkbox" value="4" {{ $status_my_auction == 4 ? 'checked' : '' }}>
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="containercheckbox">
                                            Sắp diễn ra
                                            <input type="radio" class="checkbox" name="checkbox" value="5" {{ $status_my_auction == 5 ? 'checked' : '' }}>
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="containercheckbox">
                                            Đang diễn ra
                                            <input type="radio" class="checkbox" name="checkbox" value="6" {{ $status_my_auction == 6 ? 'checked' : '' }}>
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="containercheckbox">
                                            Đã kết thúc
                                            <input type="radio" class="checkbox" name="checkbox" value="7" {{ $status_my_auction == 7 ? 'checked' : '' }}>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </form>                           
                            </div>
                        </div>
                    </div>
                </aside>
            @else
                {{-- Người dùng chưa đăng nhập --}}
            @endif 
            {{-- End My Auctions --}}
        </div>

        <div class="col-lg-9 col-md-8">
            <div class="row gy-4 mb-60 d-flex list-item">
                @foreach ($lists as $list)
                    <div class="col-sm-12 item-property col-lg-12 col-md-12" data-id="1001746" data-openingprice="undefined" data-propertystatusid="1000002" data-createdtime="2023-09-13T15:31:28.693">
                        <div data-wow-duration="1.5s" data-wow-delay="0.2s" class="eg-card auction-card1 animate-up fadeInDown eg-card-row">
                            <div class="auction-img col-lg-4 col-md-4">
                                <a href="{{ route('auction-detail',['id' => $list['id']]) }}">
                                    <img class="auction-img-background lazy lz-entered lz-loaded" alt="image" data-src="https://data.lvo.vn/media/upload/1004804/auction-hammer.jpg" data-ll-status="loaded" src="https://data.lvo.vn/media/upload/1004804/auction-hammer.jpg" style="height: 230px;">
                                </a>
                                        <div class="auction-timer">
                                            <div class="countdown" id="timer6">
                                                <h4>
                                                    {{ date('d/m/Y H:i:s', strtotime($list->start_time)); }}
                                                </h4>
                                            </div>
                                        </div>
                                <div class="author-area">
                                        
                                </div>
                            </div>
                            <div class="auction-content col-lg-8 col-md-8">
                                <h4><a class="" href="/cuoc-dau-gia/1001746-toan-bo-quyen-su-dung-dat-va-tai-san-gan-lien-voi-dat-tai-thua-dat-so-1040,-to-ban-do-so-30,-dia-chi:-thon-do-thuong,-xa-tien-phong,-huyen-me-linh,-thanh-pho-ha-noi-cua-cong-ty-co-phan-o-to-xuan-kien-vinaxuki">{{ $list->product->name }}</a></h4>
                                    <p>Trạng thái : 
                                        @if ($current_date < $list->start_time  )
                                            <span class="auctio-status auction-waiting">Sắp diễn ra</span>
                                        @elseif ($list->start_time < $current_date && $current_date < $list->end_time )
                                            <span class="auctio-status auction-loading">Đang diễn ra</span>
                                        @elseif ($list->end_time < $current_date)  
                                            <span class="auctio-status auction-done-success ">Đã kết thúc</span>  
                                        @else
                                            <span></span>                               
                                        @endif
                                    </p>
                                <div class="auction-card-bttm">
                                    <a href="{{ route('auction-detail',['id' => $list['id']]) }}" class="eg-btn btn--primary btn--sm">Chi tiết</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach              
            </div>   

            <div class="row">
                <nav class="pagination-wrap">
                    <div class="manage-auction-pagination light-theme simple-pagination">
                        {{ $lists->links('pagination::bootstrap-4') }}
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection