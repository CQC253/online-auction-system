@extends('layouts.master')
{{--set page tittle--}}
@section('title','Auction Payment')
    
@section('content')
    <section class="checkout_area padding_top">
        <div class="container">
        {{-- show message --}}
        @if(Session::has('success'))
            <p class="text-success">{{ Session::get('success') }}</p>
        @endif

        {{-- show error message --}}
        @if(Session::has('error'))
            <p class="text-danger">{{ Session::get('error') }}</p>
        @endif
        
        <div class="billing_details">
            <div class="row">
            <h2 style="width:100%; text-align: center; padding-bottom: 30px ">Vui lòng hoàn thành thanh toán để đăng ký</h2>
            <div class="col-lg-8">
                <h3>Xác nhận thông tin</h3>
                 <input type="hidden" name="bidder_id" value="{{ $bidder_info->id }}">
                <div class="col-md-12 form-group p_star">
                    <label for="name">Họ và tên:</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $bidder_info->name }}" readonly />
                </div>
                <input type="hidden" name="role" value="{{ $bidder_info->role }}">
                <div class="col-md-12 form-group p_star">
                    <label for="address">Địa chỉ:</label>
                    <input type="text" class="form-control" id="address" name="address" value="{{ $bidder_info->address }}"readonly />
                </div>
                <div class="col-md-12 form-group p_star">
                    <label for="address">Số điện thoại:</label>
                    <input type="text" class="form-control" id="number" name="tel" value="{{ $bidder_info->tel }}" readonly />
                </div>
                <div class="col-md-12 form-group p_star">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $bidder_info->email }}"readonly />
                </div>
                <div class="col-md-12 form-group p_star">
                    <label for="balance">Số dư hiện tại:</label>
                    <input type="text" class="form-control" id="balance" name="balance" value="{{ number_format($bidder_info->balance, 0, ',', '.') . ' VNĐ' }}" readonly />
                </div>
                <a href="{{ route('show-profile', ['id' => Auth::user()->id]) }}"  class="genric-btn primary circle" style="font-size: 18px; text-align: center; font-weight: 100; width: 50%; margin: 10px 25%;">Chỉnh sửa thông tin</a>
            </div>
            
            
                <div class="col-lg-4">
                    <form action="{{ route('auction-complete', ['id' => request()->route('id')]) }}" method="post">
                        @csrf
                    <div class="order_box">
                    <h2>Phương thức thanh toán</h2>
                    <ul class="list">
                        <li>
                        <p href="">Cuộc đấu giá:
                            <span>{{ $auction_payment->product->name }}</span>
                        </p>
                        </li>
                        <li>
                        <p href="">Phí đăng kí:
                            <span>{{ number_format($auction_payment->registration_fee, 0, ',', '.') . ' VNĐ' }}</span>
                        </p>
                        </li>
                        <li>
                        <p href="">Tiền đặt trước:
                            <span>{{ number_format($auction_payment->down_payment, 0, ',', '.') . ' VNĐ' }}</span>
                        </p>
                        </li>  
                    </ul>
                    <ul class="list list_2">
                        <li>
                            <p href="">Tổng:
                                <span>{{ number_format($auction_payment->registration_fee + $auction_payment->down_payment, 0, ',', '.') . ' VNĐ' }}</span>
                            </p>
                            </li> 
                    </ul>
                    <div class="payment_item">  
                        <div class="radion_btn">
                        <input type="radio" id="f-option5" name="payment_method" value="1" checked />
                        <label for="f-option5">2QC Pay</label>
                        <div class="check"></div>
                        </div>
                        <p>
                        Tổng số dư trong tài khoản: {{ number_format($bidder_info->balance, 0, ',', '.') . ' VNĐ' }}
                        </p>
                    </div>
                    <div class="payment_item active">
                        <div class="radion_btn">
                        <input type="radio" id="f-option6" name="payment_method" value="2" />
                        <label for="f-option6">Thanh toán bằng tài khoản ngân hàng </label>
                        <img src="img/product/single-product/card.jpg" alt="" />
                        <div class="check"></div>
                        </div>
                        <p>
                        Thẻ này đã được lưu khi đăng kí
                        </p>
                    </div>
                    <div class="creat_account">
                        <input type="checkbox" id="f-option4" name="confirm" value="1" />
                        <label for="f-option4">Tôi đã xem và đồng ý với </label>
                        <a href="">điều khoản & điều kiện</a>
                    </div>

                    @if ($errors->has('confirm'))
                        <span class="error">{{ $errors->first('confirm') }}</span>
                    @endif

                    <button class="btn_3" type="submit">Xác nhận thanh toán</button>
                    </div>
                 
                </form> 
                </div>
           
            
            </div>
        </div>
        </div>
    </section>
@endsection