@extends('layouts.master')
{{--set page tittle--}}
@section('title','Auction Register')
    
@push('js')
    <script src="/template/js/countdown_timer.js"></script>
@endpush

@section('content')
<div class="container" style="padding-top: 100px">
    <div id="banner-new" style="padding-top: 32px; padding-bottom: 26px; border-bottom: 1px solid #E0E0E0; ">
        <h2 class="current-page">Đăng ký đấu Giá</h2>
        <div class="link-redirect">
            <a class="page-index" href="{{ route('home') }}">Trang chủ</a> / <span class="page-des">Tài sản đấu giá</span>
        </div>
    </div>
</div>

<div class="auction-details-section pt-120 section-ap-detail" style="padding-top: 60px">
    <div class="container" style="display: flex; ">          
        <div class="row g-4 mb-50"></div>
            <div class="col-xl-7 col-lg-8 d-flex flex-row align-items-start justify-content-lg-start justify-content-center flex-md-nowrap flex-wrap gap-4 mode-register" id="photo-elements" style="flex: 1">
                <div class="w3-content w3-display-container" style="width: 100%">
                    <img class="mySlides" src="{{ json_decode($auction_register->product->thumbnail)[0] }}" style="width:100%">
                    @foreach (json_decode($auction_register->product->images) as $image)
                    <img class="mySlides" src="{{ $image }}" style="width:100%">
                    @endforeach
                
                    <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                    <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
                </div>     
            </div>
    
            <div class="col-xl-5 col-lg-4" style="flex: 1">
                <div class="product-details-right  wow fadeInDown" data-wow-duration="1.5s" data-wow-delay=".2s" style="visibility: visible; animation-duration: 1.5s; animation-delay: 0.2s; animation-name: fadeInDown;">         
                    {{-- ----------------------------Countdown Timer---------------------------- --}} 
                    <div id="countdown-timer" style="display: block;">
                        <div class="timestamp-div">
                            <p style="font-size: 17px; color: #696969; font-family: Montserrat; text-align: center;">Thời gian đếm ngược bắt đầu trả giá:</p>
                            <div id="timestamp" style="display: flex; justify-content: space-around">
                                @include('timers.countdown_timer')
                            </div>
                        </div>
                    </div>
                    {{------------------------------End Countdown Timer------------------------------}}

                    {{------------------------------Auction Detail------------------------------}}
                    <div id="auction-detail" style="display: block">
                        <div class="register-form" style="margin-top: 0px">
                            <div class="row" style="">
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Cuộc đấu giá</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ $auction_register->product->name }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian mở đăng ký:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_register->time_open_registration)); }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian kết thúc đăng ký:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_register->time_close_registration)); }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Giá khởi điểm:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_register->product->starting_price, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Phí đăng ký đấu giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_register->registration_fee, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Bước giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_register->price_step, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Số bước giá tối đa/ lần trả:</p>
                                </div>
                                <div class="col-6">
                                        <p class="right-info-text no-margin">{{ $auction_register->max_step }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Tiền đặt trước:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin"><span class="novaticPrice">{{ number_format($auction_register->down_payment, 0, ',', '.') . ' VNĐ' }}</span></p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Phương thức đấu giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ $auction_register->auction_method }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Tên người bán:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ $auction_register->product->seller->name }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian bắt đầu trả giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_register->start_time)); }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="left-title-text no-margin">Thời gian kết thúc trả giá:</p>
                                </div>
                                <div class="col-6">
                                    <p class="right-info-text no-margin">{{ date('d/m/Y H:i:s', strtotime($auction_register->end_time)); }}</p>
                                </div>
                            </div>
                            <form action="{{ route('auction-payment', ['id' => $auction_id] ) }}" method= "get">
                                <div class="describe-content info-ap">
                                    <div class="right-confirm-check">
                                        <p class="left-title-text no-margin">
                                                <input type="checkbox" required="" id="Check1" class="checkbox-biddingrequest" name="options[]"> Tôi đã đọc và đồng ý với quy chế đấu giá
                                        </p>
                                        <p class="left-title-text no-margin">
                                            <input type="checkbox" required="" id="Check2" class="checkbox-biddingrequest" name="options[]"> Tôi tự nguyện không nhận tiền lãi phát sinh từ khoản tiền đặt trước
                                        </p>
                                        <p class="left-title-text no-margin">
                                            <input type="checkbox" required="" id="Check3" class="checkbox-biddingrequest" name="options[]"> Tôi đã hiểu rõ về tài sản đấu giá và không có ý kiến gì về việc xem tài sản đấu giá
                                        </p>
                                    </div>

                                    <div class="row bidding-rq-stt" style="padding-top: 15px; padding-left: 12px; padding-right: 12px; ">
                                            <div class="d-grid gap-2">
                                            <button type="submit" name="register" class="btn btn-primary" style="text-transform: unset;border: unset; padding: 10px 20px; margin: 5px 45px; font-size: 16px; background: #b41712; width:80%; text-align:center; color: #fff; font-weight: 550 ">Đăng ký</button>       
                                            </div>      
                                    </div> 
                                    @error('options')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>                    
                            </form>
                        </div>
                    </div>
                    {{-- ----------------------------End Auction Detail------------------------------}}
                </div>
            </div>
        </div>

        <div class="row d-flex justify-content-center g-4" style="padding-top: 80px">
            <h4 style="ctext-transform: unset;border: unset; padding: 15px 30px; margin: 10px 50px; font-size: 18px; background: #b41712; text-align:center; color: #fff; border-radius: 5px; font-weight: 550; width: 20% ">Thông tin đấu giá</h4>
        
            <div class="col-lg-12">
                <div class="register-form" style="display: block; border: none; margin: 0 50px;">
                    <div class="describe-content info-ap">
                        <h4 style="margin-bottom: 20px">Thông tin tổ chức đấu giá</h4>
                        <p class="des-ap">Tổ chức đấu giá tài sản: <span class="content-ap">Công ty đấu giá 2CQ Aucion</span></p>
                        <p class="des-ap">Đấu giá viên: <span class="content-ap">Châu Quang Chiến</span></p>
                        <p class="des-ap">Địa chỉ: <span class="content-ap">92 Quang Trung, P. Thạch Thang, Q. Hải Châu, TP. Đà Nẵng</span></p>
                    </div>
                    <div class="describe-content info-ap">
                        <h4 style="margin-bottom: 20px">Tài liệu liên quan</h4>
                        <p class="des-ap" style="margin-bottom: 10px"> a. Mô tả tài sản xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p> 
                        <p class="des-ap" style="margin-bottom: 10px"> b. Hồ sơ mời đấu giá xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p> 
                        <p class="des-ap" style="margin-bottom: 10px"> c. Quy chế đấu giá tài sản xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p> 
                        <p class="des-ap" style="margin-bottom: 10px"> d. Thông báo Đấu giá tài sản xem <a href="link" target="_blank" style="color: #084CD1">tại đây</a></p>        
                    </div>    
                </div>
            </div>
        </div>
    </div> 
</div>
@endsection