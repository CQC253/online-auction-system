@extends('layouts.master')

{{--set page tittle--}}
@section('title','Result Bid')

@section('content')
<div class="container" style="margin-top: 100px">
    <div id="banner-new" style="padding-top: 32px; padding-bottom: 26px;  border-bottom: 1px solid rgb(224, 224, 224);">
        <h2 class="current-page">Kết quả đấu giá</h2>
        <div class="link-redirect">
            <a class="page-index" href="{{ route('home') }}" style="color: unset; text-decoration: none;">Trang chủ</a> / <span class="page-des">Kết quả đấu giá</span>
        </div>
    </div>

    <div id="banner-new">
        <div class="row ap-goal-price" style="margin-top: 60px">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 img-ap">
                <img src="/template/img/bannerketquadaugia.png" alt="Alternate Text" style="width: 100%;height: 100%">
            </div>
    
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 inf-ap">
                <h3 class="ap-name">{{ $auction_result->product->name }}</h3>
                <p class="prc-winner-txt">Giá trúng</p>
                <p class="prc-winner-cnt">
                    <img style="width: 20px; height: 20px; padding-bottom: 3px" src="/template/img/AuctionHammerResult.png" alt="Alternate Text">
                    <span class="novaticPrice">{{ number_format($auction_result->highest_price, 0, ',', '.') . ' VNĐ' }}</span>
                </p>
                <p class="openprice-dtl">(Giá khởi điểm: {{ number_format($auction_result->product->starting_price, 0, ',', '.') . ' VNĐ' }})</p>
            </div>
    
        </div>
    
        <div class="ap-status-result-success">
            <div style="padding: 8px">
                <p class="ap-status-result-txt">
                    @if($auction_result->status === 0)
                    Tài sản đấu giá thất bại
                    @elseif($auction_result->status === 1)
                    Tài sản đấu giá thành công
                    @endif
                </p>
            </div>
        </div>
    
        <div class="ap-open-detail">
            <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-4 no-padding" style="text-align:center">
                    <p class="time-text">Thời gian bắt đầu</p>
                    <p class="time-value">
                        {{ date('d/m/Y H:i:s', strtotime($auction_result->start_time)); }}
                    </p>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-4 no-padding" style="text-align:center">
                    <p class="time-text">Thời gian kết thúc</p>
                    <p class="time-value">
                        {{ date('d/m/Y H:i:s', strtotime($auction_result->end_time)); }}
                    </p>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-4 no-padding" style="text-align:center">
                    <p class="time-text">Người trúng đấu giá</p>
                    <p class="time-value">
                        {{ $user_name }}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection