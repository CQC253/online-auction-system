@extends('layouts.master')

{{--set page tittle--}}
@section('title','Login')

@section('content')
<section class="login_part padding_top">
<div class="login_part_form">
    <div class="col-md-3"></div>
    <div class="login_part_form_iner col-md-6">
        <h3 style="text-align: center">Welcome to 2CQ Auction! <br>
            Please Sign in here</h3>
        <form class="row contact_form" method="POST" action="{{ route('handle-login') }}" novalidate="novalidate">
            @csrf
            <div class="col-md-12 form-group p_star">
                <input type="email" class="form-control" id="email" name="email" value=""
                    placeholder="Email" required>
                    @error('email')
                    <div class="text-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="col-md-12 form-group p_star">
                <input type="password" class="form-control" id="password" name="password" value="" placeholder="Password" required>
            </div>
            <div class="col-md-12 form-group">
                <div class="creat_account d-flex align-items-center">
                    <input type="checkbox" id="f-option" name="remember">
                    <label for="f-option">Remember me</label>
                </div>

                {{-- show message --}}
                @if(Session::has('success'))
                    <p class="text-success">{{ Session::get('success') }}</p>
                @endif

                {{-- show error message --}}
                @if(Session::has('error'))
                    <p class="text-danger">{{ Session::get('error') }}</p>
                @endif

                <button type="submit" value="submit" class="btn_3">Log in</button>

                <a href="{{ route('register') }}" class="btn_3" style="text-align: center">Create New Account</a>
                <a class="lost_pass" href="#">forget password?</a>
            </div>
        </form>
    </div>
    <div class="col-md-3"></div>
</div>
</section>
@endsection