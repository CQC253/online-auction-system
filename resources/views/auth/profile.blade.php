@extends('layouts.master')

{{--set page tittle--}}
@section('title','Profile')

@push('css')
    <link rel="stylesheet" href="/template/css/profile.css">
@endpush

@push('js')
    <script src="/template/js/databank.js"></script>
    <script src="/template/js/datepicker.js"></script>
@endpush

@section('content')
<div class="container" style="margin-top: 150px">
    <div class="row g-4">
        <div class="col-lg-3">
            <div class="nav flex-column nav-pills gap-4" data-wow-duration="1.5s" data-wow-delay=".2s" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <button class="nav-link nav-btn-style mx-auto mb-20 active" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#ho-so" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="true">
                    <i class="lar la-user"></i><svg width="22" height="22" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.7782 14.2218C17.5801 13.0237 16.1541 12.1368 14.5982 11.5999C16.2646 10.4522 17.3594 8.53136 17.3594 6.35938C17.3594 2.85282 14.5066 0 11 0C7.49345 0 4.64062 2.85282 4.64062 6.35938C4.64062 8.53136 5.73543 10.4522 7.40188 11.5999C5.84598 12.1368 4.41994 13.0237 3.22184 14.2218C1.14421 16.2995 0 19.0618 0 22H1.71875C1.71875 16.8823 5.88229 12.7188 11 12.7188C16.1177 12.7188 20.2812 16.8823 20.2812 22H22C22 19.0618 20.8558 16.2995 18.7782 14.2218ZM11 11C8.44117 11 6.35938 8.91825 6.35938 6.35938C6.35938 3.8005 8.44117 1.71875 11 1.71875C13.5588 1.71875 15.6406 3.8005 15.6406 6.35938C15.6406 8.91825 13.5588 11 11 11Z"></path>
                    </svg><span class="show-text-pc">Thông tin cá nhân</span>
                </button>
                <button class="nav-link nav-btn-style mx-auto mb-20" id="v-pills-notification-tab" data-bs-toggle="pill" data-bs-target="#thong-bao" type="button" role="tab" aria-controls="v-pills-notification" aria-selected="false">
                    <svg fill="#000000" width="22" height="22" viewBox="0 0 36 36" version="1.1" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <title>notification-line</title>
                        <path class="clr-i-outline clr-i-outline-path-1" d="M32.51,27.83A14.4,14.4,0,0,1,30,24.9a12.63,12.63,0,0,1-1.35-4.81V15.15A10.81,10.81,0,0,0,19.21,4.4V3.11a1.33,1.33,0,1,0-2.67,0V4.42A10.81,10.81,0,0,0,7.21,15.15v4.94A12.63,12.63,0,0,1,5.86,24.9a14.4,14.4,0,0,1-2.47,2.93,1,1,0,0,0-.34.75v1.36a1,1,0,0,0,1,1h27.8a1,1,0,0,0,1-1V28.58A1,1,0,0,0,32.51,27.83ZM5.13,28.94a16.17,16.17,0,0,0,2.44-3,14.24,14.24,0,0,0,1.65-5.85V15.15a8.74,8.74,0,1,1,17.47,0v4.94a14.24,14.24,0,0,0,1.65,5.85,16.17,16.17,0,0,0,2.44,3Z"></path>
                        <path class="clr-i-outline clr-i-outline-path-2" d="M18,34.28A2.67,2.67,0,0,0,20.58,32H15.32A2.67,2.67,0,0,0,18,34.28Z"></path>
                        <rect x="0" y="0" width="36" height="36" fill-opacity="0"></rect>
                    </svg>
                    <span class="show-text-pc">Thông báo</span> {{-- sau này sẽ mở rộng phần này --}}
                </button>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade active show" id="ho-so" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                    <div class="dashboard-profile">
                        <div class="form-wrapper" style="padding:0px 40px 40px 40px;">
                            <div class="container res-mobile">
                                <form action="{{ route('update-profile', ['id' => $user->id]) }}" method="post" >
                                        @csrf
                                        @method('PUT')
                                    <div class="row layout"> 
                                        {{-- show message --}}
                                        @if(Session::has('success'))
                                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                                        @endif
                                        {{-- show error message --}}
                                        @if(Session::has('error'))
                                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                        @endif                                       
                                        <div class="personal-info">
                                            <h2 class="personal-info-txt">Thông tin cá nhân</h2>    
                                            <div class="button-group" style="margin-top:0px; margin-left: 150px">
                                                <button type="button" onclick="changePassword()" class="eg-btn profile-btn">
                                                    Đổi mật khẩu {{-- phần này mở rộng về sau  --}}
                                                </button>
                                            </div>
                                            <div class="button-group" style="margin-top:0px;">
                                                <button type="submit" onclick="updateAccount()" class="eg-btn profile-btn" formmethod="POST">Cập nhật</button>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Họ và tên</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="Name" name="name" value="{{ $user->name }}" required="" placeholder="Nhập email của bạn"> 
                                                    @error('name')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-12 col-md-6">
                                            <div class="form-inner" >
                                                <label class="label-form" style="position: relative">Ngày sinh</label> 
                                                <div class="input-group" style="position: relative">
                                                    <input class="date datepicker" name="birth_date" value="{{ $user->birthdate }}" id="birthDate" type="text" style=" padding: 10px 25px 10px 20px;" readonly>
                                                    <span class="input-group-addon" style="position: absolute; right: 5px; top: 10px;">  <img src="https://lacvietauction.vn/auctionart/upload/image/CalendarBlank.png"></span>
                                                    @error('birth_date')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-12 col-md-6">
                                            <div class="form-inner" >
                                                <label class="label-form" style="position: relative">Số điện thoại</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="account_phonenumber" name="phone" value="{{ $user->tel }}" required placeholder="Nhập số điện thoại của bạn"> 
                                                    @error('phone')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-12 col-md-6">
                                            <div class="form-inner" >
                                                <label class="label-form" style="position: relative">Số dư 2QCPay</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="balance" name="balance" value="{{ $user->balance }}" readonly> 
                                                    @error('balance')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Email</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="email" id="Email" name="email" value="{{ $user->email }}" required="" placeholder="Nhập email của bạn"> 
                                                    @error('email')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Địa chỉ</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="addressDetail" name="address" value="{{ $user->address }}" required="" placeholder="Nhập địa chỉ của bạn">
                                                    @error('address')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>                                         
                                    </div>
                                </form>
                                <div class="row layout"> {{-- Phát triển mở rộng --}}
                                    <div class="personal-info">
                                        <h2 class="personal-info-txt">Thông tin ngân hàng</h2>
                                        <div class="button-group" style="margin-top:0px;">
                                            <button type="button" onclick="updateBankAccount()" class="eg-btn profile-btn">
                                                Cập nhật
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-inner">
                                            <label class="label-form" style="position: relative">Số tài khoản ngân hàng</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" name="BankAccountNumber" id="bankAccountNumber" value="19036841628018" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-inner">
                                            <label class="label-form" style="position: relative">Tên ngân hàng</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" name="BankName" id="databank" value="Techcombank" required="">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <div class="form-inner">
                                            <label class="label-form" style="position: relative">Chi nhánh ngân hàng</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" name="BankAccountBranch" id="bankAccountBranch" value="Techcombank 29-3" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-inner">
                                            <label class="label-form" style="position: relative">Tên chủ tài khoản</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" name="BankAccountOwn" id="bankAccountName" value="Châu Quang Chiến" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection