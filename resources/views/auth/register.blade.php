@extends('layouts.master')

{{--set page tittle--}}
@section('title','Register')

@section('content')
<section class="login_part padding_top">
<div class="login_part_form">
    <div class="col-md-3"></div>
    <div class="login_part_form_iner col-md-6">
        <h3 style="text-align: center">Sign Up</h3>
        <form class="row contact_form" method="get" action="{{ route('handle-register') }}" novalidate="novalidate">
            @csrf
            <div class="col-md-12 form-group p_star">
                <input type="text" class="form-control" id="name" name="name" value="" placeholder="Full Name" required>
            </div>
            <div class="col-md-12 form-group p_star">
                <input type="text" class="form-control" id="address" name="address" value="" placeholder="Address" required>
            </div>
            <div class="col-md-12 form-group p_star">
                <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Phone" required>
            </div>
            <div class="col-md-12 form-group p_star">
                <input type="email" class="form-control" id="email" name="email" value="" placeholder="Email" required>
            </div>
            <div class="col-md-12 form-group p_star">
                <input type="text" class="form-control" id="password" name="password" value="" placeholder="Password" required>
            </div>
            <div class="col-md-12 form-group p_star">
                <input type="text" class="form-control" id="password" name="password" value="" placeholder="Confirm password" required>
            </div>
            <div class="col-md-12 form-group">
                <div class="creat_account d-flex align-items-center">
                    <input type="checkbox" id="f-option" name="selector">
                    <label for="f-option">I agree all statements in <a href="#!" class="text-body"><u>Terms of service</u></a></label>
                </div>
                <button type="submit" value="submit" class="btn_3">Register</button>
                <p class="text-center text-muted mt-5 mb-0">Have already an account? <a href="{{ route('login') }}" class="fw-bold text-body"><u>Login here</u></a></p>
            </div>
        </form>
    </div>
    <div class="col-md-3"></div>
</div>
</section>
@endsection