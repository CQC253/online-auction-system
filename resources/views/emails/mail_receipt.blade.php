<div class="mail_receipt" 
    style="width: 1080px; margin: 0 auto;
    padding: 15px;
    font-family: Arial, sans-serif;
    background-color: #f5f5f5;
    border: 1px solid #ccc">
    <h1 style="color: #333;">2QC Auction gửi bạn hóa đơn thanh toán</h1>
    <p>Cảm ơn bạn đã tham giá đấu giá tại 2QC Auction.</p>
    <p>Vui lòng thanh toán hóa đơn <strong>trong vòng 15 ngày</strong> kể từ ngày nhận được thông báo này.</p>
    <p>Để xem chi tiết hóa đơn, hãy kích vào liên kết bên dưới:</p>
    <a href="{{ route('show-receipt', ['id' => $id ]) }}" target="_blank" 
    style="display: inline-block;
        margin-top: 10px;
        padding: 10px 20px;
        background-color: #007bff;
        color: #fff;
        text-decoration: none;
        border-radius: 5px;">
        Xem hóa đơn
    </a>
</div>

