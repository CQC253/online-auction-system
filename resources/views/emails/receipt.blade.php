@extends('layouts.master')

{{--set page tittle--}}
@section('title','Receipt')

@push('css')
    <link rel="stylesheet" href="/template/css/profile.css">
@endpush

@push('js')
    
@endpush

@section('content')
<div class="container" style="margin-top: 150px">
    <div class="row g-4">
        <div class="col-lg-12">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade active show" id="ho-so" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                    <div class="dashboard-profile">
                        <div class="form-wrapper" style="padding:0px 40px 40px 40px;">
                            <div class="container res-mobile">
                                {{-- show message --}}
                                @if(Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif
                                {{-- show error message --}}
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                <div class="row layout">                                      
                                    <div class="personal-info">
                                        <h2 class="personal-info-txt">Thông tin cá nhân</h2>    
                                        <div class="button-group" style="margin-top:0px;">
                                            <a href="{{ route('show-profile', ['id' => $user->id ]) }}" class="btn eg-btn profile-btn" >Cập nhật</a>   
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-6">
                                        <div class="form-inner">
                                            <label class="label-form" style="position: relative">Họ và tên:</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" id="Name" name="name" value="{{ $user->name }}"   readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-12 col-md-6">
                                        <div class="form-inner" >
                                            <label class="label-form" style="position: relative">Ngày sinh:</label> 
                                            <div class="input-group" style="position: relative">
                                                <input class="date datepicker" name="birth_date" value="{{ $user->birthdate }}" id="birthDate" type="text" style=" padding: 10px 25px 10px 20px;" readonly>
                                                <span class="input-group-addon" style="position: absolute; right: 5px; top: 10px;">  <img src="https://lacvietauction.vn/auctionart/upload/image/CalendarBlank.png"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-12 col-md-6">
                                        <div class="form-inner" >
                                            <label class="label-form" style="position: relative">Số điện thoại:</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" id="account_phonenumber" name="phone" value="{{ $user->tel }}"   readonly>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-12 col-md-6">
                                        <div class="form-inner" >
                                            <label class="label-form" style="position: relative">Số dư 2QCPay:</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" id="balance" name="balance" value="{{ $user->balance }}" readonly> 
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-6">
                                        <div class="form-inner">
                                            <label class="label-form" style="position: relative">Email:</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="email" id="Email" name="email" value="{{ $user->email }}"   readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-6">
                                        <div class="form-inner">
                                            <label class="label-form" style="position: relative">Địa chỉ:</label>
                                            <div class="input-group" style="position: relative">
                                                <input type="text" id="addressDetail" name="address" value="{{ $user->address }}"   readonly>
                                            </div>
                                        </div>
                                    </div>                                         
                                </div>

                                <form action="{{ route('payment-receipt', ['id' => $auction_detail->id]) }}" method="post" >
                                        @csrf
                                        @method('PUT')
                                    <div class="row layout">                                        
                                        <div class="personal-info">
                                            <h2 class="personal-info-txt" style="justify-content: center">Thanh toán hóa đơn đấu giá</h2>    
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Tên cuộc đấu giá:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="nameAuction" name="name_auction" value="{{ $auction_detail->product->name }}"   readonly>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Thời gian bắt đầu trả giá:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="start_time" name="start_time" value="{{ date('d/m/Y H:i:s', strtotime($auction_detail->start_time)) }}"   readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Thời gian kết thúc trả giá:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="end_time" name="end_time" value="{{ date('d/m/Y H:i:s', strtotime($auction_detail->end_time)) }}"   readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Tiền đặt trước:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="down_payment" name="down_payment" value="{{ number_format($auction_detail->down_payment, 0, ',', '.') . ' VNĐ' }}"   readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Giá khởi điểm:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="starting_price" name="starting_price" value="{{ number_format($auction_detail->product->starting_price, 0, ',', '.') . ' VNĐ' }}"   readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Bước giá:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="price_step" name="price_step" value="{{ number_format($auction_detail->price_step, 0, ',', '.') . ' VNĐ' }}"   readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Số bước giá tối đa/ lần trả:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="max_step" name="max_step" value="{{ $auction_detail->max_step }}" readonly>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Phương thức đấu giá:</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="auction_method" name="auction_method" value="{{ $auction_detail->auction_method }}" readonly>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-xl-12 col-lg-12 col-md-6">
                                            <div class="form-inner">
                                                <label class="label-form" style="position: relative">Giá trúng</label>
                                                <div class="input-group" style="position: relative">
                                                    <input type="text" id="highest_price" name="highest_price" value="{{ number_format($auction_detail->highest_price, 0, ',', '.') . ' VNĐ' }}"   readonly>
                                                </div>
                                            </div>
                                        </div>                        
                                        <div class="button-group" style="margin-top:20px;">
                                            <button type="submit" class="eg-btn profile-btn" formmethod="POST">Thanh toán</button>
                                        </div>                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection