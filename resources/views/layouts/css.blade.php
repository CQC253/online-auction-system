   <link rel="icon" href="/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/template/css/bootstrap.min.css">
    <!-- sweetalert2.min.css CSS -->
    <link rel="stylesheet" href="/template/css/sweetalert2.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="/template/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="/template/css/owl.carousel.min.css">
    <!-- nice select CSS -->
  <link rel="stylesheet" href="/template/css/nice-select.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="/template/css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="/template/css/flaticon.css">
    <link rel="stylesheet" href="/template/css/themify-icons.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="/template/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="/template/css/slick.css">
    <link rel="stylesheet" href="/template/css/price_rangs.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="/template/css/style.css">
    <!-- auction_detail CSS -->
    <link rel="stylesheet" href="/template/css/auction_detail.css">
    <!-- countdown_timer CSS -->
    <link rel="stylesheet" href="/template/css/countdown_timer.css">
    <link rel="stylesheet" href="/template/css/bid_countdown_timer.css">
    <link rel="stylesheet" href="/template/css/register_countdown_timer.css">

    <!-- slide_show CSS -->
    <link rel="stylesheet" href="/template/css/slideshow.css">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">
    <link rel="stylesheet" href="/template/css/toastr.min.css">

@stack('css')