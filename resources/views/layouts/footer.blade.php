 <!--::footer_part start::-->
 <footer class="footer_part">
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-xl-3 col-lg-6 col-md-12 d-flex justify-content-xl-center">
                <div class="single_footer_part">
                    <h4>Công ty 2CQ Auction</h4>
                    <ul class="list-unstyled">
                        <li><a href="">Mã số thuế: 0123456789</a></li>
                        <li><a href="">Đại diện: ông Châu Quang Chiến</a></li>
                        <li><a href="">Địa chỉ: 92 Quang Trung, P. Thạch Thang, Q. Hải Châu, TP. Đà Nẵng</a></li>
                        <li><a href="">Điện thoại: 0935697729</a></li>
                        <li><a href="">Email: quangchien253@gmail.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 d-flex justify-content-xl-center">
                <div class="single_footer_part">
                    <h4>Về chúng tôi</h4>
                    <ul class="list-unstyled">
                        <li><a href="">Giới thiệu</a></li>
                        <li><a href="">Quy chế hoạt động</a></li>
                        <li><a href="">Cơ chế giải quyết tranh chấp</a></li>
                        <li><a href="">Hướng dẫn</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 d-flex justify-content-xl-center">
                <div class="single_footer_part">
                    <h4>Chính sách</h4>
                    <ul class="list-unstyled">
                        <li><a href="">Câu hỏi thường gặp</a></li>
                        <li><a href="">Thuê đấu giá trực tuyến</a></li>
                        <li><a href="">Văn bản pháp quy</a></li>
                        <li><a href="">Chính sách bảo mật</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 d-flex justify-content-xl-center">
                <div class="single_footer_part">
                    <h4>Tham gia nhận tin</h4>
                    <p>Đăng ký nhận tin mới qua email
                    </p>
                    <div id="mc_embed_signup">
                        <form target="_blank"
                            action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                            method="get" class="subscribe_form relative mail_part">
                            <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                                class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                                onblur="this.placeholder = ' Email Address '">
                            <button type="submit" name="submit" id="newsletter-submit"
                                class="email_icon newsletter-submit button-contactForm" style="position: unset;">Đăng kí</button>
                            <div class="mt-10 info"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <div class="copyright_part">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="copyright_text">
                        <P>
Copyright &copy;<script>document.write(new Date().getFullYear());</script>  Công ty đấu giá 2CQ <i class="ti-heart" aria-hidden="true"></i> 
</P>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="footer_icon social_icon">
                        <ul class="list-unstyled">
                            <li><a href="#" class="single_social_icon"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fas fa-globe"></i></a></li>
                            <li><a href="#" class="single_social_icon"><i class="fab fa-behance"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--::footer_part end::-->