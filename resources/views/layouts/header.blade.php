<!--::header part start::-->
<header class="main_menu home_menu" onload="initClock()">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="{{ route('home') }}"> <img src="/template/img/logo.png" alt="logo" style="height: 60px"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="menu_icon"><i class="fas fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_1"
                                    role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Tài sản đấu giá
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown_1">
                                    <a class="dropdown-item" href="category.html"> Bất động sản</a>
                                    <a class="dropdown-item" href="single-product.html">Phương tiện - Xe</a>
                                    <a class="dropdown-item" href="single-product.html">Sưu tầm - Nghệ thuật</a>
                                    <a class="dropdown-item" href="single-product.html">Hàng hiệu</a>
                                    <a class="dropdown-item" href="single-product.html">Tài sản khác</a>
                                    
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="" id="navbarDropdown_3"
                                    role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Cuộc đấu giá
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown_2">
                                    <a class="dropdown-item" href="{{ route('auction_list') }}?checkbox-status=1">Sắp diễn ra</a>
                                    <a class="dropdown-item" href="{{ route('auction_list') }}?checkbox-status=2">Đang diễn ra</a>
                                    <a class="dropdown-item" href="{{ route('auction_list') }}?checkbox-status=3">Đã kết thúc</a>                                
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_2"
                                    role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Tin tức
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown_2">
                                    <a class="dropdown-item" href="blog.html">Thông báo</a>
                                    <a class="dropdown-item" href="single-blog.html">Tin tức khác</a>
                                </div>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="contact.html">Giới thiệu</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.html">Liên hệ</a>
                            </li>
                            <li class="nav-item" style="padding: 20px 15px">
                                @include('timers.clock')
                            </li>      
                        </ul>
                    </div>
                    @auth
                        <div class="dropdown">
                            <button class="btn dropdown-toggle btn_2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 120px; line-height: 40px; height: 30px; font-weight: 400; padding-bottom: 45px; ">
                            {{ Auth::user()->name }}
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="{{ route('show-profile', ['id' => Auth::user()->id]) }}">Thông tin cá nhân</a>
                            <a class="dropdown-item" href="{{ route('auction_list') }}?checkbox=4">Đấu giá của tôi</a>
                            <a class="dropdown-item" href="{{ route('logout') }}">Đăng xuất</a>
                            </div>
                        </div>
                    @endauth
                    @guest                        
                        <div class="login">
                            <a href="{{ route('login') }}" class="btn_2" style="width: 100px; height: 40px; line-height: 40px; font-weight: 400; ">Login</a>
                        </div>
                    @endguest  
                    <div class="hearer_icon d-flex">
                        <a id="search_1" href="javascript:void(0)" ><i class="ti-search"></i></a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    
    <div class="search_input" id="search_input_box">
        <div class="container ">
            <form class="d-flex justify-content-between search-inner">
                <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                <button type="submit" class="btn"></button>
                <span class="ti-close" id="close_search" title="Close Search"></span>
            </form>
        </div>
    </div>
</header>
<!-- Header part end-->