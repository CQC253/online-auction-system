@extends('layouts.master')

{{-- set page title --}}
@section('title', 'Homepage')

@push('css')
    <link rel="stylesheet" href="/template/css/homepage.css">
@endpush

@section('content')
    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="banner_slider owl-carousel">
                        <div class="single_banner_slider">
                            <div class="row">
                                <div class="col-lg-5 col-md-8">
                                    <div class="banner_text">
                                        <div class="banner_text_iner">
                                            <h1>2CQ Auction</h1>
                                            <h4>Nền tảng đấu giá trực tuyến hàng đầu</h4>
                                            <p>Tự hào là một trong những nhà đấu giá lớn tại Việt Nam, 2CQ Auction luôn là đơn vị tiên phong ứng dụng công nghệ thông tin vào hoạt động đấu giá. 2CQ Auction là đơn vị tổ chức cuộc đấu giá trực tuyến chính thống hàng đầu Việt Nam</p>
                                            <a href="#" class="btn_2">Khám phá</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="banner_img d-none d-lg-block" style="width:60%; float: right; margin-left: 10px;">
                                    <img src="/template/img/banner.png" alt="" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- news start-->
    <section class="feature_part padding_top">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section_tittle text-center">
                        <h2>Tin tức & thông báo mới nhất</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_product_item">
                        <div class="date">
                            <span>30/8/2023</span>
                        </div>
                        <img src="https://cdn.arstechnica.net/wp-content/uploads/2023/04/IMG_2117-800x533.jpg" alt="">                        
                        <div class="single_product_text" >
                            <h4 style="overflow: hidden !important; text-overflow: ellipsis !important; display: -webkit-box !important; -webkit-line-clamp: 4 !important;-webkit-box-orient: vertical !important; min-height: 80px;">01 quyền sử dụng đất, quyền sở hữu nhà ở và tài sản khác gắn liền với đất tại thửa đất số 67, tờ bản đồ số 23, diện tích 58,2m2 có địa chỉ: Số 2, ngõ 73 phố Khương Trung, phường Khương Trung, quận Thanh Xuân, thành phố Hà Nội  (lần bán 03)</h4>
                            <a href="#" class="feature_btn">Details<i class="fas fa-play" style="color: #ff3368"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_product_item">
                        <div class="date">
                            <span>30/8/2023</span>
                        </div>
                        <img src="https://cdn.arstechnica.net/wp-content/uploads/2023/04/IMG_2117-800x533.jpg" alt="">                        
                        <div class="single_product_text" >
                            <h4 style="overflow: hidden !important; text-overflow: ellipsis !important; display: -webkit-box !important; -webkit-line-clamp: 4 !important;-webkit-box-orient: vertical !important; min-height: 80px;">01 quyền sử dụng đất, quyền sở hữu nhà ở và tài sản khác gắn liền với đất tại thửa đất số 67, tờ bản đồ số 23, diện tích 58,2m2 có địa chỉ: Số 2, ngõ 73 phố Khương Trung, phường Khương Trung, quận Thanh Xuân, thành phố Hà Nội  (lần bán 03)</h4>
                            <a href="#" class="feature_btn">Details<i class="fas fa-play" style="color: #ff3368"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_product_item">
                        <div class="date">
                            <span>30/8/2023</span>
                        </div>
                        <img src="https://cdn.arstechnica.net/wp-content/uploads/2023/04/IMG_2117-800x533.jpg" alt="">                        
                        <div class="single_product_text" >
                            <h4 style="overflow: hidden !important; text-overflow: ellipsis !important; display: -webkit-box !important; -webkit-line-clamp: 4 !important;-webkit-box-orient: vertical !important; min-height: 80px;">01 quyền sử dụng đất, quyền sở hữu nhà ở và tài sản khác gắn liền với đất tại thửa đất số 67, tờ bản đồ số 23, diện tích 58,2m2 có địa chỉ: Số 2, ngõ 73 phố Khương Trung, phường Khương Trung, quận Thanh Xuân, thành phố Hà Nội  (lần bán 03)</h4>
                            <a href="#" class="feature_btn">Details<i class="fas fa-play" style="color: #ff3368"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_product_item">
                        <div class="date">
                            <span>30/8/2023</span>
                        </div>
                        <img src="https://cdn.arstechnica.net/wp-content/uploads/2023/04/IMG_2117-800x533.jpg" alt="">                        
                        <div class="single_product_text" >
                            <h4 style="overflow: hidden !important; text-overflow: ellipsis !important; display: -webkit-box !important; -webkit-line-clamp: 4 !important;-webkit-box-orient: vertical !important; min-height: 80px;">01 quyền sử dụng đất, quyền sở hữu nhà ở và tài sản khác gắn liền với đất tại thửa đất số 67, tờ bản đồ số 23, diện tích 58,2m2 có địa chỉ: Số 2, ngõ 73 phố Khương Trung, phường Khương Trung, quận Thanh Xuân, thành phố Hà Nội  (lần bán 03)</h4>
                            <a href="#" class="feature_btn">Details<i class="fas fa-play" style="color: #ff3368"></i></a>
                        </div>
                    </div>
                </div>
            <div class="banner_text_iner">                
                <a href="#" class="btn_2">Xem tất cả</a>
            </div>
        </div>
    </section>
    <!-- news end-->

    <!-- product_list start-->
    <section class="product_list section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="section_tittle text-center">
                        <h2 style="text-align: center; padding-top:140px">Tài sản sắp được đấu giá</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="product_list_slider owl-carousel">
                        <div class="single_product_list_slider">
                            <div class="row align-items-center justify-content-between">
                                @foreach ($auctions as $auction)
                                    <div class="col-lg-3 col-sm-6">
                                        <div class="single_product_item">
                                            <div class="auction-timer" style="text-align: center;">
                                                <span class="timer-title" style="color: #696969; margin-bottom: 5px; display: block">Thời gian đấu giá</span>
                                                <div class="countdown" style="font-weight:700; margin: 5px 25%" >
                                                    <span>{{ date('d/m/Y H:i:s', strtotime($auction->start_time)); }}</span>
                                                </div>
                                            </div>
                                            <a href="{{ route('auction-detail',['id' => $auction['id']]) }}">
                                                <img src="{{ json_decode($auction->product->thumbnail)[0] }}" alt="" style="width: 100%; height:280px;">
                                            </a>
                                                                    
                                            <div class="single_product_text" >
                                                <h4 style="overflow: hidden !important; text-overflow: ellipsis !important; display: -webkit-box !important; -webkit-line-clamp: 4 !important;-webkit-box-orient: vertical !important; min-height: 30px; ">
                                                    <a href="{{ route('auction-detail',['id' => $auction['id']]) }}" style="visibility: unset;">{{ $auction->product->name }}</a>
                                                </h4>
                                                <p>Giá khởi điểm : <span style="font-weight:700">{{ number_format($auction->product->starting_price, 0, ',', '.') . ' VNĐ';}}</span></p>
                                                <a href="{{ route('auction-detail',['id' => $auction['id']]) }}" class="add_cart">Chi Tiết<i class="ti-heart"></i></a>
                                            </div>                        
                                        </div>
                                    </div>
                                @endforeach                              
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="banner_text_iner">                
                <a href="{{ route('auction_list') }}?checkbox-status=0" class="btn_2">Xem tất cả</a>
            </div>
        </div>
    </section>
    <!-- product_list end-->


    <!-- subscribe_area part start-->
    <section class="client_logo padding_top">
        <div class="container">
            <div class="section_tittle text-center">
                <h2 style="text-align: center">Khách hàng & đối tác</h2>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="single_client_logo">
                        <img src="/template/img/client-12.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-3.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-4.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-5.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-6.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-7.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-8.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-9.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-10.png" alt="">
                    </div>
                    <div class="single_client_logo">
                        <img src="/template/img/client-14.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::subscribe_area part end::-->

@endsection