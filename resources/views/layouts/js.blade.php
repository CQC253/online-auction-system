<!-- Bao gồm thư viện jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- Bao gồm thư viện Pikaday -->
<script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/vi.min.js"></script>

<script src="/template/js/toastr.min.js"></script>
<!-- popper js -->
{{-- <script src="/template/js/popper.min.js"></script> --}}
<!-- bootstrap js -->
<script src="/template/js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="/template/js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
{{-- <script src="/template/js/swiper.min.js"></script> --}}
<!-- swiper js -->
<script src="/template/js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="/template/js/owl.carousel.min.js"></script>
<script src="/template/js/jquery.nice-select.min.js"></script>
<!-- slick js -->
<script src="/template/js/slick.min.js"></script>
<script src="/template/js/jquery.counterup.min.js"></script>
<script src="/template/js/waypoints.min.js"></script>
{{-- <script src="/template/js/contact.js"></script> --}}
<script src="/template/js/jquery.ajaxchimp.min.js"></script>
<script src="/template/js/jquery.form.js"></script>
<script src="/template/js/jquery.validate.min.js"></script>
<script src="/template/js/mail-script.js"></script>
<!-- countdown_timer.js -->
{{-- <script src="/template/js/countdown_timer.js"></script>
<script src="/template/js/bid_countdown_timer.js"></script>
<script src="/template/js/register_countdown_timer.js"></script> --}}
<!-- clock.js -->
<script src="/template/js/clock.js"></script>
<!-- custom js -->
<script src="/template/js/custom.js"></script>

<!-- slide_show js -->
<script src="/template/js/slideshow.js"></script>

<!-- Thêm thư viện jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script src="https://cdn.logwork.com/widget/countdown.js"></script>



@stack('js')