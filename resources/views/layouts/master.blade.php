<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
      
    <title>@yield('title','Home Page')</title>
    {{--css--}}
    @include('layouts.css')
</head>
<body>
    {{--header--}}
    @include('layouts.header')

    {{--menu--}}
    @include('layouts.menu')

    {{--content--}}
    <main>
        @yield('content')
    </main>
    
    {{--footer--}}
    @include('layouts.footer')

    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i class="bi bi-arrow-up"></i></a>

    {{--js--}}
    @include('layouts.js')
</body>
</html>
