<?php

use App\Http\Controllers\AuctionController;
use App\Http\Controllers\BidController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ReceiptController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[HomeController::class, 'home'])->name('home');

// định nghĩa route dành cho Authentication
Route::get('/login', [LoginController::class, 'showForm'])
    ->name('login');
Route::post('/login', [LoginController::class, 'handleForm'])
    ->name('handle-login');

// Chức năng đăng ký 1 User mới
Route::get('/register', [RegisterController::class, 'showForm'])
    ->name('register');
Route::post('/register', [RegisterController::class, 'handleForm'])
    ->name('handle-register');

// Chức năng đăng xuất (logout)
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

// Định nghĩa route dành cho profile
    Route::get('/profile/{id}/show', [ProfileController::class, 'showProfile'])
    ->name('show-profile');

    Route::put('/profile/{id}/update', [ProfileController::class, 'updateProfile'])
    ->name('update-profile');

//Định nghĩa route dành cho auction detail
Route::get('/auctions/{id}', [AuctionController::class, 'showDetail'])
    ->name('auction-detail');

//Định nghĩa route dành cho register
Route::get('/auctions/{id}/register', [AuctionController::class, 'register'])
    ->name('auction-register');
    
//Định nghĩa route dành cho confirm-register
Route::post('/auctions/{id}/register', [AuctionController::class, 'handleRegister'])
    ->name('auction-handle-register');

//Định nghĩa route dành cho payment
Route::get('auctions/{id}/register/payment', [AuctionController::class, 'payment'])
    ->name('auction-payment');

//Định nghĩa route dành cho complete
Route::post('auctions/{id}/register/payment/complete', [AuctionController::class, 'complete'])
    ->name('auction-complete');

//Định nghĩa route dành cho phần bid
Route::post('/auctions/{id}/bid', [BidController::class, 'bid'])
    ->name('auction-bid');

//Định nghĩa route dành cho phần ajax bid
Route::get('/auctions/{id}/highest_price', [BidController::class, 'highest_price'])
    ->name('highest_price');
Route::get('/auctions/{id}/user_highest_price', [BidController::class, 'user_highest_price'])
    ->name('user_highest_price');
Route::get('/auctions/{id}/update_status', [BidController::class, 'update_status'])
    ->name('update_status');
    
//Định nghĩa route dành cho phần end bid
Route::get('/auctions/{id}/result-bid', [BidController::class, 'result_bid'])
    ->name('auction-result-bid'); 

//Định nghĩa route dành cho phần gửi mail
Route::post('/auctions/{id}/send-mail', [BidController::class, 'send_mail'])
->name('send_mail');

//Định nghĩa route dành cho phần danh sách đấu giá
Route::get('/list', [HomeController::class, 'list'])
    ->name('auction_list');

//Định nghĩa route dành cho phần filter của danh sách đấu giá
Route::get('/list/filter_auction', [HomeController::class, 'filter_auction'])->name('filter_auction');

// Định nghĩa route dành cho receipt
Route::get('/receipt/{id}/show', [ReceiptController::class, 'showReceipt'])
->name('show-receipt');

Route::put('/receipt/{id}/payment', [ReceiptController::class, 'paymentReceipt'])
->name('payment-receipt');

